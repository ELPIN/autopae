package Portal;

import com.everis.utilities.ExcelUtils;
import com.everis.global.Global;
import com.everis.po.GuiasRapidasUsoPage;
import com.everis.po.IndexPage;
import com.everis.po.LoginPage;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class GuiasRapidasUsoTest extends Base {

	Global global = new Global(driver);

	@DataProvider
	public Object[][] GuiasRapidasUsoCases(ITestContext context) throws Exception {
		Object[][] testObjArray = ExcelUtils.getTableArray(
				super.getDataProviderDir() + context.getCurrentXmlTest().getParameter("dataProvider"),
				context.getCurrentXmlTest().getParameter("sheet"));
		return (testObjArray);
	}

	@Test(description = "152-Gu�as r�pidas de Uso - Visualizar gu�as", dataProvider = "GuiasRapidasUsoCases")
	public void guiasRapidasVisualizarGuias(String sUsername, String sPassword) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		GuiasRapidasUsoPage guias = new GuiasRapidasUsoPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.clickSeccionGuiasRapidasUso();

		// Aserciones
		Assert.assertTrue(guias.validarGuiasRapidasUso(), "Se valida la presencia de todas las gu�as.");

	}

	@Test(description = "153-Gu�as r�pidas de Uso - Visualizar Gu�a Presentaci�n", dataProvider = "GuiasRapidasUsoCases")
	public void guiasRapidasVisualizarPresentacion(String sUsername, String sPassword) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		GuiasRapidasUsoPage guias = new GuiasRapidasUsoPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.clickSeccionGuiasRapidasUso();
		guias.clickPresentacion();

		// Aserciones
		Assert.assertEquals(guias.popupVideoPresentacion(), "Video Presentaci�n Portal");
		global.log().info("Se valida popup con el video de Presentaci�n.");

		// Acciones
		guias.visualizarVideo();
		
	}
	
	@Test(description = "154-Gu�as r�pidas de Uso - Visualizar Gu�a Manual de Aplicaci�n", dataProvider = "GuiasRapidasUsoCases")
	public void guiasRapidasVisualizarManualAplicacion(String sUsername, String sPassword, String guia) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		GuiasRapidasUsoPage guias = new GuiasRapidasUsoPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.clickSeccionGuiasRapidasUso();
		guias.clickManualAplicacion(guia);
		
	}
	
	@Test(description = "155-Gu�as r�pidas de Uso - Visualizar Gu�a Introducci�n a ENERGY CENTER", dataProvider = "GuiasRapidasUsoCases")
	public void guiasRapidasVisualizarIntroduccionEcenter(String sUsername, String sPassword, String guia) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		GuiasRapidasUsoPage guias = new GuiasRapidasUsoPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.clickSeccionGuiasRapidasUso();
		guias.clickIntroduccionEcenter(guia);
		
	}
	
	@Test(description = "156-Gu�as r�pidas de Uso - Visualizar Gu�a Ingreso a la Aplicaci�n", dataProvider = "GuiasRapidasUsoCases")
	public void guiasRapidasVisualizarIngresoAplicacion(String sUsername, String sPassword, String guia) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		GuiasRapidasUsoPage guias = new GuiasRapidasUsoPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.clickSeccionGuiasRapidasUso();
		guias.clickIngresoAplicacion(guia);
		
	}
	
	@Test(description = "157-Gu�as r�pidas de Uso - Visualizar Gu�a Gesti�n de pedidos", dataProvider = "GuiasRapidasUsoCases")
	public void guiasRapidasVisualizarGestionPedidos(String sUsername, String sPassword, String guia) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		GuiasRapidasUsoPage guias = new GuiasRapidasUsoPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.clickSeccionGuiasRapidasUso();
		guias.clickGestionPedidos(guia);
		
	}


	@Test(description = "Gu�as r�pidas de Uso - Visualizar Gu�a TyC", dataProvider = "GuiasRapidasUsoCases")
	public void guiasRapidasVisualizarTyC(String sUsername, String sPassword, String guia) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		GuiasRapidasUsoPage guias = new GuiasRapidasUsoPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.clickSeccionGuiasRapidasUso();
		guias.clickTyC(guia);

	}
	
	@Test(description = "Gu�as r�pidas de Uso - Visualizar Gu�a Cuenta Corriente", dataProvider = "GuiasRapidasUsoCases")
	public void guiasRapidasVisualizarCuentaCorriente(String sUsername, String sPassword, String guia) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		GuiasRapidasUsoPage guias = new GuiasRapidasUsoPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.clickSeccionGuiasRapidasUso();
		guias.clickCuentaCorriente(guia);
		
	}

	@Test(description = "Gu�as r�pidas de Uso - Visualizar Gu�a Productos Lubricantes", dataProvider = "GuiasRapidasUsoCases")
	public void guiasRapidasVisualizarProductosLubricantes(String sUsername, String sPassword, String guia) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		GuiasRapidasUsoPage guias = new GuiasRapidasUsoPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.clickSeccionGuiasRapidasUso();
		guias.clickProductosLubricantes(guia);

	}
	
}

