package Portal;


import com.everis.global.Global;
import com.everis.po.IndexPage;
import com.everis.po.LoginPage;
import com.everis.po.MiCuentaPage;
import com.everis.po.PatentesPage;
import com.everis.utilities.ExcelUtils;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


public class PatentesTest extends Base {

    Global global = new Global(driver);

    @DataProvider
    public Object[][] PatentesCases(ITestContext context) throws Exception {
        Object[][] testObjArray = ExcelUtils.getTableArray(
                super.getDataProviderDir() + context.getCurrentXmlTest().getParameter("dataProvider"),
                context.getCurrentXmlTest().getParameter("sheet"));
        return (testObjArray);
    }

    @Test(description = "XXX-Acceder a secci�n Patentes - Usuario FOB", dataProvider = "PatentesCases")
    public void accesoPatentesClienteFOB(String sUsername, String sPassword) throws InterruptedException {

        global.log().info("Inicia el caso de prueba");
        LoginPage login = new LoginPage(driver);
        IndexPage index = new IndexPage(driver);
        MiCuentaPage miCuenta = new MiCuentaPage(driver);
        PatentesPage patentesPage = new PatentesPage(driver);

        // Acciones
        login.executeLogin(sUsername, sPassword);
        global.log().info("Se valid� correctamente el inicio de sesi�n.");
        index.seccionMiCuenta();
        global.log().info("Se accedi� correctamente a 'Mi Cuenta'.");
        miCuenta.irSeccionPatentes();

        // Aserciones
        Assert.assertTrue(patentesPage.tituloPatentesHabilitadas(), "Patentes habilitadas para FOB");
        global.log().info("Se valida que figure el texto 'Patentes habilitadas para FOB'.");
    }

    @Test(description = "PAE-163 - Ocultar acceso a usuario CIF", dataProvider = "PatentesCases")
    public void noAccesoPatentesClienteCIF(String sUsername, String sPassword) throws InterruptedException {

        global.log().info("Inicia el caso de prueba");
        LoginPage login = new LoginPage(driver);
        IndexPage index = new IndexPage(driver);
        MiCuentaPage miCuenta = new MiCuentaPage(driver);
        PatentesPage patentesPage = new PatentesPage(driver);

        // Acciones
        login.executeLogin(sUsername, sPassword);
        global.log().info("Se valid� correctamente el inicio de sesi�n.");
        index.seccionMiCuenta();
        global.log().info("Se accedi� correctamente a 'Mi Cuenta'.");

        // Aserciones
        Assert.assertFalse(miCuenta.buttonAsociarPatente(), "Valida que no se vea el boton PATENTES para un CIF");
        global.log().info("Se valida que un usuario CIF no pueda entrar a patentes");
    }



//    @Test(description = "141-Asociar Patente Duplicada", dataProvider = "miCuentaCases")
//    public void asociarPatenteDuplicada(String sUsername, String sPassword, String patente) throws InterruptedException {
//
//        global.log().info("Inicia el caso de prueba");
//        LoginPage login = new LoginPage(driver);
//        IndexPage index = new IndexPage(driver);
//        MiCuentaPage miCuenta = new MiCuentaPage(driver);
//
//        // Acciones
//        login.executeLogin(sUsername, sPassword);
//        global.log().info("Se valid� correctamente el inicio de sesi�n.");
//        index.seccionMiCuenta();
//        miCuenta.irSeccionPatentes();
//        miCuenta.asociarPatente(patente);
//
//        // Aserciones
//        Assert.assertTrue(miCuenta.errorAsociarPatente().contains("Ingrese patente no asociada"));
//        global.log().info("Se valida cartel de error en la asociaci�n de patente. ");
//
//    }

//    @Test(description = "142-Asociar Patente Inv�lida", dataProvider = "miCuentaCases")
//    public void asociarPatenteInvalida(String sUsername, String sPassword, String patente) throws InterruptedException {
//
//        global.log().info("Inicia el caso de prueba");
//        LoginPage login = new LoginPage(driver);
//        IndexPage index = new IndexPage(driver);
//        MiCuentaPage miCuenta = new MiCuentaPage(driver);
//
//        // Acciones
//        login.executeLogin(sUsername, sPassword);
//        global.log().info("Se valid� correctamente el inicio de sesi�n.");
//        index.seccionMiCuenta();
//        miCuenta.irSeccionPatentes();
//        miCuenta.asociarPatente(patente);
//
//        // Aserciones
//        String errorCartelAsociarPatente = miCuenta.errorAsociarPatente();
//        Assert.assertTrue(errorCartelAsociarPatente.contains("Ingrese una patente v�lida"));
//        global.log().info("Se valida cartel de error en la asociaci�n de patente. ");
//
//    }

//    @Test(description = "143-Visualizar Panel para Asociar Patente - Cliente FOB", dataProvider = "miCuentaCases")
//    public void visualizarBtnAsociarPatenteClienteFob(String sUsername, String sPassword) throws InterruptedException {
//
//        global.log().info("Inicia el caso de prueba");
//        LoginPage login = new LoginPage(driver);
//        IndexPage index = new IndexPage(driver);
//        MiCuentaPage miCuenta = new MiCuentaPage(driver);
//
//        // Acciones
//        login.executeLogin(sUsername, sPassword);
//        global.log().info("Se valid� correctamente el inicio de sesi�n.");
//        index.seccionMiCuenta();
//        miCuenta.irSeccionPatentes();
//
//        // Aserciones
//        Assert.assertTrue(miCuenta.buttonAsociarPatente(), "Valido que se encuentre el bot�n de asociar patente.");
//        global.log().info("Se valida que se encuentre la opci�n de Asociar Patente. ");
//
//    }

//    @Test(description = "144-No Visualizar Panel para Asociar Patente - Cliente CIF", dataProvider = "miCuentaCases")
//    public void visualizarBtnAsociarPatenteClienteCif(String sUsername, String sPassword) throws InterruptedException {
//
//        global.log().info("Inicia el caso de prueba");
//        LoginPage login = new LoginPage(driver);
//        IndexPage index = new IndexPage(driver);
//        MiCuentaPage miCuenta = new MiCuentaPage(driver);
//
//        // Acciones
//        login.executeLogin(sUsername, sPassword);
//        global.log().info("Se valid� correctamente el inicio de sesi�n.");
//        index.seccionMiCuenta();
//
//        // Aserciones
//        Assert.assertFalse(miCuenta.buttonAsociarPatente(),"Se valida que no aparezca la opci�n de Asociar Patente.");
//        global.log().info("Se valida que no aparezca la opci�n de Asociar Patente.");
//
//    }


}
