package Portal;

import com.everis.utilities.ExcelUtils;
import com.everis.global.Global;
import com.everis.po.CuentaCorrientePage;
import com.everis.po.IndexPage;
import com.everis.po.LoginPage;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.List;


public class CuentaCorrienteTest extends Base {

    Global global = new Global(driver);

    @DataProvider
    public Object[][] CuentaCorrienteCases(ITestContext context) throws Exception {
        Object[][] testObjArray = ExcelUtils.getTableArray(
                super.getDataProviderDir() + context.getCurrentXmlTest().getParameter("dataProvider"),
                context.getCurrentXmlTest().getParameter("sheet"));
        return (testObjArray);
    }

    @Test(description = "180-Visualizacion cuenta corriente - Partidas Abiertas.", dataProvider = "CuentaCorrienteCases")
    public void cuentaCorrientePartidasAbiertas(String sUsername, String sPassword) throws Exception {

        global.log().info("Inicia el caso de prueba");
        LoginPage login = new LoginPage(driver);
        IndexPage index = new IndexPage(driver);

        // Acciones
        login.executeLogin(sUsername, sPassword);
        global.log().info("Se valido correctamente el inicio de sesion.");
        index.clickSeccionCuentaCorriente();

    }

    @Test(description = "181-Visualizacion cuenta corriente - Partidas Abiertas - Cambiar Desplegable ACC.", dataProvider = "CuentaCorrienteCases")
    public void cuentaCorrientePartidasAbiertasCambiarACC(String sUsername, String sPassword) throws Exception {

        global.log().info("Inicia el caso de prueba");
        LoginPage login = new LoginPage(driver);
        IndexPage index = new IndexPage(driver);
        CuentaCorrientePage cuentaCorriente = new CuentaCorrientePage(driver);

        // Acciones
        login.executeLogin(sUsername, sPassword);
        global.log().info("Se valid� correctamente el inicio de sesi�n.");
        index.clickSeccionCuentaCorriente();
        cuentaCorriente.validarAreasControlCredito();

    }

    @Test(description = "185-Partidas Abiertas - Visualizar Factura.", dataProvider = "CuentaCorrienteCases")
    public void cuentaCorrienteReferencia(String sUsername, String sPassword, String filtroAcc, String referencia) throws Exception {

        global.log().info("Inicia el caso de prueba");
        LoginPage login = new LoginPage(driver);
        IndexPage index = new IndexPage(driver);
        CuentaCorrientePage cuentaCorriente = new CuentaCorrientePage(driver);

        // Acciones
        login.executeLogin(sUsername, sPassword);
        global.log().info("Se valid� correctamente el inicio de sesi�n.");
        index.clickSeccionCuentaCorriente();
        cuentaCorriente.seleccionarACC(filtroAcc);
        cuentaCorriente.validarReferencia(referencia);
    }

    @Test(description = "213-Exportar excel - Partidas Abiertas - Filtro ACC-0001.", dataProvider = "CuentaCorrienteCases")
    public void cuentaCorrienteExportarExcelACC0001(String sUsername, String sPassword, String filtroAcc) throws Exception {

        global.log().info("Inicia el caso de prueba");
        LoginPage login = new LoginPage(driver);
        IndexPage index = new IndexPage(driver);
        CuentaCorrientePage cuentaCorriente = new CuentaCorrientePage(driver);

        // Acciones
        login.executeLogin(sUsername, sPassword);
        global.log().info("Se valid� correctamente el inicio de sesi�n.");
        index.clickSeccionCuentaCorriente();
        cuentaCorriente.seleccionarACC(filtroAcc);
        cuentaCorriente.exportarExcel();
    }

    @Test(description = "214-Exportar excel - Partidas Abiertas - Filtro ACC-0003.", dataProvider = "CuentaCorrienteCases")
    public void cuentaCorrienteExportarExcelACC0003(String sUsername, String sPassword, String filtroAcc) throws Exception {

        global.log().info("Inicia el caso de prueba");
        LoginPage login = new LoginPage(driver);
        IndexPage index = new IndexPage(driver);
        CuentaCorrientePage cuentaCorriente = new CuentaCorrientePage(driver);

        // Acciones
        login.executeLogin(sUsername, sPassword);
        global.log().info("Se valid� correctamente el inicio de sesi�n.");
        index.clickSeccionCuentaCorriente();
        cuentaCorriente.seleccionarACC(filtroAcc);
        cuentaCorriente.exportarExcel();
    }

    @Test(description = "215-Exportar excel - Partidas Abiertas - Filtro ACC-0008.", dataProvider = "CuentaCorrienteCases")
    public void cuentaCorrienteExportarExcelACC0008(String sUsername, String sPassword, String filtroAcc) throws Exception {

        global.log().info("Inicia el caso de prueba");
        LoginPage login = new LoginPage(driver);
        IndexPage index = new IndexPage(driver);
        CuentaCorrientePage cuentaCorriente = new CuentaCorrientePage(driver);

        // Acciones
        login.executeLogin(sUsername, sPassword);
        global.log().info("Se valid� correctamente el inicio de sesi�n.");
        index.clickSeccionCuentaCorriente();
        cuentaCorriente.seleccionarACC(filtroAcc);
        cuentaCorriente.exportarExcel();
    }

    @Test(description = "216-Exportar excel - Partidas Abiertas - Filtro ACC-0020.", dataProvider = "CuentaCorrienteCases")
    public void cuentaCorrienteExportarExcelACC0020(String sUsername, String sPassword, String filtroAcc) throws Exception {

        global.log().info("Inicia el caso de prueba");
        LoginPage login = new LoginPage(driver);
        IndexPage index = new IndexPage(driver);
        CuentaCorrientePage cuentaCorriente = new CuentaCorrientePage(driver);

        // Acciones
        login.executeLogin(sUsername, sPassword);
        global.log().info("Se valid� correctamente el inicio de sesi�n.");
        index.clickSeccionCuentaCorriente();
        cuentaCorriente.seleccionarACC(filtroAcc);
        cuentaCorriente.exportarExcel();
    }

    @Test(description = "217-Exportar excel - Partidas Abiertas - Filtro ACC-0022.", dataProvider = "CuentaCorrienteCases")
    public void cuentaCorrienteExportarExcelACC0022(String sUsername, String sPassword, String filtroAcc) throws Exception {

        global.log().info("Inicia el caso de prueba");
        LoginPage login = new LoginPage(driver);
        IndexPage index = new IndexPage(driver);
        CuentaCorrientePage cuentaCorriente = new CuentaCorrientePage(driver);

        // Acciones
        login.executeLogin(sUsername, sPassword);
        global.log().info("Se valid� correctamente el inicio de sesi�n.");
        index.clickSeccionCuentaCorriente();
        cuentaCorriente.seleccionarACC(filtroAcc);
        cuentaCorriente.exportarExcel();
    }

    @Test(description = "218-Exportar excel - Partidas Aplicadas  - Filtro ACC-0020.", dataProvider = "CuentaCorrienteCases")
    public void cuentaCorrienteExportarExcelPartidasAplicadas(String sUsername, String sPassword, String filtroAcc) throws Exception {

        global.log().info("Inicia el caso de prueba");
        LoginPage login = new LoginPage(driver);
        IndexPage index = new IndexPage(driver);
        CuentaCorrientePage cuentaCorriente = new CuentaCorrientePage(driver);

        // Acciones
        login.executeLogin(sUsername, sPassword);
        global.log().info("Se valid� correctamente el inicio de sesi�n.");
        index.clickSeccionCuentaCorriente();
        cuentaCorriente.clickPartidasAplicadas();
        cuentaCorriente.seleccionarACC(filtroAcc);
        cuentaCorriente.exportarExcel();

    }

    @Test(description = "219-Exportar excel - Cheques rechazados Historicos - Filtro ACC-0020.", dataProvider = "CuentaCorrienteCases")
    public void cuentaCorrienteExportarExcelChequesRechazados(String sUsername, String sPassword, String filtroAcc) throws Exception {

        global.log().info("Inicia el caso de prueba");
        LoginPage login = new LoginPage(driver);
        IndexPage index = new IndexPage(driver);
        CuentaCorrientePage cuentaCorriente = new CuentaCorrientePage(driver);

        // Acciones
        login.executeLogin(sUsername, sPassword);
        global.log().info("Se valid� correctamente el inicio de sesi�n.");
        index.clickSeccionCuentaCorriente();
        cuentaCorriente.clickChequesRechazados();
        cuentaCorriente.seleccionarACC(filtroAcc);
        cuentaCorriente.exportarExcel();

    }

    @Test(description = "245-Rechazo de campo fecha vac�o - partidas abiertas - filtro ACC predeterminado.", dataProvider = "CuentaCorrienteCases")
    public void rechazoFechaVaciaPartidasAbiertas(String sUsername, String sPassword) throws Exception {

        global.log().info("Inicia el caso de prueba");
        LoginPage login = new LoginPage(driver);
        IndexPage index = new IndexPage(driver);
        CuentaCorrientePage cuentaCorriente = new CuentaCorrientePage(driver);

        // Acciones
        login.executeLogin(sUsername, sPassword);
        global.log().info("Se valid� correctamente el inicio de sesi�n.");
        index.clickSeccionCuentaCorriente();
        cuentaCorriente.clickFiltrarFechas();

        //Assert
        Assert.assertEquals(cuentaCorriente.validarFechaInvalida(),"Fecha inv�lida");
    }

    @Test(description = "246-Rechazo de ambos campos fecha vac�os - partidas aplicadas - filtro ACC predeterminado.", dataProvider = "CuentaCorrienteCases")
    public void rechazoFechasVaciasPartidasAplicadas(String sUsername, String sPassword) throws Exception {

        global.log().info("Inicia el caso de prueba");
        LoginPage login = new LoginPage(driver);
        IndexPage index = new IndexPage(driver);
        CuentaCorrientePage cuentaCorriente = new CuentaCorrientePage(driver);

        // Acciones
        login.executeLogin(sUsername, sPassword);
        global.log().info("Se valid� correctamente el inicio de sesi�n.");
        index.clickSeccionCuentaCorriente();
        cuentaCorriente.clickPartidasAplicadas();
        cuentaCorriente.clickFiltrarFechas();
        List<String> fechasInvalidas = cuentaCorriente.validarFechasInvalidas();

        //Assert
        Assert.assertEquals(fechasInvalidas.get(0),"Fecha inv�lida");
        Assert.assertEquals(fechasInvalidas.get(1),"Fecha inv�lida");
    }

    @Test(description = "249-Rechazo de solo campo Fecha hasta vac�o - Partidas aplicadas - filtro predeterminado.", dataProvider = "CuentaCorrienteCases")
    public void rechazoFechaVaciaHastaPartidasAplicadas(String sUsername, String sPassword, String dia, String mes, String ano) throws Exception {

        global.log().info("Inicia el caso de prueba");
        LoginPage login = new LoginPage(driver);
        IndexPage index = new IndexPage(driver);
        CuentaCorrientePage cuentaCorriente = new CuentaCorrientePage(driver);

        // Acciones
        login.executeLogin(sUsername, sPassword);
        global.log().info("Se valid� correctamente el inicio de sesi�n.");
        index.clickSeccionCuentaCorriente();
        cuentaCorriente.clickPartidasAplicadas();
        cuentaCorriente.completarFechaHasta(dia, mes, ano);
        cuentaCorriente.clickFiltrarFechas();

        //Assert
        Assert.assertEquals(cuentaCorriente.validarFechaInvalida(),"Fecha inv�lida");
    }


    @Test(description = "253-Rechazo de ambas fechas inv�lidas - Cheques rechazados hist�ricos - Filtro ACC predeterminado.", dataProvider = "CuentaCorrienteCases")
    public void rechazoFechasInvalidasChequesRechazados(String sUsername, String sPassword, String diaDesde, String mesDesde, String anoDesde, String diaHasta, String mesHasta, String anoHasta) throws Exception {

        global.log().info("Inicia el caso de prueba");
        LoginPage login = new LoginPage(driver);
        IndexPage index = new IndexPage(driver);
        CuentaCorrientePage cuentaCorriente = new CuentaCorrientePage(driver);

        // Acciones
        login.executeLogin(sUsername, sPassword);
        global.log().info("Se valid� correctamente el inicio de sesi�n.");
        index.clickSeccionCuentaCorriente();
        cuentaCorriente.clickChequesRechazados();
        cuentaCorriente.completarFechaDesdeHasta(diaDesde, mesDesde, anoDesde, diaHasta, mesHasta, anoHasta);
        cuentaCorriente.clickFiltrarFechas();
        List<String> fechasInvalidas = cuentaCorriente.validarFechasInvalidas();

        //Assert
        Assert.assertEquals(fechasInvalidas.get(0),"Fecha inv�lida");
        Assert.assertEquals(fechasInvalidas.get(1),"Fecha inv�lida");

    }

    @Test(description = "254-Bot�n filtrar y datepicker v�lidos - partidas Abiertas - Filtro ACC predeterminado.", dataProvider = "CuentaCorrienteCases")
    public void FechasValidasPartidasAbiertas(String sUsername, String sPassword, String dia, String mes, String ano) throws Exception {

        global.log().info("Inicia el caso de prueba");
        LoginPage login = new LoginPage(driver);
        IndexPage index = new IndexPage(driver);
        CuentaCorrientePage cuentaCorriente = new CuentaCorrientePage(driver);

        // Acciones
        login.executeLogin(sUsername, sPassword);
        global.log().info("Se valid� correctamente el inicio de sesi�n.");
        index.clickSeccionCuentaCorriente();
        cuentaCorriente.completarFechaHasta(dia, mes, ano);

        //Assert

    }

    @Test(description = "255-Bot�n filtrar y datepicker v�lidos - partidas Aplicadas - Filtro ACC predeterminado.", dataProvider = "CuentaCorrienteCases")
    public void FechasValidasPartidasAplicadas(String sUsername, String sPassword, String diaDesde, String mesDesde, String anoDesde, String diaHasta, String mesHasta, String anoHasta) throws Exception {

        global.log().info("Inicia el caso de prueba");
        LoginPage login = new LoginPage(driver);
        IndexPage index = new IndexPage(driver);
        CuentaCorrientePage cuentaCorriente = new CuentaCorrientePage(driver);

        // Acciones
        login.executeLogin(sUsername, sPassword);
        global.log().info("Se valid� correctamente el inicio de sesi�n.");
        index.clickSeccionCuentaCorriente();
        cuentaCorriente.clickPartidasAplicadas();
        cuentaCorriente.completarFechaDesdeHasta(diaDesde, mesDesde, anoDesde, diaHasta, mesHasta, anoHasta);

        //Assert

    }


}

