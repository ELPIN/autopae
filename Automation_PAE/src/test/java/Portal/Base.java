package Portal;

import com.everis.global.Global;
import com.everis.po.HistorialPedidosPage;
import com.everis.utilities.GenerateWord;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.testng.ITestContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import static com.everis.utilities.Listener.onFinish;
import static com.everis.utilities.Listener.onTestStart;

public class Base {
	WebDriver driver;

	final Logger log = LogManager.getLogger(HistorialPedidosPage.class);

	private String seleniumUrl;
	private String driverPath;
	private String projectDir;
	private String dataProviderDir;
	private String driversDir;
	private GenerateWord generateWord = new GenerateWord();
	public Base() {
		this.projectDir = System.getProperty("user.dir");
		// this.dataProviderDir="D://repo//pae-ui//src//test//dataprovider//";
		this.dataProviderDir = projectDir + "//src//test//dataprovider//";
		this.driversDir = projectDir + "//src//test//resources//webDrivers//";
	}

	public String getProjectDir() {
		return projectDir;
	}

	public String getDataProviderDir() {
		return dataProviderDir;
	}

	public String getDriversDir() {
		return driversDir;
	}
	
	@BeforeMethod(alwaysRun = true)
	public void setupBeforeSuite(ITestContext context) {
		generateWord.startUpWord("EvidenciaAuto");
		onTestStart("scenario1");

		seleniumUrl = context.getCurrentXmlTest().getParameter("selenium.url");
		driverPath = driversDir + context.getCurrentXmlTest().getParameter("selenium.driver");

		try {
			if (driverPath.contains("geckodriver")) {
				System.setProperty("webdriver.gecko.driver", driverPath);

				FirefoxOptions ffOpt = new FirefoxOptions();
				ffOpt.setCapability("marionette", true);
				driver = new FirefoxDriver(ffOpt);

				Dimension d = new Dimension(1044, 744);
				driver.manage().window().setSize(d);
				Global.setDriverInUse("geckodriver");

				//driver.manage().window().maximize();

			} else {
				System.setProperty("webdriver.chrome.driver", driverPath);
				// Initialize browser
				driver = new ChromeDriver();
				// driver.manage().window().maximize();

				Dimension d = new Dimension(1044, 744);
				driver.manage().window().setSize(d);
				Global.setDriverInUse("chromedriver");

				// driver.manage().window().fullscreen();

				// Accede a cada dimensión individualmente
				/*
				 * int width = driver.manage().window().getSize().getWidth(); int height =
				 * driver.manage().window().getSize().getHeight(); log.info(width);
				 * log.info(height);
				 */

			}

			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		} catch (Exception e) {
			log.info(e.getMessage());
			throw new IllegalStateException("Can't start selenium browser", e);
		}
		driver.get(seleniumUrl);

	}

	@AfterMethod(alwaysRun = true)
	public void setupAfterSuite() {

		try {
			generateWord.endToWord("s");
		} catch (IOException e) {
			e.printStackTrace();
		}
		onFinish();

		driver.quit();
	}

}
