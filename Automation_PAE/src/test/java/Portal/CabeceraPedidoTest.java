package Portal;

import com.everis.utilities.ExcelUtils;
import com.everis.global.Global;
import com.everis.po.CabeceraPedidoPage;
import com.everis.po.IndexPage;
import com.everis.po.LoginPage;
import com.everis.po.ProductosDisponiblesPage;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CabeceraPedidoTest extends Base {

	Global global = new Global(driver);

	@DataProvider
	public Object[][] cabeceraPedidosCases(ITestContext context) throws Exception {
		Object[][] testObjArray = ExcelUtils.getTableArray(
				super.getDataProviderDir() + context.getCurrentXmlTest().getParameter("dataProvider"),
				context.getCurrentXmlTest().getParameter("sheet"));
		return (testObjArray);
	}

	@Test(description = "20-Fecha preferente de Despacho igual a fecha del d�a", dataProvider = "cabeceraPedidosCases")
	public void fechaDespachoIgualFechaDia(String sUsername, String sPassword) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		CabeceraPedidoPage cabeceraPedido = new CabeceraPedidoPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		cabeceraPedido.completarFechaPedido(cabeceraPedido.getObtenerFechaActual());

		// Aserciones
		String errorCartelFechaDespacho = cabeceraPedido.errorItemCartelFechaDespacho();
		Assert.assertTrue(errorCartelFechaDespacho.contains("Por favor ingrese una fecha preferente de despacho."));

	}

	@Test(description = "32-Cliente CIF�- Lista de Patentes", dataProvider = "cabeceraPedidosCases")
	public void clienteCifPatentes(String sUsername, String sPassword) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		CabeceraPedidoPage cabeceraPedido = new CabeceraPedidoPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");

		// Aserciones
		Assert.assertTrue(cabeceraPedido.disabledPatenteIsPresent(), "Valido que el campo patente est� deshabilitado");
		global.log().info("Se valida que el campo patente est� grisado.�");
	}

	@Test(description = "36-Ingreso Orden de Compra�en la venta", dataProvider = "cabeceraPedidosCases")
	public void ingresarOrdenCompra(String sUsername, String sPassword, String canalVenta, String patente,
			String ordenCompra) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		CabeceraPedidoPage cabeceraPedido = new CabeceraPedidoPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		cabeceraPedido.completarCabeceraPedidoVta(canalVenta, patente, ordenCompra);
		global.log().info("Se despliega lista de Productos Disponibles.");

	}

	@Test(description = "37-Sin ingreso Orden de Compra en la venta", dataProvider = "cabeceraPedidosCases")
	public void sinIngresorOrdenCompra(String sUsername, String sPassword, String canalVenta, String patente)
			throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		CabeceraPedidoPage cabeceraPedido = new CabeceraPedidoPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		cabeceraPedido.seleccionarCanalVenta(canalVenta);
		cabeceraPedido.completarFechaPedido(cabeceraPedido.getObtenerFechaValidaPedido());
		cabeceraPedido.seleccionarPatente(patente);
		cabeceraPedido.clickbtnContinuar();

		// Aserciones
		String cartelProductosDisponibles = productosDisponibles.cartelProductosDisponibles();
		Assert.assertTrue(cartelProductosDisponibles.contains("Productos disponibles"));

	}

	@Test(description = "38-Bot�n Continuar�- Completando todos los campos obligatorios", dataProvider = "cabeceraPedidosCases")
	public void completarCamposObligatorios(String sUsername, String sPassword, String canalVenta, String patente,
			String ordenCompra) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		CabeceraPedidoPage cabeceraPedido = new CabeceraPedidoPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		cabeceraPedido.completarCabeceraPedidoVta(canalVenta, patente, ordenCompra);
		global.log().info("Se despliega lista de Productos Disponibles.");

	}

	@Test(description = "39-Bot�n Continuar�- Completando algunos campos obligatorios", dataProvider = "cabeceraPedidosCases")
	public void sinCompletarCamposObligatorios(String sUsername, String sPassword) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		CabeceraPedidoPage cabeceraPedido = new CabeceraPedidoPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		cabeceraPedido.clickbtnContinuar();

		// Aserciones
		String errorCartelFechaDespacho = cabeceraPedido.errorItemCartelFechaDespacho();
		Assert.assertTrue(errorCartelFechaDespacho.contains("Por favor ingrese una fecha preferente de despacho."));
		String errorCartelCanalVta = cabeceraPedido.errorItemCartelCanalVta();
		Assert.assertTrue(errorCartelCanalVta.contains("Por favor ingrese un canal de ventas."));
		String errorCartelPatente = cabeceraPedido.errorItemCartelPatente();
		Assert.assertTrue(errorCartelPatente.contains("Por favor ingrese una patente."));
		global.log().info("Se visualizan carteles de error por campos obligatorios imcompletos.");

	}

	@Test(description = "40-Bot�n Continuar�- Bot�n Actualizar", dataProvider = "cabeceraPedidosCases")
	public void visualizarBtnActualizar(String sUsername, String sPassword, String canalVenta, String patente,
			String ordenCompra) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		CabeceraPedidoPage cabeceraPedido = new CabeceraPedidoPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		cabeceraPedido.completarCabeceraPedidoVta(canalVenta, patente, ordenCompra);

		// Aserciones
		String btnVisualizarIsPresent = cabeceraPedido.btnActualizar();
		Assert.assertTrue(btnVisualizarIsPresent.contains("Actualizar"));
		global.log().info("Se visualiza bot�n actualizar.");

	}

	//Prueba de git

}
