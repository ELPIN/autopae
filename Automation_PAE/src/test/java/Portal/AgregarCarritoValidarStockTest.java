package Portal;

import com.everis.utilities.ExcelUtils;
import com.everis.global.Global;
import com.everis.po.*;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class AgregarCarritoValidarStockTest extends Base {

	Global global = new Global(driver);

	@DataProvider
	public Object[][] agregarCarritoValidarStockCases(ITestContext context) throws Exception {
		Object[][] testObjArray = ExcelUtils.getTableArray(
				super.getDataProviderDir() + context.getCurrentXmlTest().getParameter("dataProvider"),
				context.getCurrentXmlTest().getParameter("sheet"));
		return (testObjArray);
	}

	@Test(description = "59-Bot�n Agregar�- sin cantidad ingresada - sin contrato - cliente CIF", dataProvider = "agregarCarritoValidarStockCases")
	public void btnAgregarSinCantidad(String sUsername, String sPassword, String canalVenta, String producto)
			throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		CabeceraPedidoPage cabeceraPedido = new CabeceraPedidoPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		cabeceraPedido.completarFechaPedido(cabeceraPedido.getObtenerFechaValidaPedido());
		cabeceraPedido.seleccionarCanalVenta(canalVenta);
		cabeceraPedido.clickbtnContinuar();
		String cartelProductosDisponibles = productosDisponibles.cartelProductosDisponibles();
		Assert.assertTrue(cartelProductosDisponibles.contains("Productos disponibles"));
		global.log().info("Se despliega lista de Productos Disponibles.");
		productosDisponibles.agregarProducto(producto);

		// Aserciones
		String errorCartelCantidad = productosDisponibles.cartelErrorCantidad();
		Assert.assertTrue(errorCartelCantidad.contains("Debe indicar la cantidad a solicitar"));
		global.log().info("Se visualiza cartel de error de: 'Debe indicar la cantidad a solicitar'. ");
	}

	@Test(description = "61-Bot�n Agregar�- Con contrato y cantidad producto mayor remanente contrato - Cliente CIF", dataProvider = "agregarCarritoValidarStockCases")
	public void btnAgregarConContratoCantidadMayorDisponible(String sUsername, String sPassword, String canalVenta,
			String contrato, String ordenCompra, String cantidad, String codigo) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		CabeceraPedidoPage cabeceraPedido = new CabeceraPedidoPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		cabeceraPedido.completarCabeceraPedidoVtaCifConContrato(canalVenta, contrato, ordenCompra);
		global.log().info("Se despliega lista de Productos Disponibles.");
		productosDisponibles.agregarCantidad(cantidad, codigo);
		productosDisponibles.agregarProducto(codigo);
		global.log().info("Se agrega una cantidad mayor del producto a la disponible.");

		// Aserciones
		String errorCartelCantidad = productosDisponibles.cartelErrorCantidadInsuficiente();
		Assert.assertTrue(errorCartelCantidad.contains("Atenci�n"));
		global.log().info("Se visualiza cartel de error de: 'Debe indicar la cantidad a solicitar'. ");
	}

	@Test(description = "62-Bot�n Agregar�- Con contrato y cantidad producto menor o igual remanente contrato - cliente CIF", dataProvider = "agregarCarritoValidarStockCases")
	public void btnAgregarConContratoCantidadMenorDisponible(String sUsername, String sPassword, String canalVenta,
			String contrato, String ordenCompra, String cantidad, String codigo) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		CabeceraPedidoPage cabeceraPedido = new CabeceraPedidoPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);
		CarritoPage carrito = new CarritoPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		cabeceraPedido.completarCabeceraPedidoVtaCifConContrato(canalVenta, contrato, ordenCompra);
		global.log().info("Se despliega lista de Productos Disponibles.");
		productosDisponibles.agregarCantidad(cantidad, codigo);
		productosDisponibles.agregarProducto(codigo);
		global.log().info("Se agrega una cantidad menor del producto a la disponible.");
		productosDisponibles.validarStockConContrato();

		// Aserciones
		String btnGenerarPedido = carrito.bntGenerarPedido();
		Assert.assertTrue(btnGenerarPedido.contains("Generar Pedido"));
		global.log().info("Se visualiza bot�n de Generar Pedido. ");
	}

	@Test(description = "64-Bot�n Agregar�- stock no es suficiente y es obsoleto", dataProvider = "agregarCarritoValidarStockCases")
	public void stockNoSuficienteObsoleto(String sUsername, String sPassword, String canalVenta, String patente,
			String ordenCompra, String codigo, String cantidad) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		CabeceraPedidoPage cabeceraPedido = new CabeceraPedidoPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		cabeceraPedido.completarCabeceraPedidoVta(canalVenta, patente, ordenCompra);
		global.log().info("Se despliega lista de Productos Disponibles.");
		productosDisponibles.buscadorProductos(codigo);
		global.log().info("Se busc� el producto satisfactoriamente.");
		productosDisponibles.validarProductoPorIdentificador(codigo);
		global.log().info("Se valida el producto buscado por c�digo. ");
		productosDisponibles.agregarCantidad(cantidad, codigo);
		global.log().info("Se agrega una cantidad mayor a la disponible el producto obsoleto. ");
		productosDisponibles.agregarProducto(codigo);
		// Aserciones
		String errorCartelCentroAtencion = productosDisponibles.cartelLlamadoCentroAtencion();
		Assert.assertTrue(errorCartelCentroAtencion.contains(
				"Solo la cantidad indicada est� disponible para la venta. Consulte por un producto de referencia llamando a nuestro Centro de Atenci�n 0800-555-3776"));
		global.log().info(
				"Se visualiza cartel de error de:  'Solo la cantidad indicada est� disponible para la venta. Consulte por un producto de referencia llamando a nuestro Centro de Atenci�n...'");
	}

	@Test(description = "66-Bot�n Agregar�- Stock disponible e insuficiente", dataProvider = "agregarCarritoValidarStockCases")
	public void btnAgregarStockDisponibleNoSuficiente(String sUsername, String sPassword, String canalVenta,
			String patente, String ordenCompra, String codigo, String cantidad) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		CabeceraPedidoPage cabeceraPedido = new CabeceraPedidoPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		cabeceraPedido.completarCabeceraPedidoVta(canalVenta, patente, ordenCompra);
		global.log().info("Se despliega lista de Productos Disponibles.");
		productosDisponibles.buscadorProductos(codigo);
		global.log().info("Se busc� el producto satisfactoriamente.");
		productosDisponibles.validarProductoPorIdentificador(codigo);
		global.log().info("Se valida el producto buscado por c�digo. ");
		productosDisponibles.agregarCantidad(cantidad, codigo);
		global.log().info("Se agrega una cantidad mayor del producto a la disponible.");
		productosDisponibles.agregarProducto(codigo);
		// Aserciones
		String pedidoReservaCartel = productosDisponibles.cartelGenerarPedidoReserva();
		Assert.assertTrue(pedidoReservaCartel
				.contains("La cantidad ingresada no est� disponible �quiere generar un Pedido de Reserva?"));
		global.log().info(
				"Se visualiza cartel de:  'La cantidad ingresada no est� disponible �quiere generar un Pedido de Reserva?'");

	}

	@Test(description = "67-Mensaje de Back�Order�- Se acepta", dataProvider = "agregarCarritoValidarStockCases")
	public void mensajeBackOrder(String sUsername, String sPassword, String canalVenta, String patente,
			String ordenCompra, String codigo, String cantidad) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		CabeceraPedidoPage cabeceraPedido = new CabeceraPedidoPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		cabeceraPedido.completarCabeceraPedidoVta(canalVenta, patente, ordenCompra);
		global.log().info("Se despliega lista de Productos Disponibles.");
		productosDisponibles.buscadorProductos(codigo);
		global.log().info("Se busc� el producto satisfactoriamente.");
		productosDisponibles.validarProductoPorIdentificador(codigo);
		global.log().info("Se valida el producto buscado por c�digo. ");
		productosDisponibles.agregarCantidad(cantidad, codigo);
		global.log().info("Se agrega una cantidad mayor del producto a la disponible.");
		productosDisponibles.agregarProducto(codigo);
		String pedidoReservaCartel = productosDisponibles.cartelGenerarPedidoReserva();
		Assert.assertTrue(pedidoReservaCartel
				.contains("La cantidad ingresada no est� disponible �quiere generar un Pedido de Reserva?"));
		global.log().info(
				"Se visualiza cartel de:  'La cantidad ingresada no est� disponible �quiere generar un Pedido de Reserva?'");
		productosDisponibles.btnReservarCantidad();
		productosDisponibles.quitarProducto(codigo);
		global.log().info("Se visualiza el bot�n de Quitar y se presiona.");

	}

	@Test(description = "68-Mensaje de Back�Order�- No se acepta", dataProvider = "agregarCarritoValidarStockCases")
	public void mensajeBackOrderNoAcepta(String sUsername, String sPassword, String canalVenta, String patente,
			String ordenCompra, String codigo, String cantidad) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		CabeceraPedidoPage cabeceraPedido = new CabeceraPedidoPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		cabeceraPedido.completarCabeceraPedidoVta(canalVenta, patente, ordenCompra);
		global.log().info("Se despliega lista de Productos Disponibles.");
		productosDisponibles.buscadorProductos(codigo);
		global.log().info("Se busc� el producto satisfactoriamente.");
		productosDisponibles.validarProductoPorIdentificador(codigo);
		global.log().info("Se valida el producto buscado por c�digo. ");
		productosDisponibles.agregarCantidad(cantidad, codigo);
		global.log().info("Se agrega una cantidad mayor del producto a la disponible.");
		productosDisponibles.agregarProducto(codigo);
		String pedidoReservaCartel = productosDisponibles.cartelGenerarPedidoReserva();
		Assert.assertTrue(pedidoReservaCartel
				.contains("La cantidad ingresada no est� disponible �quiere generar un Pedido de Reserva?"));
		global.log().info(
				"Se visualiza cartel de:  'La cantidad ingresada no est� disponible �quiere generar un Pedido de Reserva?'");
		productosDisponibles.btnQuitarDelPedido();

		// Aserciones
		String AgregarBtn = productosDisponibles.btnAgregar();
		Assert.assertTrue(AgregarBtn.contains("Agregar"));
		global.log().info("Se visualiza el bot�n de Agregar.");

	}

	@Test(description = "71-Revisar Carrito�- Volver al carrito", dataProvider = "agregarCarritoValidarStockCases")
	public void revisarCarritoVolverCarrito(String sUsername, String sPassword, String canalVenta, String patente,
			String ordenCompra, String codigo, String cantidad) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		CabeceraPedidoPage cabeceraPedido = new CabeceraPedidoPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);
		CarritoPage carrito = new CarritoPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		cabeceraPedido.completarCabeceraPedidoVta(canalVenta, patente, ordenCompra);
		global.log().info("Se despliega lista de Productos Disponibles.");
		productosDisponibles.buscadorProductos(codigo);
		global.log().info("Se busc� el producto satisfactoriamente.");
		productosDisponibles.validarProductoPorIdentificador(codigo);
		global.log().info("Se valida el producto buscado por c�digo. ");
		productosDisponibles.agregarCantidad(cantidad, codigo);
		global.log().info("Se agrega una cantidad menor del producto a la disponible.");
		productosDisponibles.agregarProducto(codigo);
		Assert.assertTrue(carrito.carritoIsDisplayed(), "Se valida que el carrito est� habilitado.");
		carrito.carritoCompra();
		carrito.btnAgregarProductos();
		String AgregarBtn = productosDisponibles.btnAgregar();
		Assert.assertTrue(AgregarBtn.contains("Agregar"));
		productosDisponibles.quitarProducto(codigo);
		global.log().info(
				"Se redirige a la secci�n de Productos Disponibles y se quita la cantidad agregada previamente al producto.");
	}

	@Test(description = "72-Revisar Carrito�- Agregar m�s productos", dataProvider = "agregarCarritoValidarStockCases")
	public void agregarMasProductosCarrito(String sUsername, String sPassword, String canalVenta, String patente,
			String ordenCompra, String codigo, String cantidad) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		CabeceraPedidoPage cabeceraPedido = new CabeceraPedidoPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);
		CarritoPage carrito = new CarritoPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		cabeceraPedido.completarCabeceraPedidoVta(canalVenta, patente, ordenCompra);
		global.log().info("Se despliega lista de Productos Disponibles.");
		productosDisponibles.buscadorProductos(codigo);
		global.log().info("Se busc� el producto satisfactoriamente.");
		productosDisponibles.validarProductoPorIdentificador(codigo);
		global.log().info("Se valida el producto buscado por c�digo. ");
		productosDisponibles.agregarCantidad(cantidad, codigo);
		global.log().info("Se agrega una cantidad menor del producto a la disponible.");
		productosDisponibles.agregarProducto(codigo);
		Assert.assertTrue(carrito.carritoIsDisplayed(), "Se valida que el carrito est� habilitado.");
		carrito.carritoCompra();
		carrito.btnAgregarProductos();
		String AgregarBtn = productosDisponibles.btnAgregar();
		Assert.assertTrue(AgregarBtn.contains("Agregar"));
		productosDisponibles.quitarProducto(codigo);
		global.log().info(
				"Se redirige a la secci�n de Productos Disponibles y se quita la cantidad agregada previamente al producto.");
	}

	@Test(description = "73-Revisar Carrito�- modificar uno o m�s productos", dataProvider = "agregarCarritoValidarStockCases")
	public void revisarCarritoModificar(String sUsername, String sPassword, String canalVenta, String patente,
			String ordenCompra, String codigo, String cantidad) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		CabeceraPedidoPage cabeceraPedido = new CabeceraPedidoPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);
		CarritoPage carrito = new CarritoPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		cabeceraPedido.completarCabeceraPedidoVta(canalVenta, patente, ordenCompra);
		global.log().info("Se despliega lista de Productos Disponibles.");
		productosDisponibles.buscadorProductos(codigo);
		global.log().info("Se busc� el producto satisfactoriamente.");
		productosDisponibles.validarProductoPorIdentificador(codigo);
		global.log().info("Se valida el producto buscado por c�digo. ");
		productosDisponibles.agregarCantidad(cantidad, codigo);
		global.log().info("Se agrega una cantidad menor del producto a la disponible.");
		productosDisponibles.agregarProducto(codigo);
		Assert.assertTrue(carrito.carritoIsDisplayed(), "Se valida que el carrito est� habilitado.");
		carrito.carritoCompra();
		carrito.btnModificarProductos();
		Assert.assertTrue(carrito.btnModificarSeleccionadosIsDisplayed(),
				"Se valida que el bot�n Modificar Seleccionados est� habilitado.");
		global.log().info("Se valida que el bot�n Modificar Seleccionados est� habilitado.");
		carrito.cancelarPedido();
		// Aserciones
		Assert.assertTrue(index.headerIsVisible(), "Valido si el mensaje de bienvenida aparecio correctamente.");
		Assert.assertEquals(index.getHeaderText(), "�Bienvenid@!");

	}

	@Test(description = "74-Revisar Carrito - Abrir resumen del carrito", dataProvider = "agregarCarritoValidarStockCases")
	public void revisarCarritoResumen(String sUsername, String sPassword, String canalVenta, String patente,
			String ordenCompra, String codigo, String cantidad) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		CabeceraPedidoPage cabeceraPedido = new CabeceraPedidoPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);
		CarritoPage carrito = new CarritoPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		cabeceraPedido.completarCabeceraPedidoVta(canalVenta, patente, ordenCompra);
		global.log().info("Se despliega lista de Productos Disponibles.");
		productosDisponibles.buscadorProductos(codigo);
		global.log().info("Se busc� el producto satisfactoriamente.");
		productosDisponibles.validarProductoPorIdentificador(codigo);
		global.log().info("Se valida el producto buscado por c�digo. ");
		productosDisponibles.agregarCantidad(cantidad, codigo);
		global.log().info("Se agrega una cantidad menor del producto a la disponible.");
		productosDisponibles.agregarProducto(codigo);
		Assert.assertTrue(carrito.carritoIsDisplayed(), "Se valida que el carrito est� habilitado.");
		carrito.carritoCompra();
		// Aserciones
		String cartelTotalYPeso = carrito.cartelTotalYPeso();
		Assert.assertTrue(cartelTotalYPeso.contains("Precio Neto :"));
		Assert.assertTrue(cartelTotalYPeso.contains("Peso KG :"));
		global.log().info("Se muestran los totales de Precio y Peso.");

	}

	@Test(description = "75-Revisar Carrito�- Eliminar productos", dataProvider = "agregarCarritoValidarStockCases")
	public void revisarCarritoEliminarProductos(String sUsername, String sPassword, String canalVenta, String patente,
			String ordenCompra, String codigo, String cantidad) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		CabeceraPedidoPage cabeceraPedido = new CabeceraPedidoPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);
		CarritoPage carrito = new CarritoPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		cabeceraPedido.completarCabeceraPedidoVta(canalVenta, patente, ordenCompra);
		global.log().info("Se despliega lista de Productos Disponibles.");
		productosDisponibles.buscadorProductos(codigo);
		global.log().info("Se busc� el producto satisfactoriamente.");
		productosDisponibles.validarProductoPorIdentificador(codigo);
		global.log().info("Se valida el producto buscado por c�digo. ");
		productosDisponibles.agregarCantidad(cantidad, codigo);
		global.log().info("Se agrega una cantidad menor del producto a la disponible.");
		productosDisponibles.agregarProducto(codigo);
		Assert.assertTrue(carrito.carritoIsDisplayed(), "Se valida que el carrito est� habilitado.");
		carrito.carritoCompra();
		carrito.eliminarProducto();
		global.log().info("Se elimina el producto del carrito");

		// Aserciones
		Assert.assertTrue(index.headerIsVisible(), "Valido si el mensaje de bienvenida aparecio correctamente.");
		Assert.assertEquals(index.getHeaderText(), "�Bienvenid@!");

	}

	@Test(description = "76-Revisar Carrito�- cancelar carrito", dataProvider = "agregarCarritoValidarStockCases")
	public void cancelarCarrito(String sUsername, String sPassword, String canalVenta, String patente,
			String ordenCompra, String codigo, String cantidad) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		CabeceraPedidoPage cabeceraPedido = new CabeceraPedidoPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);
		CarritoPage carrito = new CarritoPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		cabeceraPedido.completarCabeceraPedidoVta(canalVenta, patente, ordenCompra);
		global.log().info("Se despliega lista de Productos Disponibles.");
		productosDisponibles.buscadorProductos(codigo);
		global.log().info("Se busc� el producto satisfactoriamente.");
		productosDisponibles.validarProductoPorIdentificador(codigo);
		global.log().info("Se valida el producto buscado por c�digo. ");
		productosDisponibles.agregarCantidad(cantidad, codigo);
		global.log().info("Se agrega una cantidad menor del producto a la disponible.");
		productosDisponibles.agregarProducto(codigo);
		Assert.assertTrue(carrito.carritoIsDisplayed(), "Se valida que el carrito est� habilitado.");
		carrito.carritoCompra();
		carrito.cancelarPedido();
		// Aserciones
		Assert.assertTrue(index.headerIsVisible(), "Valido si el mensaje de bienvenida aparecio correctamente.");
		Assert.assertEquals(index.getHeaderText(), "�Bienvenid@!");

	}

	@Test(description = "77-Revisar Carrito�- Generar pedido", dataProvider = "agregarCarritoValidarStockCases")
	public void revisarCarritoGenerarPedido(String sUsername, String sPassword, String canalVenta, String patente,
			String ordenCompra, String codigo, String cantidad) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		CabeceraPedidoPage cabeceraPedido = new CabeceraPedidoPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);
		CarritoPage carrito = new CarritoPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		cabeceraPedido.completarCabeceraPedidoVta(canalVenta, patente, ordenCompra);
		global.log().info("Se despliega lista de Productos Disponibles.");
		productosDisponibles.buscadorProductos(codigo);
		global.log().info("Se busc� el producto satisfactoriamente.");
		productosDisponibles.validarProductoPorIdentificador(codigo);
		global.log().info("Se valida el producto buscado por c�digo. ");
		productosDisponibles.agregarCantidad(cantidad, codigo);
		global.log().info("Se agrega una cantidad menor del producto a la disponible.");
		productosDisponibles.agregarProducto(codigo);
		Assert.assertTrue(carrito.carritoIsDisplayed(), "Se valida que el carrito est� habilitado.");
		carrito.carritoCompra();
		carrito.generarPedidoCarrito();
		
		// Aserciones
		String resumenCarrito = carrito.resumenCarrito();
		Assert.assertTrue(resumenCarrito
				.contains("Los siguientes productos generar�n un Pedido para la fecha preferente de despacho:"));
		global.log().info("Sistema lleva a la�p�gina�de Resumen de Carrito");
	}

	@Test(description = "78-Resumen Carrito�- Ver resumen carrito", dataProvider = "agregarCarritoValidarStockCases")
	public void resumenCarritoVerResumen(String sUsername, String sPassword, String canalVenta, String patente,
			String ordenCompra, String codigoStock, String codigoSinStock, String cantidad) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		CabeceraPedidoPage cabeceraPedido = new CabeceraPedidoPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);
		CarritoPage carrito = new CarritoPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		cabeceraPedido.completarCabeceraPedidoVta(canalVenta, patente, ordenCompra);
		global.log().info("Se despliega lista de Productos Disponibles.");
		productosDisponibles.agregarCantidad(cantidad, codigoStock);
		global.log().info("Se agrega una cantidad menor del producto " + codigoStock + " a la disponible.");
		productosDisponibles.agregarProducto(codigoStock);
		productosDisponibles.agregarCantidad(cantidad, codigoSinStock);
		global.log().info("Se agrega una cantidad menor del producto " + codigoSinStock + " a la disponible.");
		productosDisponibles.agregarProducto(codigoSinStock);
		productosDisponibles.btnReservarCantidad();
		global.log().info("Se reserva la cantidad. ");
		Assert.assertTrue(carrito.carritoIsDisplayed(), "Se valida que el carrito est� habilitado.");
		carrito.carritoCompra();

		// Aserciones
		Assert.assertTrue(carrito.validarResumenProductosCarrito(codigoStock, codigoSinStock, cantidad),
				"Se valida que el resumen de los productos sea correcto");
		global.log().info("Se valida resumen de productos con stock " + codigoStock + " y sin stock " + codigoSinStock);
	}

	@Test(description = "79-Resumen Carrito�- productos disponibles", dataProvider = "agregarCarritoValidarStockCases")
	public void resumenProductosDisponibles(String sUsername, String sPassword, String canalVenta, String patente,
			String ordenCompra, String codigoStock, String cantidad) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		CabeceraPedidoPage cabeceraPedido = new CabeceraPedidoPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);
		CarritoPage carrito = new CarritoPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		cabeceraPedido.completarCabeceraPedidoVta(canalVenta, patente, ordenCompra);
		global.log().info("Se despliega lista de Productos Disponibles.");
		productosDisponibles.agregarCantidad(cantidad, codigoStock);
		global.log().info("Se agrega una cantidad menor del producto " + codigoStock + " a la disponible.");
		productosDisponibles.agregarProducto(codigoStock);
		carrito.generarPedido();
		String resumenCarrito = carrito.resumenCarrito();
		Assert.assertTrue(resumenCarrito
				.contains("Los siguientes productos generar�n un Pedido para la fecha preferente de despacho:"));
		global.log().info("Se muestra el resumen de los datos del pedido. ");
		productosDisponibles.clickbtnProductosDisponiblesBack();

		// Aserciones
		String cartelProductosDisponibles = productosDisponibles.cartelProductosDisponibles();
		Assert.assertTrue(cartelProductosDisponibles.contains("Productos disponibles"));
		global.log().info("El sistema vuelve a la pantalla de productos satisfactoriamente. ");

	}

	@Test(description = "86-Fecha preferente de Despacho ma�ana y es antes del horario de corte al��confirma el pedido��", dataProvider = "agregarCarritoValidarStockCases")
	public void fechaPreferenteMananaAntesCorte(String sUsername, String sPassword, String canalVenta,
			String codigoStock, String cantidad) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		CabeceraPedidoPage cabeceraPedido = new CabeceraPedidoPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		cabeceraPedido.validarHoraCorte(canalVenta, cantidad, codigoStock);

	}

}
