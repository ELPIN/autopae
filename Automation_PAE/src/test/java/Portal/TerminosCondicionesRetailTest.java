package Portal;

import com.everis.global.Global;
import com.everis.po.AprobacionesPage;
import com.everis.po.IndexPage;
import com.everis.po.LoginPage;
import com.everis.utilities.ExcelUtils;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class TerminosCondicionesRetailTest extends Base {

	Global global = new Global(driver);

	@DataProvider
	public Object[][] TerminosCondicionesRetailCases(ITestContext context) throws Exception {
		Object[][] testObjArray = ExcelUtils.getTableArray(
				super.getDataProviderDir() + context.getCurrentXmlTest().getParameter("dataProvider"),
				context.getCurrentXmlTest().getParameter("sheet"));
		return (testObjArray);
	}

	@Test(description = "163-Terminos y Condiciones - Mostrar notificaciones pendientes - Usuario Maestro", dataProvider = "TerminosCondicionesRetailCases")
	public void tyCmostrarNotificacionesPendientesUserMaster(String sUsername, String sPassword) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");

		// Aserciones
		Assert.assertTrue(index.validarIconoNotificaciones(), "Se valida �cono de Campana de Notificaciones. ");
		global.log().info(
				"El usuario: " + sUsername + " tiene: " + index.getIconoNotificacionesText() + " notificaciones.");

	}

	@Test(description = "164-Terminos y Condiciones - Mostrar notificaciones pendientes - Usuario no Maestro", dataProvider = "TerminosCondicionesRetailCases")
	public void tyCnotificacionesPendientesUserNoMaster(String sUsername, String sPassword) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");

		// Aserciones
		Assert.assertFalse(index.validarIconoNotificaciones(), "Se valida �cono de Campana de Notificaciones. ");
		global.log().info("Se valida que no aparezca �cono de Campana de Notificaciones. ");
		
		Assert.assertFalse(index.btnAprobaciones(),"Se valida que no aparezca la secci�n de Aprobaciones");
		global.log().info("Se valida que no aparezca la secci�n de Aprobaciones");
	
	}

	@Test(description = "165-Terminos y Condiciones - Sin notificaciones pendientes", dataProvider = "TerminosCondicionesRetailCases")
	public void tyCsinNotificaciones(String sUsername, String sPassword) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");

		// Aserciones
		index.validarNotificacionesPendientes();

	}

	@Test(description = "166-Terminos y Condiciones - Listar TyC pendientes por men� Secci�n 'Aprobaciones'", dataProvider = "TerminosCondicionesRetailCases")
	public void tyCmenuAprobaciones(String sUsername, String sPassword) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		AprobacionesPage aprobaciones = new AprobacionesPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.clickSeccionAprobaciones();

		// Aserciones
		Assert.assertTrue(aprobaciones.validarGrillaAprobaciones());
		global.log().info(
				"Se valid� que las notificaciones en la grilla coinciden con el n�mero indicado en la Campana de Notificaciones. ");

	}

	@Test(description = "167-Terminos y Condiciones - Listar TyC pendientes desde el icono de notificaciones pendientes", dataProvider = "TerminosCondicionesRetailCases")
	public void tyCiconoNotificaciones(String sUsername, String sPassword) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		AprobacionesPage aprobaciones = new AprobacionesPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.clickBtnNotificaciones();

		// Aserciones
		Assert.assertTrue(aprobaciones.validarGrillaAprobaciones());
		global.log().info(
				"Se valid� que las notificaciones en la grilla coinciden con el n�mero indicado en la Campana de Notificaciones. ");

	}

	@Test(description = "168-Terminos y Condiciones - Visualizar TyC", dataProvider = "TerminosCondicionesRetailCases")
	public void tyCVisualizar(String sUsername, String sPassword, String shipTo) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		AprobacionesPage aprobaciones = new AprobacionesPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.clickSeccionAprobaciones();
		aprobaciones.clickNotificacionPorShipto(shipTo);
		

		// Aserciones
		Assert.assertTrue(aprobaciones.getPopupTyCretailTxt()
				.contains("Por medio de la presente quien suscribe (haciendo click en el bot�n"));
		global.log().info("Se valida popup de la notificaci�n seleccionada. ");
		Assert.assertTrue(aprobaciones.notificacionesPdf(),
				"Se valida que aparezca el .pfd de la notificaci�n seleccionada. ");
		global.log().info("Se valida que aparezca el .pfd de la notificaci�n seleccionada. ");

	}
	
	@Test(description = "169-Terminos y Condiciones -  Descargar TyC", dataProvider = "TerminosCondicionesRetailCases")
	public void tyCDescargar(String sUsername, String sPassword, String shipTo) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		AprobacionesPage aprobaciones = new AprobacionesPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.clickSeccionAprobaciones();
		aprobaciones.clickNotificacionPorShipto(shipTo);
		

		// Aserciones
		Assert.assertTrue(aprobaciones.getPopupTyCretailTxt()
				.contains("Por medio de la presente quien suscribe (haciendo click en el bot�n"));
		global.log().info("Se valida popup de la notificaci�n seleccionada. ");
		Assert.assertTrue(aprobaciones.notificacionesPdf(),
				"Se valida que aparezca el .pfd de la notificaci�n seleccionada. ");
		global.log().info("Se valida que aparezca el .pfd de la notificaci�n seleccionada. ");

		//Acciones
		aprobaciones.clickDownload();

	}

	
	@Test(description = "174-Terminos y Condiciones -   Visualizar TyC - Compatibilidad Navegador FireFox", dataProvider = "TerminosCondicionesRetailCases")
	public void tyCVisualizarFirefox(String sUsername, String sPassword, String shipTo) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		AprobacionesPage aprobaciones = new AprobacionesPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.clickSeccionAprobaciones();
		aprobaciones.clickNotificacionPorShipto(shipTo);
		

		// Aserciones
		Assert.assertTrue(aprobaciones.getPopupTyCretailTxt()
				.contains("Por medio de la presente quien suscribe (haciendo click en el bot�n"));
		global.log().info("Se valida popup de la notificaci�n seleccionada. ");
		Assert.assertTrue(aprobaciones.notificacionesPdf(),
				"Se valida que aparezca el .pfd de la notificaci�n seleccionada. ");
		global.log().info("Se valida que aparezca el .pfd de la notificaci�n seleccionada. ");

	}
	@Test(description = "191-T�rminos y Condiciones - Historial ", dataProvider = "TerminosCondicionesRetailCases")
	public void tyCHistorial(String sUsername, String sPassword) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		AprobacionesPage aprobaciones = new AprobacionesPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.clickSeccionAprobaciones();
		aprobaciones.clickHistorial();


		// Aserciones
		Assert.assertTrue(aprobaciones.validarTituloHistorial());
		global.log().info("Se valida estar en la pagina historial. ");

	}

	@Test(description = "192-T�rminos y Condiciones - Historial de TyC - Descargar", dataProvider = "TerminosCondicionesRetailCases")
	public void tyCHistorialDescargar(String sUsername, String sPassword, String tituloNotificacion) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		AprobacionesPage aprobaciones = new AprobacionesPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.clickSeccionAprobaciones();
		aprobaciones.clickHistorial();
		aprobaciones.descargarNotificacionEnHistorialPorTitulo(tituloNotificacion);

		// Aserciones
		Assert.assertTrue(aprobaciones.validarSiSeAbrioPestanaNueva());
		global.log().info("Se valida estar en el PDF de la notificacion. ");

	}
}
