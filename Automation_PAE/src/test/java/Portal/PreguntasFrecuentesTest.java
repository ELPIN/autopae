package Portal;


import com.everis.global.Global;
import com.everis.po.IndexPage;
import com.everis.po.LoginPage;
import com.everis.po.PreguntasFrecuentesPage;
import com.everis.utilities.ExcelUtils;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class PreguntasFrecuentesTest extends Base {

	Global global = new Global(driver);

	@DataProvider
	public Object[][] PreguntasFrecuentesCases(ITestContext context) throws Exception {
		Object[][] testObjArray = ExcelUtils.getTableArray(
				super.getDataProviderDir() + context.getCurrentXmlTest().getParameter("dataProvider"),
				context.getCurrentXmlTest().getParameter("sheet"));
		return (testObjArray);
	}
	
	@Test(description = "161-Verificar que se visualizan las preguntas en la secci�n de las preguntas frecuentes", dataProvider = "PreguntasFrecuentesCases")
	public void seccionPreguntasFrecuentesLista(String sUsername, String sPassword) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		PreguntasFrecuentesPage preguntasFrecuentes = new PreguntasFrecuentesPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.clickSeccionPreguntasFrecuentes();
		
		// Aserciones
		
		Assert.assertTrue(preguntasFrecuentes.grillaPreguntasFrecuentes().contains("1. �C�mo recuperar mi contrase�a?"));
		global.log().info("Se valida la Pregunta 1.");
		Assert.assertTrue(preguntasFrecuentes.grillaPreguntasFrecuentes().contains("2. �C�mo genero subusuarios? �Cu�ntos puedo generar?"));
		global.log().info("Se valida la Pregunta 2.");
		Assert.assertTrue(preguntasFrecuentes.grillaPreguntasFrecuentes().contains("3. �C�mo elijo el Ship-To para hacer los pedidos?"));
		global.log().info("Se valida la Pregunta 3.");
		Assert.assertTrue(preguntasFrecuentes.grillaPreguntasFrecuentes().contains("4. �Puedo hacer pedidos de lubricantes a granel?"));
		global.log().info("Se valida la Pregunta 4.");
		Assert.assertTrue(preguntasFrecuentes.grillaPreguntasFrecuentes().contains("5. �Qu� es la Fecha Preferente de Despacho?"));
		Assert.assertTrue(preguntasFrecuentes.grillaPreguntasFrecuentes().contains("6. �Cu�l es el horario de corte definido en planta de Lubricantes?"));
		Assert.assertTrue(preguntasFrecuentes.grillaPreguntasFrecuentes().contains("7. �Hasta cu�ntos d�as puedo programar mi pedido de Lubricantes?"));
		Assert.assertTrue(preguntasFrecuentes.grillaPreguntasFrecuentes().contains("8. �Qu� informaci�n se carga en Orden de Compra?"));
		Assert.assertTrue(preguntasFrecuentes.grillaPreguntasFrecuentes().contains("9. �Qu� cantidades me trae cuando asocio un Contrato?"));
		Assert.assertTrue(preguntasFrecuentes.grillaPreguntasFrecuentes().contains("10. �Qu� debo hacer si no encuentro la patente a la hora de generar un pedido?"));
		Assert.assertTrue(preguntasFrecuentes.grillaPreguntasFrecuentes().contains("11. �Puedo cancelar un pedido que ya fue programado?"));
		Assert.assertTrue(preguntasFrecuentes.grillaPreguntasFrecuentes().contains("12. �Cu�les son los estados del pedido?"));
		Assert.assertTrue(preguntasFrecuentes.grillaPreguntasFrecuentes().contains("13. �Qu� significa que un pedido est� en "));
		Assert.assertTrue(preguntasFrecuentes.grillaPreguntasFrecuentes().contains("14. �C�mo se lee el cuadro de "));
		Assert.assertTrue(preguntasFrecuentes.grillaPreguntasFrecuentes().contains("15. �A qui�n contacto en caso de dudas?"));
		global.log().info("Se valida listado de 15 preguntas en el apartado de Preguntas Frecuentes.");
		
	}
	
	@Test(description = "162-Preguntas Frecuentes - Visualizar Respuesta", dataProvider = "PreguntasFrecuentesCases")
	public void seccionPreguntasFrecuentesVisualizarRespuesta(String sUsername, String sPassword, String pregunta) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		PreguntasFrecuentesPage preguntasFrecuentes = new PreguntasFrecuentesPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.clickSeccionPreguntasFrecuentes();
		preguntasFrecuentes.clickPreguntaFrecuente(pregunta);
		
		// Aserciones
		Assert.assertTrue(preguntasFrecuentes.bodyRespuestaPreguntaFrecuente());
		global.log().info("Se valida que aparezca el body de la respuesta de: " + pregunta);

	}

}