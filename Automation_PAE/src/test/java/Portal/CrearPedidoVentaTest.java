package Portal;

import com.everis.utilities.ExcelUtils;
import com.everis.global.Global;
import com.everis.po.*;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class CrearPedidoVentaTest extends Base {

	Global global = new Global(driver);

	@DataProvider
	public Object[][] crearPedidoVentaCases(ITestContext context) throws Exception {
		Object[][] testObjArray = ExcelUtils.getTableArray(
				super.getDataProviderDir() + context.getCurrentXmlTest().getParameter("dataProvider"),
				context.getCurrentXmlTest().getParameter("sheet"));
		return (testObjArray);
	}

	@Test(description = "118-Bot�n Confirmar pedido� - tiene un contrato asociado", dataProvider = "crearPedidoVentaCases")
	public void generarPedidoConContratoClienteCIF(String sUsername, String sPassword, String canalVenta,
			String contrato, String patente, String ordenCompra, String cantidad, String codigo) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		CabeceraPedidoPage cabeceraPedido = new CabeceraPedidoPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		cabeceraPedido.completarCabeceraPedidoVtaCifConContrato(canalVenta, contrato, ordenCompra);
		global.log().info("Se despliega lista de Productos Disponibles.");
		productosDisponibles.completarProductosDisponibles(codigo, cantidad);

		// Aserciones
		Assert.assertTrue(productosDisponibles.cartelPedidoGenerado(), "Se valida cartel de generaci�n del pedido. ");
		global.log().info("Se gener� el pedido con el n�mero: " + productosDisponibles.numeroPedidoGenerado());

		// Cancelar Pedido API
		productosDisponibles.cancelarPedidoApi();

	}

	@Test(description = "119-Bot�n Confirmar pedido� - Se ejecuta correctamente", dataProvider = "crearPedidoVentaCases")
	public void btnConfirmarPedido(String sUsername, String sPassword, String canalVenta, String patente,
			String ordenCompra, String cantidad, String codigo) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		productosDisponibles.completarPedidoVenta(canalVenta, patente, ordenCompra, cantidad, codigo);

		// Aserciones
		Assert.assertTrue(productosDisponibles.cartelPedidoGenerado(), "Se valida cartel de generaci�n del pedido. ");
		global.log().info("Se gener� el pedido con el n�mero: " + productosDisponibles.numeroPedidoGenerado());

		// Cancelar Pedido API
		productosDisponibles.cancelarPedidoApi();

	}

	@Test(description = "120-Generar Pedido sin contrato - cliente CIF", dataProvider = "crearPedidoVentaCases")
	public void generarPedidoClienteCIF(String sUsername, String sPassword, String canalVenta, String patente,
			String ordenCompra, String cantidad, String codigo) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		productosDisponibles.completarPedidoVenta(canalVenta, patente, ordenCompra, cantidad, codigo);

		// Aserciones
		Assert.assertTrue(productosDisponibles.cartelPedidoGenerado(), "Se valida cartel de generaci�n del pedido. ");
		global.log().info("Se gener� el pedido con el n�mero: " + productosDisponibles.numeroPedidoGenerado());

		// Cancelar Pedido API
		productosDisponibles.cancelarPedidoApi();

	}

	@Test(description = "121-Generar Pedido BO sin contrato - cliente CIF", dataProvider = "crearPedidoVentaCases")
	public void generarPedidoBoClienteCIF(String sUsername, String sPassword, String canalVenta, String patente,
			String ordenCompra, String cantidad, String codigo) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		productosDisponibles.completarPedidoVentaBackOrder(canalVenta, patente, ordenCompra, cantidad, codigo);

		// Cancelar Pedido API
		productosDisponibles.cancelarPedidoBoApi();

	}

	@Test(description = "122-Generar Pedido combinado con Pedido BO sin contrato - cliente CIF", dataProvider = "crearPedidoVentaCases")
	public void generarPedidoCombinadoSinContratoCIF(String sUsername, String sPassword, String canalVenta,
			String patente, String ordenCompra, String codigoStock, String codigoSinStock, String cantidad)
			throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		productosDisponibles.completarPedidoMixto(canalVenta, ordenCompra, patente, cantidad, codigoStock,
				codigoSinStock);

		// Cancelar Pedido API
		productosDisponibles.cancelarPedidoMixtoApi();

	}

	@Test(description = "123-Generar Pedido  con contrato - cliente CIF", dataProvider = "crearPedidoVentaCases")
	public void generarPedidoConContratoCif(String sUsername, String sPassword, String canalVenta, String contrato,
			String ordenCompra, String codigo, String cantidad) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);
		CabeceraPedidoPage cabeceraPedido = new CabeceraPedidoPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		cabeceraPedido.completarCabeceraPedidoVtaCifConContrato(canalVenta, contrato, ordenCompra);
		productosDisponibles.completarProductosDisponibles(codigo, cantidad);

		// Aserciones
		Assert.assertTrue(productosDisponibles.cartelPedidoGenerado(), "Se valida cartel de generaci�n del pedido. ");

		// Cancelar Pedido API
		productosDisponibles.cancelarPedidoApi();

	}

	@Test(description = "124-Generar Pedido BO con contrato - cliente CIF", dataProvider = "crearPedidoVentaCases")
	public void generarPedidoBackOrderConContratoCif(String sUsername, String sPassword, String canalVenta,
			String contrato, String ordenCompra, String codigo, String cantidad) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);
		CabeceraPedidoPage cabeceraPedido = new CabeceraPedidoPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		cabeceraPedido.completarCabeceraPedidoVtaCifConContrato(canalVenta, contrato, ordenCompra);
		productosDisponibles.completarProductosDisponiblesBackOrderConContrato(codigo, cantidad);


	}

	@Test(description = "125-Generar Pedido combinado con Pedido BO con contrato - cliente CIF", dataProvider = "crearPedidoVentaCases")
	public void generarPedidoCombinadoCIF(String sUsername, String sPassword, String canalVenta, String contrato,
			String patente, String ordenCompra, String codigoStock, String codigoSinStock, String cantidad)
			throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		productosDisponibles.completarPedidoMixtoConContrato(canalVenta, ordenCompra, contrato, patente, cantidad,
				codigoStock, codigoSinStock);

		// Cancelar Pedido API
		productosDisponibles.cancelarPedidoMixtoApi();

	}

	@Test(description = "126-Generar Pedido con producto obsoleto con stock insuficiente - cliente CIF", dataProvider = "crearPedidoVentaCases")
	public void generarPedidoProductoObsoletoNoStockCIF(String sUsername, String sPassword, String canalVenta,
			String patente, String ordenCompra, String codigo, String cantidad) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se despliega lista de Productos Disponibles.");
		productosDisponibles.agregarProductoObsoleto(canalVenta, patente, ordenCompra, cantidad, codigo);

	}

	@Test(description = "127-Generar Pedido con producto obsoleto con stock suficiente - cliente CIF", dataProvider = "crearPedidoVentaCases")
	public void generarPedidoProductoObsoletoCif(String sUsername, String sPassword, String canalVenta, String patente,
			String ordenCompra, String codigo, String cantidad) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se despliega lista de Productos Disponibles.");
		productosDisponibles.agregarProductoObsoleto(canalVenta, patente, ordenCompra, cantidad, codigo);

		// Cancelar Pedido API
		productosDisponibles.cancelarPedidoApi();

	}

	@Test(description = "128-Generar Pedido - sin contrato - cliente FOB", dataProvider = "crearPedidoVentaCases")
	public void generarPedidoClienteBob(String sUsername, String sPassword, String canalVenta, String patente,
			String ordenCompra, String cantidad, String codigo) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		productosDisponibles.completarPedidoVenta(canalVenta, patente, ordenCompra, cantidad, codigo);

		// Aserciones
		Assert.assertTrue(productosDisponibles.cartelPedidoGenerado(), "Se valida cartel de generaci�n del pedido. ");
		global.log().info("Se gener� el pedido con el n�mero: " + productosDisponibles.numeroPedidoGenerado());

		// Cancelar Pedido API
		productosDisponibles.cancelarPedidoApi();

	}

	@Test(description = "129-Generar Pedido BO - sin contrato - cliente FOB", dataProvider = "crearPedidoVentaCases")
	public void generarPedidoBoClienteFob(String sUsername, String sPassword, String canalVenta, String patente,
			String ordenCompra, String cantidad, String codigo) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		productosDisponibles.completarPedidoVentaBackOrder(canalVenta, patente, ordenCompra, cantidad, codigo);

		// Cancelar Pedido API
		productosDisponibles.cancelarPedidoBoApi();

	}

	@Test(description = "130-Generar Pedido combinado con Pedido BO sin contrato - cliente FOB", dataProvider = "crearPedidoVentaCases")
	public void generarPedidoCombinadoFob(String sUsername, String sPassword, String canalVenta, String patente,
			String ordenCompra, String codigoStock, String codigoSinStock, String cantidad) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		productosDisponibles.completarPedidoMixto(canalVenta, patente, ordenCompra, cantidad, codigoStock,
				codigoSinStock);

		// Cancelar Pedido API
		productosDisponibles.cancelarPedidoMixtoApi();

	}

	@Test(description = "131-Generar Pedido  con contratos - cliente FOB", dataProvider = "crearPedidoVentaCases")
	public void generarPedidoContratoClienteFob(String sUsername, String sPassword, String canalVenta, String contrato,
			String patente, String ordenCompra, String cantidad, String codigo, String shipTo) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		CabeceraPedidoPage cabeceraPedido = new CabeceraPedidoPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.cambiarShipTo(shipTo);
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		cabeceraPedido.completarCabeceraPedidoVtaFobConContrato(canalVenta, contrato, patente, ordenCompra);
		productosDisponibles.completarProductosDisponibles(codigo, cantidad);

		// Cancelar Pedido API
		productosDisponibles.cancelarPedidoApi();

	}

	@Test(description = "132-Generar Pedido BO con contrato - cliente FOB", dataProvider = "crearPedidoVentaCases")
	public void generarPedidoBOContratoClienteFob(String sUsername, String sPassword, String canalVenta,
			String contrato, String patente, String ordenCompra, String cantidad, String codigo, String shipTo) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		CabeceraPedidoPage cabeceraPedido = new CabeceraPedidoPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.cambiarShipTo(shipTo);
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		cabeceraPedido.completarCabeceraPedidoVtaFobConContrato(canalVenta, contrato, patente, ordenCompra);
		productosDisponibles.completarProductosDisponiblesBackOrderConContrato(codigo, cantidad);

	}

	@Test(description = "133-Generar Pedido combinado con Pedido BO con contrato - cliente FOB", dataProvider = "crearPedidoVentaCases")
	public void generarPedidoCombinadoFobContrato(String sUsername, String sPassword, String canalVenta,
			String contrato, String patente, String ordenCompra, String codigoStock, String codigoSinStock,
			String cantidad) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		productosDisponibles.completarPedidoMixtoConContrato(canalVenta, ordenCompra, contrato, patente, cantidad,
				codigoStock, codigoSinStock);

		// Cancelar Pedido API
		productosDisponibles.cancelarPedidoMixtoApi();
	}

	@Test(description = "134-Generar Pedido con producto obsoleto con stock insuficiente - cliente FOB", dataProvider = "crearPedidoVentaCases")
	public void generarPedidoProductoObsoletoNoStockFob(String sUsername, String sPassword, String canalVenta,
			String patente, String ordenCompra, String cantidad, String codigo) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		productosDisponibles.agregarProductoObsoleto(canalVenta, patente, ordenCompra, cantidad, codigo);

		// Aserciones
		String errorCartelCentroAtencion = productosDisponibles.cartelLlamadoCentroAtencion();
		Assert.assertTrue(errorCartelCentroAtencion.contains(
				"Solo la cantidad indicada est� disponible para la venta. Consulte por un producto de referencia llamando a nuestro Centro de Atenci�n 0800-555-3776"));
		global.log().info(
				"Se visualiza cartel de error de:  'Solo la cantidad indicada est� disponible para la venta. Consulte por un producto de referencia llamando a nuestro Centro de Atenci�n...'");

	}

	@Test(description = "176-Generar Pedido BO con Orden de Compra", dataProvider = "crearPedidoVentaCases")
	public void generarPedidoBoConOrdenCompra(String sUsername, String sPassword, String canalVenta, String patente,
			String ordenCompra, String cantidad, String codigo) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		productosDisponibles.completarPedidoVentaBackOrder(canalVenta, patente, ordenCompra, cantidad, codigo);

		// Cancelar Pedido API
		productosDisponibles.cancelarPedidoBoApi();

	}

	@Test(description = "177-Generar Pedido BO sin Orden de Compra", dataProvider = "crearPedidoVentaCases")
	public void generarPedidoBoSinOrdenCompra(String sUsername, String sPassword, String canalVenta, String patente,
			String ordenCompra, String cantidad, String codigo) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);
		CabeceraPedidoPage cabeceraPedido = new CabeceraPedidoPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		cabeceraPedido.seleccionarCanalVenta(canalVenta);
		cabeceraPedido.completarFechaPedido(cabeceraPedido.getObtenerFechaValidaPedido());
		cabeceraPedido.clickbtnContinuar();
		productosDisponibles.completarProductosDisponiblesBackOrder(codigo, cantidad);

		// Cancelar Pedido API
		productosDisponibles.cancelarPedidoBoApi();

	}

	@Test(description = "TL 644 Bot�n nuevo pedido - Pedidos - Fuels - Pesados", dataProvider = "crearPedidoVentaCases")
	public void verificarBtnNuevoPedidoPesados(String sUsername, String sPassword) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		PedidosPage pedidosPage = new PedidosPage(driver);
		CabeceraPedidoPage cabeceraPedidoPage = new CabeceraPedidoPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.clickSeccionPedidos();
		String validacionTitulo = pedidosPage.validarTituloPesados();
		pedidosPage.nuevoPedidoPesados();
		boolean validacionCabeceraDePedido = cabeceraPedidoPage.validarTituloCabeceraPedido();

		//Assert
		Assert.assertEquals(validacionTitulo, "Combustibles Pesados");
		Assert.assertTrue(validacionCabeceraDePedido, "Se valida la redirecci�n a la cabecera del pedido");
	}

	@Test(description = "TL 642 - Bot�n nuevo pedido - Pedidos - Fuels - Liquidos", dataProvider = "crearPedidoVentaCases")
	public void verificarBtnNuevoPedidoLiquidos(String sUsername, String sPassword) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		PedidosPage pedidosPage = new PedidosPage(driver);
		CabeceraPedidoPage cabeceraPedidoPage = new CabeceraPedidoPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.clickSeccionPedidos();
		String validacionTitulo = pedidosPage.validarTituloLiquidos();
		pedidosPage.nuevoPedidoLiquidos();
		boolean validacionCabeceraDePedido = cabeceraPedidoPage.validarTituloCabeceraPedido();

		//Assert
		Assert.assertEquals(validacionTitulo, "Combustibles L�quidos");
		Assert.assertTrue(validacionCabeceraDePedido, "Se valida la redirecci�n a la cabecera del pedido");
	}

}
