package Portal;


import com.everis.global.Global;
import com.everis.po.IndexPage;
import com.everis.po.LoginPage;
import com.everis.po.PedidosPage;
import com.everis.utilities.ExcelUtils;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class ModificarPedidosDeVentaTest extends Base {

    Global global = new Global(driver);

    @DataProvider
    public Object[][] modificarPedidosDeVentaCases(ITestContext context) throws Exception {
        Object[][] testObjArray = ExcelUtils.getTableArray(
                super.getDataProviderDir() + context.getCurrentXmlTest().getParameter("dataProvider"),
                context.getCurrentXmlTest().getParameter("sheet"));
        return (testObjArray);
    }

    @Test(description = "257-Modificar Pedido reserva - Eliminar 1 producto.", dataProvider = "modificarPedidosDeVentaCases")
    public void eliminar1Producto(String sUsername, String sPassword, String jsonBody, String productoAEliminar, String producto2) throws Exception {

        global.log().info("Inicia el caso de prueba");
        LoginPage login = new LoginPage(driver);
        IndexPage index = new IndexPage(driver);
        PedidosPage pedidos = new PedidosPage(driver);

        //Pedido a eliminar
        String orderNumber = pedidos.agregarPedidoReservaAPI(jsonBody);

        // Acciones
        login.executeLogin(sUsername, sPassword);
        global.log().info("Se valid� correctamente el inicio de sesi�n.");
        index.clickSeccionPedidos();
        pedidos.apartadoPedidoReserva();
        pedidos.seleccionarPedidoDeReserva(orderNumber);
        pedidos.eliminar1LineaDePedidoReserva(orderNumber, productoAEliminar);

        // Aserciones
        Assert.assertTrue(pedidos.existenciaPedidoDeReserva(orderNumber), "Se valida que el pedido de Reserva con orderNumber: " + orderNumber + " continua existiendo.");
        Assert.assertFalse(pedidos.productoContinuaEnPedido(productoAEliminar, orderNumber), "Se valida que el producto: " + productoAEliminar + " fue eliminado del pedido: " + orderNumber);

        //Terminar de eliminar el pedido para que la atuomatizacion sea repetible
        pedidos.eliminar1LineaDePedidoReserva(orderNumber, producto2);
        Assert.assertFalse(pedidos.existenciaPedidoDeReserva(orderNumber), "Se valida que el pedido de Reserva con orderNumber: " + orderNumber + " ya no existe.");


    }

    @Test(description = "258-Modificar Pedido reserva - Eliminar mas de 1 producto.", dataProvider = "modificarPedidosDeVentaCases")
    public void eliminarMasDe1Producto(String sUsername, String sPassword, String jsonBody, String productoAEliminar, String productoAEliminar2) throws Exception {

        global.log().info("Inicia el caso de prueba");
        LoginPage login = new LoginPage(driver);
        IndexPage index = new IndexPage(driver);
        PedidosPage pedidos = new PedidosPage(driver);

        //Pedido a eliminar
        String orderNumber = pedidos.agregarPedidoReservaAPI(jsonBody);

        // Acciones
        login.executeLogin(sUsername, sPassword);
        global.log().info("Se valid� correctamente el inicio de sesi�n.");
        index.clickSeccionPedidos();
        pedidos.apartadoPedidoReserva();
        pedidos.seleccionarPedidoDeReserva(orderNumber);
        pedidos.eliminar1LineaDePedidoReserva(orderNumber, productoAEliminar);
        pedidos.seleccionarPedidoDeReserva(orderNumber);
        pedidos.eliminar1LineaDePedidoReserva(orderNumber, productoAEliminar2);

        // Aserciones
        Assert.assertFalse(pedidos.existenciaPedidoDeReserva(orderNumber), "Se valida que el pedido de Reserva con orderNumber: " + orderNumber + "ya no existe.");
    }

    @Test(description = "259-Modificar Pedido reserva - Eliminar  1 producto en pedido con 1 solo producto.", dataProvider = "modificarPedidosDeVentaCases")
    public void eliminar1ProductoDe1(String sUsername, String sPassword, String jsonBody, String productoAEliminar) throws Exception {

        global.log().info("Inicia el caso de prueba");
        LoginPage login = new LoginPage(driver);
        IndexPage index = new IndexPage(driver);
        PedidosPage pedidos = new PedidosPage(driver);

        //Pedido a eliminar
        String orderNumber = pedidos.agregarPedidoReservaAPI(jsonBody);

        // Acciones
        login.executeLogin(sUsername, sPassword);
        global.log().info("Se valid� correctamente el inicio de sesi�n.");
        index.clickSeccionPedidos();
        pedidos.apartadoPedidoReserva();
        pedidos.seleccionarPedidoDeReserva(orderNumber);
        pedidos.eliminar1LineaDePedidoReserva(orderNumber, productoAEliminar);

        // Aserciones
        Assert.assertFalse(pedidos.existenciaPedidoDeReserva(orderNumber), "Se valida que el pedido de Reserva con orderNumber: " + orderNumber + "ya no existe.");
    }

    @Test(description = "260-Modificar Pedido reserva - Boton Eliminar Producto.", dataProvider = "modificarPedidosDeVentaCases")
    public void eliminarSinSeleccionarProducto(String sUsername, String sPassword, String nroPedido) throws Exception {

        global.log().info("Inicia el caso de prueba");
        LoginPage login = new LoginPage(driver);
        IndexPage index = new IndexPage(driver);
        PedidosPage pedidos = new PedidosPage(driver);

        // Acciones
        login.executeLogin(sUsername, sPassword);
        global.log().info("Se valid� correctamente el inicio de sesi�n.");
        index.clickSeccionPedidos();
        pedidos.apartadoPedidoReserva();

        // Aserciones
        Assert.assertTrue(pedidos.validarEliminarPedidoSinSeleccionarProducto(nroPedido), "Se valida que da una advertencia al tratar de eliminar una linea de pedido reserva sin seleccionar producto.");
    }

}
