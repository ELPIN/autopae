package Portal;


import com.everis.global.Global;
import com.everis.po.CabeceraPedidoPage;
import com.everis.po.IndexPage;
import com.everis.po.LoginPage;
import com.everis.po.ProductosDisponiblesPage;
import com.everis.utilities.ExcelUtils;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class ProductosPreciosDescuentosTest extends Base {

	Global global = new Global(driver);

	@DataProvider
	public Object[][] productosPreciosDescuentosCases(ITestContext context) throws Exception {
		Object[][] testObjArray = ExcelUtils.getTableArray(
				super.getDataProviderDir() + context.getCurrentXmlTest().getParameter("dataProvider"),
				context.getCurrentXmlTest().getParameter("sheet"));
		return (testObjArray);
	}

	@Test(description = "43-Campo contrato cabecera�- Vac�o", dataProvider = "productosPreciosDescuentosCases")
	public void campoContratoCabeceraVacio(String sUsername, String sPassword, String canalVenta) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		CabeceraPedidoPage cabeceraPedido = new CabeceraPedidoPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		cabeceraPedido.completarFechaPedido(cabeceraPedido.getObtenerFechaValidaPedido());
		cabeceraPedido.seleccionarCanalVenta(canalVenta);
		cabeceraPedido.clickbtnContinuar();

		// Aserciones
		String cartelProductosDisponibles = productosDisponibles.cartelProductosDisponibles();
		Assert.assertTrue(cartelProductosDisponibles.contains("Productos disponibles"));
		global.log().info("Se despliega lista de Productos Disponibles.");

	}

	@Test(description = "48-B�squeda de productos - Por C�digo", dataProvider = "productosPreciosDescuentosCases")
	public void busquedaProductoCodigo(String sUsername, String sPassword, String canalVenta, String patente,
			String ordenCompra, String codigo) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		CabeceraPedidoPage cabeceraPedido = new CabeceraPedidoPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		cabeceraPedido.completarCabeceraPedidoVta(canalVenta, patente, ordenCompra);
		global.log().info("Se despliega lista de Productos Disponibles.");
		productosDisponibles.buscadorProductos(codigo);
		productosDisponibles.validarProductoPorIdentificador(codigo);

	}

	@Test(description = "49-B�squeda de productos - Por descripci�n", dataProvider = "productosPreciosDescuentosCases")
	public void busquedaProductoDescripcion(String sUsername, String sPassword, String canalVenta, String patente,
			String ordenCompra, String descripcion) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		CabeceraPedidoPage cabeceraPedido = new CabeceraPedidoPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		cabeceraPedido.completarCabeceraPedidoVta(canalVenta, patente, ordenCompra);
		global.log().info("Se despliega lista de Productos Disponibles.");
		productosDisponibles.buscadorProductos(descripcion);
		productosDisponibles.validarProductoPorIdentificador(descripcion);

	}

	@Test(description = "51-B�squeda por Filtros - Selecci�n de filtros", dataProvider = "productosPreciosDescuentosCases")
	public void busquedaPorFiltro(String sUsername, String sPassword, String canalVenta, String patente,
			String ordenCompra, String categoria, String codigo) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		CabeceraPedidoPage cabeceraPedido = new CabeceraPedidoPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		cabeceraPedido.completarCabeceraPedidoVta(canalVenta, patente, ordenCompra);
		global.log().info("Se despliega lista de Productos Disponibles.");
		productosDisponibles.filtrarProductos(categoria);
		productosDisponibles.validarProductoPorIdentificador(codigo);

	}

	@Test(description = "52-B�squeda por Filtros - Selecci�n de filtros con contrato seleccionado", dataProvider = "productosPreciosDescuentosCases")
	public void busquedaPorFiltroConContrato(String sUsername, String sPassword, String canalVenta, String contrato,
			String ordenCompra, String categoria, String codigo) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		CabeceraPedidoPage cabeceraPedido = new CabeceraPedidoPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		cabeceraPedido.completarCabeceraPedidoVtaCifConContrato(canalVenta, contrato, ordenCompra);
		global.log().info("Se despliega lista de Productos Disponibles.");
		productosDisponibles.filtrarProductos(categoria);
		productosDisponibles.validarProductoPorIdentificador(codigo);

	}

	@Test(description = "53-B�squeda por Filtros - Limpiar filtros", dataProvider = "productosPreciosDescuentosCases")
	public void limpiarFiltro(String sUsername, String sPassword, String canalVenta, String patente, String ordenCompra,
			String categoria) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		CabeceraPedidoPage cabeceraPedido = new CabeceraPedidoPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		cabeceraPedido.completarCabeceraPedidoVta(canalVenta, patente, ordenCompra);
		global.log().info("Se despliega lista de Productos Disponibles.");
		productosDisponibles.filtrarProductos(categoria);
		global.log().info("Se aplic� el filtro correspondiente");
		productosDisponibles.limpiarFiltros();

		// Aserciones
		Assert.assertFalse(productosDisponibles.validarFiltro(categoria));
		global.log().info("Se valida que el filtro se elimin� correctamente.");

	}

	@Test(description = "54-B�squeda por Filtros - Eliminar un filtro", dataProvider = "productosPreciosDescuentosCases")
	public void eliminarFiltro(String sUsername, String sPassword, String canalVenta, String patente,
			String ordenCompra, String categoria) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		CabeceraPedidoPage cabeceraPedido = new CabeceraPedidoPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		cabeceraPedido.completarCabeceraPedidoVta(canalVenta, patente, ordenCompra);
		global.log().info("Se despliega lista de Productos Disponibles.");
		productosDisponibles.filtrarProductos(categoria);
		global.log().info("Se aplic� el filtro correspondiente");
		productosDisponibles.seleccionarfiltroAplicado(categoria);
		global.log().info("Se presiona el filtro agregado");

		// Aserciones
		Assert.assertFalse(productosDisponibles.validarFiltro(categoria));
		global.log().info("Se valida que el filtro se elimin� correctamente.");
	}

	@Test(description = "56-Archivo de carga - Carga masiva productos", dataProvider = "productosPreciosDescuentosCases")
	public void cargaMasivaProductos(String sUsername, String sPassword, String canalVenta, String patente,
			String ordenCompra, String archivo) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		CabeceraPedidoPage cabeceraPedido = new CabeceraPedidoPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		cabeceraPedido.completarCabeceraPedidoVta(canalVenta, patente, ordenCompra);
		global.log().info("Se despliega lista de Productos Disponibles.");
		productosDisponibles.subirArchivo(archivo);

	}

	@Test(description = "57-Archivo de carga - Carga masiva productos c�digo incorrecto", dataProvider = "productosPreciosDescuentosCases")
	public void cargaMasivaProductosCodigoIncorrecto(String sUsername, String sPassword, String canalVenta,
			String patente, String ordenCompra, String archivo) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		CabeceraPedidoPage cabeceraPedido = new CabeceraPedidoPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		cabeceraPedido.completarCabeceraPedidoVta(canalVenta, patente, ordenCompra);
		global.log().info("Se despliega lista de Productos Disponibles.");
		productosDisponibles.subirArchivo(archivo);

		// Aserciones
		String errorCartelCargaProductos = productosDisponibles.cartelLlamadoCentroAtencion();
		Assert.assertTrue(errorCartelCargaProductos.contains("No se pudieron cargar los siguientes productos:"));
		global.log().info("Se visualiza cartel de Atenci�n indicando que no se cargaron productos.");

	}

}
