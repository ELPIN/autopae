package Portal;


import com.everis.global.Global;
import com.everis.po.IndexPage;
import com.everis.po.LoginPage;
import com.everis.po.MiCuentaPage;
import com.everis.utilities.ExcelUtils;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


public class MiCuentaTest extends Base {

	Global global = new Global(driver);

	@DataProvider
	public Object[][] miCuentaCases(ITestContext context) throws Exception {
		Object[][] testObjArray = ExcelUtils.getTableArray(
				super.getDataProviderDir() + context.getCurrentXmlTest().getParameter("dataProvider"),
				context.getCurrentXmlTest().getParameter("sheet"));
		return (testObjArray);
	}

	@Test(description = "138-Acceder a opci�n Mi Cuenta - Usuario Maestro", dataProvider = "miCuentaCases")
	public void miCuentaUserMaster(String sUsername, String sPassword) throws InterruptedException {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.seccionMiCuenta();

	}

	@Test(description = "139-Acceder a opci�n Mi Cuenta - Usuario No Maestro", dataProvider = "miCuentaCases")
	public void miCuentaUserNoMaster(String sUsername, String sPassword) {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.clickProfileBtn();

		// Aserciones
		Assert.assertFalse(index.btnPerfil(), "Se valida que no se encuentre el bot�n Mi Cuenta. ");
		global.log().info("Se valida que no se encuentre el bot�n Mi Cuenta. ");

	}

	@Test(description = "141-Asociar Patente Duplicada", dataProvider = "miCuentaCases")
	public void asociarPatenteDuplicada(String sUsername, String sPassword, String patente) throws InterruptedException {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		MiCuentaPage miCuenta = new MiCuentaPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.seccionMiCuenta();
		miCuenta.irSeccionPatentes();
		miCuenta.asociarPatente(patente);

		// Aserciones
		Assert.assertTrue(miCuenta.errorAsociarPatente().contains("Ingrese patente no asociada"));
		global.log().info("Se valida cartel de error en la asociaci�n de patente. ");

	}

	@Test(description = "142-Asociar Patente Inv�lida", dataProvider = "miCuentaCases")
	public void asociarPatenteInvalida(String sUsername, String sPassword, String patente) throws InterruptedException {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		MiCuentaPage miCuenta = new MiCuentaPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.seccionMiCuenta();
		miCuenta.irSeccionPatentes();
		miCuenta.asociarPatente(patente);

		// Aserciones
		String errorCartelAsociarPatente = miCuenta.errorAsociarPatente();
		Assert.assertTrue(errorCartelAsociarPatente.contains("Ingrese una patente v�lida"));
		global.log().info("Se valida cartel de error en la asociaci�n de patente. ");

	}
	
	@Test(description = "143-Visualizar Panel para Asociar Patente - Cliente FOB", dataProvider = "miCuentaCases")
	public void visualizarBtnAsociarPatenteClienteFob(String sUsername, String sPassword) throws InterruptedException {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		MiCuentaPage miCuenta = new MiCuentaPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.seccionMiCuenta();
		miCuenta.irSeccionPatentes();
		
		// Aserciones
		Assert.assertTrue(miCuenta.buttonAsociarPatente(), "Valido que se encuentre el bot�n de asociar patente.");
		global.log().info("Se valida que se encuentre la opci�n de Asociar Patente. ");

	}
	
	@Test(description = "144-No Visualizar Panel para Asociar Patente - Cliente CIF", dataProvider = "miCuentaCases")
	public void visualizarBtnAsociarPatenteClienteCif(String sUsername, String sPassword) throws InterruptedException {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		MiCuentaPage miCuenta = new MiCuentaPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.seccionMiCuenta();
		
		// Aserciones
		Assert.assertFalse(miCuenta.buttonAsociarPatente(),"Se valida que no aparezca la opci�n de Asociar Patente.");
		global.log().info("Se valida que no aparezca la opci�n de Asociar Patente.");

	}
	
	@Test(description = "145-ABM Usuarios - Alta de Usuario Nuevo - Campo correo electr�nico vac�o", dataProvider = "miCuentaCases")
	public void altaUsuarioABMCampoUserVacio(String sUsername, String sPassword) throws InterruptedException {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		MiCuentaPage miCuenta = new MiCuentaPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.seccionMiCuenta();
		miCuenta.irSeccionGestionarSubUsuarios();
		miCuenta.clickBtnAgregarUsuarios();
		miCuenta.clickBtnDarAlta();
		
		// Aserciones
		Assert.assertEquals(miCuenta.errorAgregarUsuarios(),"Ingrese un correo v�lido");
		global.log().info("Se valida cartel de error de campo usuario vac�o.");

	}
	
	@Test(description = "146-ABM Usuarios - Alta de Usuario Nuevo", dataProvider = "miCuentaCases")
	public void altaUsuarioABM(String sUsername, String sPassword, String subUsuario) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		MiCuentaPage miCuenta = new MiCuentaPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.seccionMiCuenta();
		miCuenta.irSeccionGestionarSubUsuarios();
		miCuenta.validarSubUsuario(subUsuario);
		miCuenta.altaSubUsuario(subUsuario);
		
		//Se elimina el subUsuario para reutilizarlo
		miCuenta.eliminarSubUsuarioApi(sUsername,subUsuario);

	}
	
	@Test(description = "147-ABM Usuarios - Alta de Usuario Existente", dataProvider = "miCuentaCases")
	public void altaUsuarioABMUsuarioExistente(String sUsername, String sPassword, String subUsuario) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		MiCuentaPage miCuenta = new MiCuentaPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.seccionMiCuenta();
		miCuenta.irSeccionGestionarSubUsuarios();
		miCuenta.clickBtnAgregarUsuarios();
		miCuenta.sendMailAgregarUsuarios(subUsuario);
		miCuenta.clickBtnDarAlta();
		
		// Aserciones
		Assert.assertEquals(miCuenta.poupBajaSubUsuario(),"El usuario no pudo ser dado de alta (El usuario ya existe)");
		global.log().info("Se valida cartel de alerta indicando que el usuario: " + subUsuario + " ya existe.");

	}
	
	@Test(description = "148-ABM Usuarios - Modificaci�n de Usuario", dataProvider = "miCuentaCases")
	public void altaUsuarioModificarUsuario(String sUsername, String sPassword, String subUsuario, String shiptoUno, String shiptoDos) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		MiCuentaPage miCuenta = new MiCuentaPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.seccionMiCuenta();
		miCuenta.irSeccionGestionarSubUsuarios();
		miCuenta.modificarShipToPorDefecto(subUsuario,shiptoUno,shiptoDos);
		
	}
	
	@Test(description = "149-ABM Usuarios - Ingreso al Portal con ShipTo por Defecto", dataProvider = "miCuentaCases")
	public void ingresoPortalShiptoDefecto(String sUsername, String sPassword, String subUsuario, String subUPassword, String shiptoUno, String shiptoDos) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		MiCuentaPage miCuenta = new MiCuentaPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.seccionMiCuenta();
		miCuenta.irSeccionGestionarSubUsuarios();
		miCuenta.modificarShipToPorDefecto(subUsuario, shiptoUno, shiptoDos);
		index.cerrarSesion();
		login.executeLogin(subUsuario, subUPassword);
		index.seleccionarShipTo();
		
		// Aserciones
		Assert.assertFalse(index.validarShiptoDefecto(), "Se valida que no se aparezca el cartel de confirmaci�n de shipto.");
		global.log().info("El Shipto seleccionado esta configurado por defecto. ");
	}
	
	@Test(description = "150-ABM Usuarios - Ingreso al Portal con ShipTo por Defecto y ShipTo habilitados", dataProvider = "miCuentaCases")
	public void ingresoPortalShiptoHabilitados(String sUsername, String sPassword, String subUsuario, String subUPassword, String shiptoUno, String shiptoDos, String shiptoTres) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		MiCuentaPage miCuenta = new MiCuentaPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.seccionMiCuenta();
		miCuenta.irSeccionGestionarSubUsuarios();
		miCuenta.habilitarShipto(subUsuario, shiptoUno, shiptoDos, shiptoTres);
		index.cerrarSesion();
		login.executeLogin(subUsuario, subUPassword);
		index.seleccionarShipTo();
		
	}
	
	@Test(description = "151-ABM Usuarios - Baja de Usuario", dataProvider = "miCuentaCases")
	public void altaUsuarioBaja(String sUsername, String sPassword, String subUsuario) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		MiCuentaPage miCuenta = new MiCuentaPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.seccionMiCuenta();
		miCuenta.irSeccionGestionarSubUsuarios();
		miCuenta.validarSubUsuario(subUsuario);
		miCuenta.altaSubUsuario(subUsuario);
		index.seccionMiCuenta();
		miCuenta.irSeccionGestionarSubUsuarios();
		miCuenta.eliminarSubUsuario(subUsuario);
		
	}

}
