package Portal;

import com.everis.utilities.ExcelUtils;
import com.everis.global.Global;
import com.everis.po.*;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class HistorialEstadoPedidosTest extends Base {

	Global global = new Global(driver);

	@DataProvider
	public Object[][] historialEstadoPedidosCases(ITestContext context) throws Exception {
		Object[][] testObjArray = ExcelUtils.getTableArray(
				super.getDataProviderDir() + context.getCurrentXmlTest().getParameter("dataProvider"),
				context.getCurrentXmlTest().getParameter("sheet"));
		return (testObjArray);
	}

	@Test(description = "95-Editar�un pedido�- datos cabecera", dataProvider = "historialEstadoPedidosCases")
	public void editarPedidoDatosCabecera(String sUsername, String sPassword, String canalVenta, String ordenCompra,
			String patente, String codigo, String cantidad, String noPedido) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		PedidosPage pedidos = new PedidosPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);
		HistorialPedidosPage historial = new HistorialPedidosPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		productosDisponibles.completarPedidoVenta(canalVenta, patente, ordenCompra, cantidad, codigo);
		historial.buscarNoPedido(noPedido);
		pedidos.editarPedidoCanalVenta();

		// Aserciones
		Assert.assertTrue(pedidos.campoContratoStatus(), "Se valida el campo contrato. ");
		global.log().info("Se valida que este deshabilitado el campo contrato. ");
		
		//Cancelar Pedido API
		productosDisponibles.cancelarPedidoApi();
	}

	@Test(description = "98-Copiar�un pedido�- icono copiar pedido", dataProvider = "historialEstadoPedidosCases")
	public void copiarPedido(String sUsername, String sPassword, String canalVenta, String ordenCompra,
			String patente, String codigo, String cantidad, String noPedido) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		PedidosPage pedidos = new PedidosPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		productosDisponibles.completarPedidoVenta(canalVenta, patente, ordenCompra, cantidad, codigo);
		pedidos.repetirPedido(noPedido);
		
		//Cancelar Pedido API
		productosDisponibles.cancelarPedidoApi();

	}

	@Test(description = "102-Cancelar�un pedido�- confirmar cancelacion de pedido", dataProvider = "historialEstadoPedidosCases")
	public void confirmarCancelarPedido(String sUsername, String sPassword, String canalVenta, String ordenCompra,
			String patente, String codigo, String cantidad, String noPedido) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		PedidosPage pedidos = new PedidosPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);
		HistorialPedidosPage historial = new HistorialPedidosPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		productosDisponibles.completarPedidoVenta(canalVenta, patente, ordenCompra, cantidad, codigo);
		historial.buscarNoPedido(noPedido);
		pedidos.cancelarPedidoBtn();

		// Aserciones
		Assert.assertTrue(pedidos.cartelCancelacionConfirmada(), "Se valida cartel de cancelaci�n exitosa. ");
		global.log().info("Se cancel� el pedio satisfactoriamente. ");

	}

	@Test(description = "103-Cancelar�un pedido�- cancelar pedido despues de hora de corte", dataProvider = "historialEstadoPedidosCases")
	public void cancelarPedidoDespuesCorte(String sUsername, String sPassword, String noPedido) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		PedidosPage pedidos = new PedidosPage(driver);
		HistorialPedidosPage historial = new HistorialPedidosPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.clickSeccionPedidos();
		historial.buscarNoPedido(noPedido);
		pedidos.cancelarDataProvider(noPedido);

		// Aserciones
		Assert.assertEquals(pedidos.cartelPedidoNoModificable(),
				"El pedido no puede ser cancelado, ya se encuentra en planificaci�n");
		global.log().info("Se muestra cartel de Atenci�n indicando que el pedido no puede ser cancelado. ");

	}

	@Test(description = "104-Detalle del pedido�- detalle sobre numero de pedido", dataProvider = "historialEstadoPedidosCases")
	public void detalleSobreNumeroPedido(String sUsername, String sPassword, String canalVenta, String ordenCompra,
			String patente, String codigo, String cantidad, String noPedido) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		PedidosPage pedidos = new PedidosPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);
		HistorialPedidosPage historial = new HistorialPedidosPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		productosDisponibles.completarPedidoVenta(canalVenta, patente, ordenCompra, cantidad, codigo);
		historial.buscarNoPedido(noPedido);

		// Aserciones
		Assert.assertTrue(pedidos.validarResumenProductosPedidos(codigo, cantidad),
				"Se valida que el resumen de los productos sea correcto");
		global.log().info("Se valida resumen del pedido satisfactoriamente. ");
		
		//Cancelar Pedido API
		productosDisponibles.cancelarPedidoApi();

	}

	@Test(description = "106-Historial de pedido�- b�squeda por n�mero pedido", dataProvider = "historialEstadoPedidosCases")
	public void verificarBusquedaNumeroPedido(String sUsername, String sPassword, String canalVenta, String ordenCompra,
			String patente, String codigo, String cantidad, String noPedido) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);
		HistorialPedidosPage historial = new HistorialPedidosPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		productosDisponibles.completarPedidoVenta(canalVenta, patente, ordenCompra, cantidad, codigo);
		historial.buscarNoPedido(noPedido);
		
		//Cancelar Pedido API
		productosDisponibles.cancelarPedidoApi();

	}

	@Test(description = "107-Historial de pedido�- b�squeda por estado", dataProvider = "historialEstadoPedidosCases")
	public void historialPedidoBusquedaEstado(String sUsername, String sPassword, String canalVenta, String ordenCompra,
			String patente, String codigo, String cantidad, String estado) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		PedidosPage pedidos = new PedidosPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);
		HistorialPedidosPage historial = new HistorialPedidosPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		productosDisponibles.completarPedidoVenta(canalVenta, patente, ordenCompra, cantidad, codigo);
		historial.busquedaEstado(estado);
		pedidos.validarHeaderPedido();
		
		//Cancelar Pedido API
		productosDisponibles.cancelarPedidoApi();

	}

	@Test(description = "108-Verificar que el sistema realiza b�squeda por�producto�", dataProvider = "historialEstadoPedidosCases")
	public void historialPedidoBusquedaProducto(String sUsername, String sPassword, String canalVenta,
			String ordenCompra, String patente, String codigo, String cantidad) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		PedidosPage pedidos = new PedidosPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);
		HistorialPedidosPage historial = new HistorialPedidosPage(driver);
		
		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		productosDisponibles.completarPedidoVenta(canalVenta, patente, ordenCompra, cantidad, codigo);
		historial.busquedaProducto(codigo);
		pedidos.validarHeaderPedido();
		
		//Cancelar Pedido API
		productosDisponibles.cancelarPedidoApi();

	}

	@Test(description = "114-Pantalla de�b�squeda�- descargar reporte", dataProvider = "historialEstadoPedidosCases")
	public void descargarReporte(String sUsername, String sPassword, String canalVenta, String ordenCompra,
			String patente, String codigo, String cantidad, String noPedido) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		HistorialPedidosPage historial = new HistorialPedidosPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		productosDisponibles.completarPedidoVenta(canalVenta, patente, ordenCompra, cantidad, codigo);
		historial.buscarNoPedido(noPedido);
		historial.descargarReporte();
		
		//Cancelar Pedido API
		productosDisponibles.cancelarPedidoApi();
	}

	@Test(description = "116-Back�Order - pedido back order", dataProvider = "historialEstadoPedidosCases")
	public void pedidoReservaPedidoBO(String sUsername, String sPassword, String canalVenta, String patente,
			String ordenCompra, String cantidad, String codigo) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		PedidosPage pedidos = new PedidosPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		productosDisponibles.completarPedidoVentaBackOrder(canalVenta, patente, ordenCompra, cantidad, codigo);
		productosDisponibles.clickBtnVolver();
		index.clickSeccionPedidos();
		pedidos.apartadoPedidoReserva();

		// Aserciones
		Assert.assertTrue(pedidos.validarResumenProductosPedidosBo(codigo, cantidad),
				"Se valida que el resumen de los productos sea correcto");
		global.log().info("Se valida resumen del pedido Back Order satisfactoriamente. ");
		
		//Cancelar Pedido API
		productosDisponibles.cancelarPedidoBoApi();
			
	}
	
	@Test(description = "136-Anular pedido  BO", dataProvider = "historialEstadoPedidosCases")
	public void cancerlarPedidoBackOrder(String sUsername, String sPassword, String canalVenta, String patente,
			String ordenCompra, String cantidad, String codigo) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);
		PedidosPage pedidos = new PedidosPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);
		
		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.nuevoPedido();
		global.log().info("Se ingresa a la secci�n de Cabecera de Pedidos.");
		productosDisponibles.completarPedidoVentaBackOrder(canalVenta, patente, ordenCompra, cantidad, codigo);
		productosDisponibles.clickBtnVolver();
		index.clickSeccionPedidos();
		pedidos.apartadoPedidoReserva();
		// Aserciones
		Assert.assertTrue(pedidos.validarResumenProductosPedidosBo(codigo, cantidad), "Se valida que el resumen de los productos sea correcto");
		global.log().info("Se valida resumen del pedido Back Order satisfactoriamente. ");
		
		//Cancelar Pedido
		pedidos.backResumen();
		pedidos.cancelarPedidoBoBtn();
		Assert.assertTrue(pedidos.cartelCancelacionConfirmada(), "Se valida cartel de cancelaci�n exitosa. ");
		global.log().info("Se cancel� el pedido satisfactoriamente. ");

	}

}
