package Portal;

import com.everis.global.Global;
import com.everis.po.LoginPage;
import com.everis.po.MontoDisponiblePage;
import com.everis.utilities.ExcelUtils;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class MontoDisponibleTest extends Base {
    Global global = new Global(driver);

    @DataProvider
    public Object[][] MontoDisponibleCases(ITestContext context) throws Exception {
        Object[][] testObjArray = ExcelUtils.getTableArray(
                super.getDataProviderDir() + context.getCurrentXmlTest().getParameter("dataProvider"),
                context.getCurrentXmlTest().getParameter("sheet"));
        return (testObjArray);
    }

    @Test(description = "198-Visualizacion detalle comprometido - Filtro ACC-0020", dataProvider = "MontoDisponibleCases")
    public void visualizacionDetalleComprometidoACC0020(String sUsername, String sPassword, String filtroAcc) {
        global.log().info("Inicia el caso de prueba");
        LoginPage login = new LoginPage(driver);
        MontoDisponiblePage montoPage = new MontoDisponiblePage(driver);

        login.executeLogin(sUsername, sPassword);
        global.log().info("Se valid� correctamente el inicio de sesi�n.");

        montoPage.seleccionarFiltroAcc(filtroAcc);
        montoPage.clickDetalle();

    }

    @Test(description = "199-Visualizacion detalle comprometido - Filtro ACC-0001", dataProvider = "MontoDisponibleCases")
    public void visualizacionDetalleComprometidoACC0001(String sUsername, String sPassword, String filtroAcc) {
        global.log().info("Inicia el caso de prueba");
        LoginPage login = new LoginPage(driver);
        MontoDisponiblePage montoPage = new MontoDisponiblePage(driver);

        login.executeLogin(sUsername, sPassword);
        global.log().info("Se valid� correctamente el inicio de sesi�n.");
        montoPage.seleccionarFiltroAcc(filtroAcc);
        montoPage.clickDetalle();

    }

    @Test(description = "200-Visualizacion detalle comprometido - Filtro ACC-0003", dataProvider = "MontoDisponibleCases")
    public void visualizacionDetalleComprometidoACC0003(String sUsername, String sPassword, String filtroAcc) {
        global.log().info("Inicia el caso de prueba");
        LoginPage login = new LoginPage(driver);
        MontoDisponiblePage montoPage = new MontoDisponiblePage(driver);

        login.executeLogin(sUsername, sPassword);
        global.log().info("Se valid� correctamente el inicio de sesi�n.");
        montoPage.seleccionarFiltroAcc(filtroAcc);
        montoPage.clickDetalle();

    }

    @Test(description = "201-Visualizacion detalle comprometido - Filtro ACC-0008", dataProvider = "MontoDisponibleCases")
    public void visualizacionDetalleComprometidoACC0008(String sUsername, String sPassword, String filtroAcc) {
        global.log().info("Inicia el caso de prueba");
        LoginPage login = new LoginPage(driver);
        MontoDisponiblePage montoPage = new MontoDisponiblePage(driver);

        login.executeLogin(sUsername, sPassword);
        global.log().info("Se valid� correctamente el inicio de sesi�n.");
        montoPage.seleccionarFiltroAcc(filtroAcc);
        montoPage.clickDetalle();

    }

    @Test(description = "225-Visualizacion  - Resumen Tipos comprometidos - Pedidos Pendientes - Filtro ACC-0003", dataProvider = "MontoDisponibleCases")
    public void visualizacionPedidosPendientesACC0003(String sUsername, String sPassword, String filtroAcc) {
        global.log().info("Inicia el caso de prueba");
        LoginPage login = new LoginPage(driver);
        MontoDisponiblePage montoPage = new MontoDisponiblePage(driver);

        login.executeLogin(sUsername, sPassword);
        global.log().info("Se valid� correctamente el inicio de sesi�n.");


        montoPage.seleccionarFiltroAcc(filtroAcc);
        montoPage.verPedidosLiberados();

        Assert.assertTrue(montoPage.validarSeccionPedidosLiberados(), "Se valida que el click abre el detalle de pedidos liberados");

    }

    @Test(description = "226-Visualizacion  - Resumen Tipos comprometidos - Partidas Abiertas Deudoras - Filtro ACC-0003", dataProvider = "MontoDisponibleCases")
    public void visualizacionPartidasAbiertasDeudorasACC0003(String sUsername, String sPassword, String filtroAcc) {
        global.log().info("Inicia el caso de prueba");
        LoginPage login = new LoginPage(driver);
        MontoDisponiblePage montoPage = new MontoDisponiblePage(driver);

        login.executeLogin(sUsername, sPassword);
        global.log().info("Se valid� correctamente el inicio de sesi�n.");

        montoPage.seleccionarFiltroAcc(filtroAcc);
        montoPage.verPartidasAbiertasDeudoras();

        Assert.assertTrue(montoPage.validarTransladoACuentaCorriente(), "Se valida que el click lleva a cuenta corriente");

    }

    @Test(description = "227-Visualizacion  - Resumen Tipos comprometidos - Partidas Abiertas Acreedoras - Filtro ACC-0003", dataProvider = "MontoDisponibleCases")
    public void visualizacionPartidasAbiertasAcreedorasACC0003(String sUsername, String sPassword, String filtroAcc) {
        global.log().info("Inicia el caso de prueba");
        LoginPage login = new LoginPage(driver);
        MontoDisponiblePage montoPage = new MontoDisponiblePage(driver);

        login.executeLogin(sUsername, sPassword);
        global.log().info("Se valid� correctamente el inicio de sesi�n.");
        montoPage.seleccionarFiltroAcc(filtroAcc);
        montoPage.verPartidasAbiertasAcreedoras();

        Assert.assertTrue(montoPage.validarTransladoACuentaCorriente(), "Se valida que el click lleva a cuenta corriente");

    }

    @Test(description = "228-Visualizacion  - Resumen Tipos comprometidos - Cheques Pendientes de Acreditar  - Filtro ACC-0003", dataProvider = "MontoDisponibleCases")
    public void visualizacionChequesPendientesACC0003(String sUsername, String sPassword, String filtroAcc) {
        global.log().info("Inicia el caso de prueba");
        LoginPage login = new LoginPage(driver);
        MontoDisponiblePage montoPage = new MontoDisponiblePage(driver);

        login.executeLogin(sUsername, sPassword);
        global.log().info("Se valid� correctamente el inicio de sesi�n.");


        montoPage.seleccionarFiltroAcc(filtroAcc);
        montoPage.verChequesPendientesAcreditar();

        Assert.assertTrue(montoPage.validarTransladoACuentaCorriente(), "Se valida que el click lleva a cuenta corriente");

    }

    @Test(description = "235-Visualizacion  - Resumen Tipos comprometidos - Pedidos Pendientes - Filtro ACC-0020", dataProvider = "MontoDisponibleCases")
    public void visualizacionPedidosPendientesACC0020(String sUsername, String sPassword, String filtroAcc) {
        global.log().info("Inicia el caso de prueba");
        LoginPage login = new LoginPage(driver);
        MontoDisponiblePage montoPage = new MontoDisponiblePage(driver);

        login.executeLogin(sUsername, sPassword);
        global.log().info("Se valid� correctamente el inicio de sesi�n.");

        montoPage.seleccionarFiltroAcc(filtroAcc);
        montoPage.verPedidosLiberados();

        Assert.assertTrue(montoPage.validarSeccionPedidosLiberados(), "Se valida que el click abre el detalle de pedidos liberados");

    }
}