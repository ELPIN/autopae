package Portal;


import com.everis.global.Global;
import com.everis.po.IndexPage;
import com.everis.po.LoginPage;
import com.everis.utilities.ExcelUtils;
import com.everis.utilities.ExtentReportUtil;
import com.everis.utilities.GenerateWord;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class LoginTest extends Base {
	 GenerateWord generateWord = new GenerateWord();
	Global global = new Global(driver);

	@DataProvider
	public Object[][] LoginCases(ITestContext context) throws Exception {
		Object[][] testObjArray = ExcelUtils.getTableArray(
				super.getDataProviderDir() + context.getCurrentXmlTest().getParameter("dataProvider"),
				context.getCurrentXmlTest().getParameter("sheet"));
		return (testObjArray);
	}

	@Test(description = "1-Ingresar Credenciales - Verificar campo usuario", dataProvider = "LoginCases")
	public void comprobarCampoUsuario(String sUsername, String sPassword) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		// Acciones

		login.setPassword(sPassword);

		ExtentReportUtil.INSTANCE.stepPass(driver, "Se ingresa el usuario:");
		generateWord.sendText("Se inici� correctamente la p�gina del Demoblaze");
		generateWord.addImageToWord(driver);

		login.clickSubmit();

		// Aserciones
		Assert.assertTrue(login.cartelErrorCampoUser(),
				"Valido el mensaje del cartel de error en el campo de usuario.");

	}

	@Test(description = "2-Ingresar Credenciales - Verificar campo Contrase�a", dataProvider = "LoginCases")
	public void comprobarCampoContrasena(String sUsername, String sPassword) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		// Acciones
		login.setUsername(sUsername);
		login.clickSubmit();

		// Aserciones
		Assert.assertTrue(login.cartelErrorCampoPassword(),
				"Valido el mensaje del cartel de error en el campo de contrase�a.");

	}

	@Test(description = "3-Ingresar Credenciales - Verificar campo  Usuario y Contrase�a")
	public void comprobarCamposUsuarioYContrasena() {

		global.log().info("Inicia el caso de prueba:");
		LoginPage login = new LoginPage(driver);

		// Acciones
		login.clickSubmit();

		// Aserciones
		Assert.assertTrue(login.cartelErrorCampoUser(),
				"Valido el mensaje del cartel de error en el campo de usuario.");
		Assert.assertTrue(login.cartelErrorCampoPassword(),
				"Valido el mensaje del cartel de error en el campo de contrase�a.");

	}

	@Test(description = "4-Ingresar Credenciales - Usuario  incorrecto", dataProvider = "LoginCases")
	public void usuarioInvalido(String sUsername, String sPassword) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);

		// Acciones
		login.setUsername(sUsername);
		login.setPassword(sPassword);
		login.clickSubmit();

		// Aserciones
		String errorCartelDatoLogInvalido = login.errorPageCartelDatoLogInvalido();
		Assert.assertTrue(
				errorCartelDatoLogInvalido.contains("The username or password provided in the request are invalid."));

	}

	@Test(description = "5-Ingresar Credenciales - Contrase�a  incorrecta", dataProvider = "LoginCases")
	public void contrasenaInvalida(String sUsername, String sPassword) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);

		// Acciones
		login.setUsername(sUsername);
		login.setPassword(sPassword);
		login.clickSubmit();

		// Aserciones
		String errorCartelDatoLogInvalido = login.errorPageCartelDatoLogInvalido();
		Assert.assertTrue(
				errorCartelDatoLogInvalido.contains("The username or password provided in the request are invalid."));

	}

	@Test(description = "6-Ejecuto login y valido usuario", dataProvider = "LoginCases")
	public void validLogin(String sUsername, String sPassword) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");

	}

	@Test(description = "7-Recuperar contrase�a - Opci�n 'Olvid� su contrase�a'")
	public void recuperarContrasena() {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);

		// Acciones
		login.clickOlvidoContrasena();

	}

	@Test(description = "8-Recuperar contrase�a - Formulario de verificaci�n de identidad - Enviar c�digo de verificaci�n", dataProvider = "LoginCases")
	public void recuperarContrasenaEnviarCodigo(String user) {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);

		// Acciones
		login.recuperarContrasenaUser(user);

	}

	@Test(description = "9-Recuperar contrase�a - Formulario de Cambio de Contrase�a-Bot�n Cancelar")
	public void recuperarContrasenaBtnCancelar() {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);

		// Acciones
		login.clickOlvidoContrasena();
		login.clickCancelar();

		// Aserciones
		Assert.assertTrue(login.headerIsVisible(), "Valido el mensaje de inicio de sesi�n correspondiente.");
		Assert.assertEquals(login.getHeaderText(), "Bienvenid@");
		global.log().info("Se valida que el sistema rediriga a la p�gina principal del login. ");

	}

	@Test(description = "13-Recuperar contrase�a -  Formulario de verificaci�n de identidad - Ingreso incorrecto de C�digo de Verificaci�n", dataProvider = "LoginCases")
	public void recuperarContrasenaCodigoInvalido(String user, String codigo) {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);

		// Acciones
		global.log().info("Se presiona opci�n Recuperar Contrase�a. ");
		login.recuperarContrasenaUser(user);
		login.setCodigoVerificacion(codigo);

		// Aserciones
		Assert.assertTrue(login.infoEmailFailText(), "Se valida cartel de error.");
		global.log().info("Se valida que aparece cartel de error indicando que el c�digo es inv�lido. ");
	}

	@Test(description = "14-Recuperar contrase�a -   Formulario de verificaci�n de identidad - Ingreso incorrecto de usuario", dataProvider = "LoginCases")
	public void recuperarContrasenaUsuarioInvalido(String user) {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);

		// Acciones
		login.clickOlvidoContrasena();
		login.setEmailRecuperarPass(user);

		// Aserciones
		Assert.assertTrue(login.errorUserFieldText(),
				"Se valida que aparezca cartel de error indicando que el email es inv�lido.");
		global.log().info("Se valida que aparezca cartel de error indicando que el email es inv�lido.");

	}

	@Test(description = "15-Recuperar contrase�a -  Formulario de verificaci�n de identidad - M�ximo de intentos de ingresos incorrectos de C�digo de Verificaci�n", dataProvider = "LoginCases")
	public void recuperarContrasenaLimiteIntentos(String user, String codigo) {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);

		// Acciones
		login.recuperarContrasenaUser(user);
		// Ingreso tres veces c�digo inv�lido
		login.setCodigoVerificacion(codigo);
		login.setCodigoVerificacion(codigo);
		login.setCodigoVerificacion(codigo);

		// Aserciones
		Assert.assertTrue(login.infoEmailFailNoRetryText(), "Se valida cartel de error.");
		global.log().info("Se valida que aparece cartel de error indicando que ha hecho demasiados intentos incorrectos.");
	}

	@Test(description = "18-Cerrar Sesi�n - Opci�n Mi cuenta", dataProvider = "LoginCases")
	public void cerrarSesionOpcionMiCuenta(String sUsername, String sPassword) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.clickProfileBtn();

		// Aserciones
		String btbSalir = index.bntSalir();
		Assert.assertTrue(btbSalir.contains("Salir"));
		global.log().info("Se valida que aparezca el bot�n 'Salir'. ");

	}

	@Test(description = "19-Cerrar Sesi�n - Opci�n Salir", dataProvider = "LoginCases")
	public void cerrarSesion(String sUsername, String sPassword) throws Exception {

		global.log().info("Inicia el caso de prueba");
		LoginPage login = new LoginPage(driver);
		IndexPage index = new IndexPage(driver);

		// Acciones
		login.executeLogin(sUsername, sPassword);
		global.log().info("Se valid� correctamente el inicio de sesi�n.");
		index.cerrarSesion();

	}

}