package com.everis.po;

import com.everis.global.Global;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class CabeceraPedidoPage extends Global {

	// Definir web elements a nivel de clase

	public CabeceraPedidoPage(WebDriver d) {
		super(d);
		PageFactory.initElements(driver, this);
	}

	public String getObtenerFechaActual() {
		return obtenerFechaActual();
	}

	public String getObtenerFechaDiaSiguienteDespacho() {
		return obtenerFechaDiaSiguienteDespacho();
	}

	public String getObtenerFechaValidaPedido() {
		return obtenerFechaValidaPedido();
	}

	public String errorItemCartelFechaDespacho() {
		return getText(cartelErrorFechaDespacho);
	}

	public String errorItemCartelCanalVta() {
		return getText(cartelErrorCanalVta);
	}

	public String errorItemCartelPatente() {
		return getText(cartelErrorPatente);
	}

	public String btnActualizar() {
		return getText(btnActualizar);
	}

	@FindBy(xpath = "//input[@placeholder='Seleccionar fecha']")
	private WebElement iconoCalendario;

	@FindBy(xpath = "//div[@class='invalid-feedback' and contains(., 'Por favor ingrese una fecha preferente de despacho.')]")
	private WebElement cartelErrorFechaDespacho;

	@FindBy(xpath = "//div[@class='invalid-feedback' and contains(., 'Por favor ingrese un canal de ventas.')]")
	private WebElement cartelErrorCanalVta;

	@FindBy(xpath = "//div[@class='invalid-feedback' and contains(., 'Por favor ingrese una patente.')]")
	private WebElement cartelErrorPatente;

	@FindBy(xpath = "//select[@formcontrolname='canalDeVenta']")
	private WebElement selectElementCanalVenta;

	@FindBy(xpath = "//select[@formcontrolname='contrato']")
	private WebElement selectElementContrato;

	@FindBy(xpath = "//input[@placeholder=\"Ingrese una patente, por ejemplo 'AR123CC'\"]")
	private WebElement inputPatente;

	@FindBy(xpath = "//button[@role='option' and contains(., 'ARCBG122')]")
	private WebElement optionPatente;

	@FindBy(xpath = "//input[@placeholder='Ingresar n�mero de compra']")
	private WebElement completarOrdenCompra;

	@FindBy(xpath = "//button[@type='submit' and @class='btn btn-primary p-2 border-radius w-100']")
	private WebElement btnContinuar;

	@FindBy(xpath = "//div[@class='mb-3' and contains(., 'Buscando contratos vigentes para el canal seleccionado...')]")
	private WebElement cartelCargandoCanalesVta;

	@FindBy(xpath = "//button[@type='submit' and contains(., 'Actualizar')]")
	private WebElement btnActualizar;
	
	@FindBy(xpath = "//tbody//tr[@class='flex-row flex-wrap d-flex font-weight-medium abs-center']")
	private WebElement grillaProductosDisponibles;
	
	@FindBy(xpath = "//input[@placeholder='No se indica patente' and @disabled='disabled']")
	private WebElement cartelNoPatente;

	@FindBy(xpath = "//div//h4[contains (.,'Datos del Pedido')]")
	private WebElement tituloDatosDelPedido;
	
	private String waitCnalVta = "//div[@class='mb-3' and contains(., 'Buscando contratos vigentes para el canal seleccionado...')]";
	private String patenteOption = "//button[@role='option' and contains(., 'textoButton')]";
	private String textoButton = "textoButton";
	private String formatDate = "dd/MM/yyyy";
	private String etiqueraProductosDisponibles = "Productos disponibles";
	private String cabeceraSuccess = "Se completa la cabecera del pedido de manera satisfactoria. ";

	// Steps
	
	public Boolean disabledPatenteIsPresent() {
		waitForVisibility(cartelNoPatente);
		return cartelNoPatente.isDisplayed();
	}

	Boolean isPresent = driver
			.findElements(By.xpath("//input[@placeholder='No se indica patente' and @disabled='disabled']")).size() > 0;

	public String obtenerFechaActual() {
		DateFormat dateFormat = new SimpleDateFormat(formatDate);
		Date date = new Date();

		return dateFormat.format(date);

	}

	private String obtenerFechaValidaPedido() {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat(formatDate);
		sdf.format(date);
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, 7);
		return sdf.format(c.getTime());

	}

	private String obtenerFechaDiaSiguienteDespacho() {
		Date date = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat(formatDate);
		sdf.format(date);
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DATE, 1);
		return sdf.format(c.getTime());

	}

	public void completarFechaPedido(String fecha) {

		List<WebElement> inputs = driver.findElements(By.xpath("//input[@placeholder='Seleccionar fecha']"));

		for (WebElement input : inputs) {
			((JavascriptExecutor) driver).executeScript("arguments[0].removeAttribute('readonly','readonly')", input);
		}
		
		sendKeys(iconoCalendario,fecha,"Se ingrensa fecha preferente de despacho.");
		iconoCalendario.sendKeys(Keys.ENTER);
		
	}
	
	public void seleccionarCanalVenta(String canal) throws Exception {

		Select selectObject = new Select(selectElementCanalVenta);
		try {
			selectObject.selectByVisibleText(canal);
			new WebDriverWait(driver, 20).until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(waitCnalVta)));
			
			log().info("Se selecciona el Canal de Venta.");
		} catch (Exception e) {
			log().info("No se pudo seleccionar el Canal de Venta.");
			throw new Exception("No se pudo seleccionar el Canal de Venta.");

		}

	}

	public void seleccionarContrato(String contrato) throws Exception {

		Select selectObject = new Select(selectElementContrato);
		try {
			selectObject.selectByVisibleText(contrato);
			log().info("Se selecciona el contrato. ");
		} catch (Exception e) {
			log().info("No se pudo seleccionar el contrato. ");
			throw new Exception("No se pudo seleccionar el contrato. ");

		}
	}

	public void seleccionarPatente(String patente) {

		String xpathButton = reemplazarEnXpath(patenteOption, textoButton, patente);
		
		clear(inputPatente);
		sendKeys(inputPatente,patente,"Se ingresa patente: " + patente);
		driver.findElement(By.xpath(xpathButton)).click();
		log().info("Se selecciona Patente.");
	}

	public void ordenCompra(String ordenCompra) {

		sendKeys(completarOrdenCompra,ordenCompra,"Se completa orden de compra.");

	}

	public void clickbtnContinuar() {

		click(btnContinuar,"Se presiona bot�n continuar.");
		
	}

	// Actions

	public void completarCabeceraPedidoVta(String canalVenta, String patente, String ordenCompra) throws Exception {

		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);
		try {
			seleccionarCanalVenta(canalVenta);
			completarFechaPedido(getObtenerFechaValidaPedido());
			seleccionarPatente(patente);
			ordenCompra(ordenCompra);
			clickbtnContinuar();
			String cartelProductosDisponibles = productosDisponibles.cartelProductosDisponibles();
			Assert.assertTrue(cartelProductosDisponibles.contains(etiqueraProductosDisponibles));
			log().info(cabeceraSuccess);
		} catch (Exception e) {

			throw new Exception("No se pudo completar la cabecera del pedido.");

		}
	}

	public void completarCabeceraPedidoVtaCif(String canalVenta, String ordenCompra) throws Exception {

		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);
		try {
			seleccionarCanalVenta(canalVenta);
			completarFechaPedido(getObtenerFechaValidaPedido());
			ordenCompra(ordenCompra);
			clickbtnContinuar();
			String cartelProductosDisponibles = productosDisponibles.cartelProductosDisponibles();
			Assert.assertTrue(cartelProductosDisponibles.contains(etiqueraProductosDisponibles));
			log().info(cabeceraSuccess);
		} catch (Exception e) {

			throw new Exception("No se pudo completar la cabecera del pedido.");

		}
	}

	public void completarCabeceraPedidoVtaCifConContrato(String canalVenta, String contrato, String ordenCompra)
			throws Exception {

		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);

		seleccionarCanalVenta(canalVenta);
		seleccionarContrato(contrato);
		completarFechaPedido(getObtenerFechaValidaPedido());
		ordenCompra(ordenCompra);
		clickbtnContinuar();
		String cartelProductosDisponibles = productosDisponibles.cartelProductosDisponibles();
		Assert.assertTrue(cartelProductosDisponibles.contains(etiqueraProductosDisponibles));
		log().info(cabeceraSuccess);
		Thread.sleep(5000);

	}

	public void completarCabeceraPedidoVtaFobConContrato(String canalVenta, String contrato, String patente,
			String ordenCompra) throws Exception {

		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);

		seleccionarCanalVenta(canalVenta);
		seleccionarContrato(contrato);
		completarFechaPedido(getObtenerFechaValidaPedido());
		seleccionarPatente(patente);
		ordenCompra(ordenCompra);
		clickbtnContinuar();
		String cartelProductosDisponibles = productosDisponibles.cartelProductosDisponibles();
		Assert.assertTrue(cartelProductosDisponibles.contains(etiqueraProductosDisponibles));
		log().info(cabeceraSuccess);
		Thread.sleep(5000);

	}

	@SuppressWarnings("deprecation")
	public void validarHoraCorte(String canalVenta, String cantidad, String codigoStock) throws Exception {

		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);
		Date date = new Date();
		CarritoPage carrito = new CarritoPage(driver);
		CabeceraPedidoPage cabeceraPedido = new CabeceraPedidoPage(driver);
		DateFormat hourFormat = new SimpleDateFormat("HHmm");
		int horaActual = Integer.parseInt(hourFormat.format(date));
		int diaActual = date.getDay();

		if (horaActual <= 1130 && diaActual <= 4) {

			try {
				log().info("Se valid� el horario antes del corte y que el d�a de ma�ana este habilitado para despacho. ");
				completarFechaPedido(getObtenerFechaDiaSiguienteDespacho());
				log().info("Se selecciona como Fecha preferente de Despacho el d�a de ma�ana. ");
				cabeceraPedido.seleccionarCanalVenta(canalVenta);
				clickbtnContinuar();
				String cartelProductosDisponibles = productosDisponibles.cartelProductosDisponibles();
				Assert.assertTrue(cartelProductosDisponibles.contains(etiqueraProductosDisponibles));
				log().info(cabeceraSuccess);
				log().info("Se despliega lista de Productos Disponibles.");
				productosDisponibles.agregarCantidad(cantidad, codigoStock);
				productosDisponibles.agregarProducto(codigoStock);
				log().info("Se agrega una cantidad menor del producto a la disponible.");
				carrito.generarPedido();
				log().info("Se presiona bot�n de generar pedido.");
				productosDisponibles.clickConfirmarPedido();
			} catch (Exception e) {

				throw new Exception("Ocurri� un error en la generaci�n del pedido. ");

			}
		} else {
			completarFechaPedido(getObtenerFechaDiaSiguienteDespacho());
			// Aserciones
			String errorCartelFechaDespacho = cabeceraPedido.errorItemCartelFechaDespacho();
			Assert.assertTrue(errorCartelFechaDespacho.contains("Por favor ingrese una fecha preferente de despacho."));
			log().info(
					"La hora de cut off ha sido superada o el d�a de ma�ana est� inhabilitado, se debe elegir una Fecha preferente de Despacho distinta a ma�ana. ");
		}

	}

	public boolean validarTituloCabeceraPedido(){
		return tituloDatosDelPedido.isDisplayed();
	}
}
