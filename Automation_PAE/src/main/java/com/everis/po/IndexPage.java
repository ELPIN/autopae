package com.everis.po;


import com.everis.global.Global;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class IndexPage extends Global {

	// Definir web elements a nivel de clase

	public IndexPage(WebDriver d) {
		super(d);
		PageFactory.initElements(driver, this);
	}




	// comentario
	public static String getNotificacionesTxt() {
		return notificacionesTxt;
	}

	private void setNotificacionesTxt(String notificacionesTxt) {
		IndexPage.notificacionesTxt = notificacionesTxt;
		log().info("Se obtiene el n�mero de notificaciones.");
	}

	private static String notificacionesTxt;

	@FindBy(id = "profile-button")
	private WebElement profileButton;

	@FindBy(xpath = "//span[@class='navigation-icons pedidos']")
	private WebElement seccionPedidos;

	@FindBy(xpath = "//li[@id='nav-delivery' and contains (.,'Gu�as r�pidas de uso')]")
	private WebElement seccionGuiasUso;

	@FindBy(xpath = "//li[@id='nav-delivery' and contains (.,'T�rminos y Condiciones')]")
	private WebElement seccionAprobaciones;

	@FindBy(id = "nav-cuenta-corriente")
	private WebElement seccionCtaCorriente;

	@FindBy(xpath = "//li[@id='nav-delivery' and contains (.,'Preguntas frecuentes')]")
	private WebElement seccionPreguntasFrecuentes;

	@FindBy(id = "header-button-salir")
	private WebElement salirButton;

	@FindBy(id = "header-button-perfil")
	private WebElement btnMiCuenta;

	@FindBy(name = "chevron-down")
	private WebElement nuevoPedidoButton;

	@FindBy(xpath = "//div//a[@class='btn py-2 mb-2 dropdown-item mb-2 text-center border-radius bg-secondary text-white' and contains(., 'Lubricantes')]")
	private WebElement lubricanteOptionButton;

	@FindBy(xpath = "//div//h3[@class='text-left text-md-center text-lg-left mb-2' and contains(., ' �Bienvenid@! ')]")
	private WebElement mensajeConfirmacion;

	@FindBy(xpath = "//div//a[@class='nav-toggler topbar-item justify-content-center ml-0']")
	private WebElement lateralMenu;

	@FindBy(xpath = "//div//h3[@class='text-left text-md-center text-lg-left mb-2' and contains(., '�Bienvenid@!')]")
	private WebElement headerIndex;

	@FindBy(xpath = "//div//select[@id='shiptos-header-dropdown' ]")
	private WebElement listaShipTo;

	@FindBy(xpath = "//div[@class='modal-content']//div//button[contains(., 'Aceptar') ]")
	private WebElement btnAceptarCambioDeShipTo;

	@FindBy(xpath = "(//select[@id='shiptos-header-dropdown'])[2]")
	private WebElement listaShipTo2;

	@FindBy(xpath = "//div//div[contains(.,'Se perder�n los cambios que no hayan sido guardados. �Desea continuar?')]//div//button[@class='btn btn-success']")
	private WebElement btnAceptarShipTo;

	@FindBy(xpath = "//a[@data-target='#notificacion' and @class='nav-link notificacion']")
	private WebElement btnNotificaciones;

	private String optionShipTo = "//select[@id='shiptos-header-dropdown']//option[contains(.,'textoButton')]";
	private String textoButton = "textoButton";
	
	boolean validarLogin = false;

	// Steps

	public String bntSalir() {
		return getText(salirButton);
	}

	public String getHeaderText() {
		return getText(mensajeConfirmacion);
	}

	public String getIconoNotificacionesText() {
		return getText(btnNotificaciones);

	}

	public Boolean cartelConfirmarShipTo() {
		return btnAceptarShipTo.isDisplayed();
	}

	public Boolean iconoNotificacionesIsDisplayed() {
		return btnNotificaciones.isDisplayed();
	}

	public Boolean headerIsVisible() {
		waitForVisibility(headerIndex);
		return headerIndex.isDisplayed();
	}

	public boolean validarShiptoDefecto() {
		try {
			cartelConfirmarShipTo();
			log().info("Se valida cartel de confirmaci�n del Shipto. ");
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	public boolean validarIconoNotificaciones() {
		try {
			iconoNotificacionesIsDisplayed();
			log().info("Se valida �cono Campana de Notificaciones. ");
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	public void clickProfileBtn() {
		click(profileButton, "Se presiona el bot�n del perfil.");
	}

	public boolean cartelAtencionPedidoObsoleto() {
		try {
			driver.findElement(By.xpath("//input[@formcontrolname='contrato']")).isEnabled();
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	public boolean btnPerfil() {
		try {
			waitForVisibility(btnMiCuenta);
			btnMiCuenta.isDisplayed();
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	public boolean btnAprobaciones() {
		try {

			click(lateralMenu, "Se despliega men� lateral. ");
			Thread.sleep(3000);
			waitForVisibility(seccionAprobaciones);
			seccionAprobaciones.isDisplayed();
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	// Actions

	public void clickSeccionGuiasRapidasUso() throws InterruptedException {
		GuiasRapidasUsoPage guias = new GuiasRapidasUsoPage(driver);

		click(lateralMenu, "Se despliega men� lateral. ");
		Thread.sleep(3000);

		click(seccionGuiasUso, "Se presiona el bot�n del la secci�n de Gu�as r�pidas de uso.");

		// Asersiones
		Assert.assertTrue(guias.headerGuias().contains("�En qu� te podemos ayudar?"),
				"Se valida que aparezca el header de la secci�n de Gu�as.");
		log().info("Se valida que se ingres� a la secci�n de Gu�as r�pidas de uso.");

	}

	public void clickSeccionPreguntasFrecuentes() throws InterruptedException {

		click(lateralMenu, "Se despliega men� lateral. ");
		Thread.sleep(3000);

		click(seccionPreguntasFrecuentes, "Se presiona el bot�n del la secci�n de Preguntas Frecuentes.");

	}

	public void clickSeccionPedidos() throws InterruptedException {
		PedidosPage pedidos = new PedidosPage(driver);

		// Paso necesario cuando el navegador no se maximiza
		Thread.sleep(2000);
		click(lateralMenu, "Se despliega men� lateral. ");
		Thread.sleep(2000);

		click(seccionPedidos, "Se ingresa a la secci�n de Pedidos.");
		Assert.assertTrue(pedidos.etiquetaEdicionPedidos(), "Se valida que aparezca la etiqueta de Pedidos. ");
		log().info("Se valida que se ingres� a la secci�n de pedidos. ");
	}

	public void clickSeccionAprobaciones() throws InterruptedException {
		AprobacionesPage aprobaciones = new AprobacionesPage(driver);

		setNotificacionesTxt(getIconoNotificacionesText());

		click(lateralMenu, "Se despliega men� lateral. ");
		Thread.sleep(3000);

		click(seccionAprobaciones, "Se presiona el bot�n del la secci�n de Aprobaciones.");

		log().info("Se valida que se ingres� a la secci�n de Aprobaciones. ");

		Thread.sleep(3000);

	}
	
	public void clickSeccionCuentaCorriente() throws Exception {
		CuentaCorrientePage cuentaCorriente = new CuentaCorrientePage(driver);

		click(lateralMenu, "Se despliega men� lateral. ");
		Thread.sleep(3000);

		click(seccionCtaCorriente, "Se presiona el bot�n del la secci�n de Cuenta Corriente.");

		// Asersiones
		Boolean b = cuentaCorriente.grillaCtaCorriente();

		/*if (Boolean.TRUE.equals(b)) {
			log().info("Se valida presencia de datos en la grilla de Partidas Abiertas de Cuenta Corriente.");
		} else {
			log().info("No se valid� correctamente la grilla de Partidas Abiertas de Cuenta Corriente.");
			throw new Exception("No se valid� correctamente la grilla de Partidas Abiertas de Cuenta Corriente.");
		}*/

	}

	public void clickBtnNotificaciones() {

		AprobacionesPage aprobaciones = new AprobacionesPage(driver);

		iconoNotificacionesIsDisplayed();
		setNotificacionesTxt(getIconoNotificacionesText());
		click(btnNotificaciones, "Se presiona la Campana de Notificaciones. ");

		log().info("Se valida que se ingres� a la secci�n de Aprobaciones. ");

	}

	public void cerrarSesion() {
		LoginPage login = new LoginPage(driver);

		click(profileButton, "Se presiona bot�n del Perfil.");
		click(salirButton, "Se presiona bot�n Salir.");

		// Aserciones
		Assert.assertTrue(login.headerIsVisible(), "Valido el mensaje de inicio de sesi�n correspondiente.");
		Assert.assertEquals(login.getHeaderText(), "Bienvenid@");
	}

	public void nuevoPedido() {
		click(nuevoPedidoButton, "Se presiona la opci�n de Nuevo Pedido.");
		click(lubricanteOptionButton, "Se selecciona la opci�n Lubricantes.");
	}

	public void seccionMiCuenta() throws InterruptedException {
		MiCuentaPage miCuenta = new MiCuentaPage(driver);

		click(profileButton, "Se presiona bot�n de Pefil.");
		click(btnMiCuenta, "Se presiona bot�n de Mi Cuenta.");

		// Aserciones
		log().info("Se ingresa a la secci�n de Mi Cuenta. ");
		Thread.sleep(5000);

	}

	public void seleccionarShipTo() throws Exception {

		try {
			Thread.sleep(4000);
			click(listaShipTo2, "Se despliega lista de ShipTo.");
			String xpathButton = reemplazarEnXpath(optionShipTo, textoButton, MiCuentaPage.getNumeroShipto());
			driver.findElement(By.xpath(xpathButton)).click();
			log().info("Se selecciona ShipTo habilitado: " + MiCuentaPage.getNumeroShipto());

			if (validarShiptoDefecto() == true) {
				click(btnAceptarShipTo, "Se confima el Shipto seleccionado.");
			} else {
				log().info("El ShipTo seleccionado: " + MiCuentaPage.getNumeroShipto()
						+ " se encuentra configurado por defecto");
			}
		} catch (Exception e) {
			log().info("No se encontr� el ShipTo: " + MiCuentaPage.getNumeroShipto());
			throw new Exception("Ocurri� un error al seleccionar el Shipto.");
		}

	}

	public void cambiarShipTo(String shipToName){
		Select selectObject = new Select(listaShipTo);
		selectObject.selectByVisibleText(shipToName);
		click(btnAceptarCambioDeShipTo, "Se clickea sobre el aceptar sobre el modal para cambiar shipto");
		log().info("Se selecciona el Ship to: " + shipToName);
	}

	public void validarNotificacionesPendientes() throws InterruptedException {
		AprobacionesPage aprobaciones = new AprobacionesPage(driver);

		Assert.assertTrue(validarIconoNotificaciones(), "Se valida �cono de Campana de Notificaciones. ");

		if (getIconoNotificacionesText().equalsIgnoreCase("")) {

			log().info("El usuario no tiene notificaciones pendientes.");
			clickSeccionAprobaciones();
			Assert.assertEquals(
					aprobaciones.getGrillaAprobacionesPendientes(), "No se encuentran Aceptaciones pendientes");
			log().info("Se valida que aparezca cartel de 'No se encuentran Aprobaciones pendientes");

		} else {
			log().info("El usuario tiene: " + getIconoNotificacionesText() + " notificaciones pendientes.");

		}

	}

}
