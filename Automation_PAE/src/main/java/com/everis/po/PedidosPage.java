package com.everis.po;

import com.everis.global.Global;
import io.restassured.path.json.JsonPath;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.List;

public class PedidosPage extends Global {

	// Definir web elements a nivel de clase

	public PedidosPage(WebDriver d) {
		super(d);
		PageFactory.initElements(driver, this);
	}

	public String cartelPedidoNoModificable() {
		return getText(cartelPedidoNoModificable);
	}

	public String validarCantidadResumen() {
		return getText(validarCantidadSolicitada.get(1));
	}

	public String validarProductoResumen() {
		return validarCodigoProducto.getText();
	}

	public String validarNumeroResumen() {
		return getText(validarNumeroPedido);
	}

	public String headerNumeroPedido() {
		return getText(grillaBusquedaPedidos);
	}

	@FindBy(id = "nav-delivery")
	private WebElement seccionPedidos;

	@FindBy(xpath = "//input[@formcontrolname='contrato']")
	private WebElement campoContrato;

	@FindBy(xpath = "//div[contains (., 'Atenci�n')]//div[@class='modal-body']")
	private WebElement cartelPedidoNoModificable;

	@FindBy(xpath = "//button[@class='btn btn-lg btn-success' and contains (.,'S�, deseo cancelarlo')]")
	private WebElement btnValidarCancelacion;

	@FindBy(xpath = "//tbody//tr//td//a[contains (., 'Editar Pedido')]")
	private WebElement iconoEditarPedido;

	@FindBy(xpath = "//div[@class='ml-auto float-right btn-group']//a[@data-toggle='collapse']")
	private WebElement btnPedidoReserva;

	@FindBy(xpath = "//tbody//tr//td//span")
	private WebElement detalleNumeroPedido;

	@FindBy(xpath = "//div//tbody//tr//td[@class='col-3 col-md-3 col-sm-3 abs-center text-wrap']")
	private List<WebElement> validarCantidadSolicitada;

	@FindBy(xpath = "//div//tbody//tr[@class='font-weight-medium position-relative justify-content-between justify-content-md-start']")
	private WebElement validarCodigoProducto;

	@FindBy(xpath = "//app-modal-detalle-pedido//div//div//div//div[contains (., 'N�mero de Pedido')]//p")
	private WebElement validarNumeroPedido;

	@FindBy(xpath = "//table[@id='table-history']//tbody")
	private WebElement grillaBusquedaPedidos;

	@FindBy(xpath = "//div[@class='modal-content m-auto modal-search-dialog modal-lg border-0']//a[@class='closebtn hoverIcon']//span")
	private WebElement btnAtrasResumen;

	@FindBy(xpath = "//div[@class='card pedidos']//div//h4[contains (., 'Pedidos')]")
	private WebElement headerPedidos;

	@FindBy(xpath = "//div[@class='card']//div//h4[@class='headline']")
	private WebElement datosPedidos;

	@FindBy(xpath = "//div[contains (., 'Atenci�n')]//div[@class='modal-body']")
	private WebElement cartelConfirmacionCancelar;

	@FindBy(xpath = "//div[contains (., 'Informaci�n')]//div[@class='modal-body']")
	private WebElement confirmacionAceptada;

	@FindBy(xpath = "//div//h5[contains (.,'L�quidos')]")
	private WebElement textoNuevosPedidosLiquidos;

	@FindBy(xpath = "//div//h5[contains (.,'Pesados')]")
	private WebElement textoNuevosPedidosPesados;

	@FindBy(xpath = "//div//h5[contains (.,'L�quidos')]/../a[contains (., 'Nuevo Pedido')]")
	private  WebElement btnNuevosPedidosLiquidos;

	@FindBy(xpath = "//div//h5[contains (.,'Pesados')]/../a[contains (., 'Nuevo Pedido')]")
	private WebElement btnNuevosPedidosPesados;


	private String checkBoxProducto = "//tr//td[contains (., 'nroDeProducto')]/parent::tr//td//div[@class='form-check abs-center']//input[@type='checkbox']";
	private String btnDeUnPedidoReserva = "//td//span[@data-toggle='modal' and contains (., 'nroDePedido')]";
	private String btnRepetirPedido = "(//tbody//tr[td/span[contains(.,'textoButton')]]//div//a)[2]";
	private String xpathProducto = "//div//tbody//tr[@class='font-weight-medium position-relative justify-content-between justify-content-md-start' and contains (., 'textoButton')]";
	private String iconoNumeroPedido = "//td//span[contains(.,'textoButton')]";
	private String cancelarPedidoReserva = "//tbody//tr[td/span[contains(.,'textoButton')]]//div//a[@class='btn rounded btn-outline-dark border-light cross dropdown-toggle tooltip-open tp-transparent']";
	private String textoButton = "textoButton";

	// Steps



	public Boolean etiquetaEdicionPedidos() {
		waitForVisibility(headerPedidos);
		return headerPedidos.isDisplayed();
	}

	public Boolean headerDatosPedido() {
		waitForVisibility(datosPedidos);
		return datosPedidos.isDisplayed();
	}

	public Boolean cartelConfirmarCancelar() {
		waitForVisibility(cartelConfirmacionCancelar);
		return cartelConfirmacionCancelar.isDisplayed();
	}

	public Boolean cartelCancelacionConfirmada() {
		waitForVisibility(confirmacionAceptada);
		return confirmacionAceptada.isDisplayed();
	}

	private boolean cartelAtencionEditarPedido() {
		try {
			cartelPedidoNoModificable.isDisplayed();
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	public boolean campoContratoStatus() {
		log().info("Se valida el campo contrato. ");

		try {
			Assert.assertEquals(campoContrato.getAttribute("disabled"), "true");
			log().info("Se busca que el atributo 'disabled' sea 'true'. ");
			return true;

		} catch (Exception e) {
			log().info("El atributo 'disabled' es 'false'. ");
			return false;
		}

	}

	public void apartadoPedidoReserva() throws Exception {

		click(btnPedidoReserva, "Se presiona bot�n de pedido de reserva.");
		Thread.sleep(3000);
	}

	public void backResumen() throws InterruptedException {
		click(btnAtrasResumen, "Se cierra la ventana del resumen.");
	}

	// Actions

	public void editarPedidoCanalVenta() {

		try {
			click(iconoEditarPedido, "Se presiona bot�n de editar pedido.");
			if (cartelAtencionEditarPedido() == false) {
				log().info("Se valida que el pedido sea editable. ");

			} else {
				log().info("No se puede editar el pedido, se visualiza cartel de Atenci�n con mensaje: "
						+ cartelPedidoNoModificable());

				throw new IllegalArgumentException("No se pudo editar el pedido.");
			}

		} catch (Exception e) {

			throw new IllegalArgumentException("No se pudo editar el pedido.");
		}
	}

	public void repetirPedido(String noPedido) throws Exception {
		HistorialPedidosPage historial = new HistorialPedidosPage(driver);

		historial.buscarNoPedido(noPedido);
		try {
			String xpathButton = reemplazarEnXpath(btnRepetirPedido, textoButton, ProductosDisponiblesPage.getNumeroPedido());
			driver.findElement(By.xpath(xpathButton)).click();
			log().info("Se presiona el bot�n de Repetir Pedido. ");
		} catch (Exception e) {
			log().info("El pedido con n�mero: " + ProductosDisponiblesPage.getNumeroPedido() + " no es interactuable.");
			throw new Exception("El pedido con n�mero: " + ProductosDisponiblesPage.getNumeroPedido() + " no es interactuable");
		}

		Assert.assertTrue(headerDatosPedido(), "Se valida que aparezca la etiqueta de Datos del Pedido. ");
		log().info("Se valida que aparezca la etiqueta de Datos del Pedido. ");

	}

	public void iconoNumeroPedido() throws Exception {

		String xpathButton = reemplazarEnXpath(iconoNumeroPedido, textoButton,
				ProductosDisponiblesPage.getNumeroPedido());
		log(). info(xpathButton);
		driver.findElement(By.xpath(xpathButton)).click();
		log().info("Se presiona el n�mero del pedido para desplegar el resumen. ");

	}
	
	public void iconoNumeroPedidoBo() throws Exception {

		String xpathButton = reemplazarEnXpath(iconoNumeroPedido, textoButton,
				ProductosDisponiblesPage.getNumeroPedidoBo());
		log(). info(xpathButton);
		driver.findElement(By.xpath(xpathButton)).click();
		log().info("Se presiona el n�mero del pedido BO para desplegar el resumen. ");

	}

	public boolean validarResumenProductosPedidos(String codigo, String cantidad) {

		try {

			iconoNumeroPedido();
			Thread.sleep(7000);

			// valida n�mero de pedido en el resumen
			String numeroPedido = validarNumeroResumen();
			Assert.assertTrue(numeroPedido.contains(ProductosDisponiblesPage.getNumeroPedido()));
			log().info("Se valida el n�mero del pedido en el resumen. ");

			// valida producto del pedido

			String xpathButton = reemplazarEnXpath(xpathProducto, textoButton, codigo);
			Assert.assertTrue(xpathButton.contains(codigo));
			log().info("Se valida el c�digo del producto en el pedido. ");

			// valida cantidad de productos en el resumen
			String cantidadProducto = validarCantidadResumen();
			Assert.assertTrue(cantidadProducto.contains(cantidad));
			log().info("Se valida la cantidad del pedido. ");

			return true;
		} catch (Exception e) {
			log().info("Hubo un error en la validaci�n del resumen del pedido. ");
			return false;

		}

	}
	
	public boolean validarResumenProductosPedidosBo(String codigo, String cantidad) throws Exception {

		//try {

			iconoNumeroPedidoBo();
			Thread.sleep(7000);

			// valida n�mero de pedido en el resumen
			String numeroPedido = validarNumeroResumen();
			Assert.assertTrue(numeroPedido.contains(ProductosDisponiblesPage.getNumeroPedidoBo()));
			log().info("Se valida el n�mero del pedido en el resumen. ");

			// valida producto del pedido

			String xpathButton = reemplazarEnXpath(xpathProducto, textoButton, codigo);
			Assert.assertTrue(xpathButton.contains(codigo));
			log().info("Se valida el c�digo del producto en el pedido. ");

			// valida cantidad de productos en el resumen
			String cantidadProducto = validarCantidadResumen();
			log().info(cantidad);
			log().info(cantidadProducto);
			Assert.assertTrue(cantidadProducto.contains(cantidad));
			log().info("Se valida la cantidad del pedido. ");

			return true;
		/*} catch (Exception e) {
			log().info("Hubo un error en la validaci�n del resumen del pedido. ");
			return false;

		}*/

	}

	public void validarHeaderPedido() throws Exception {

		// valida en el header el n�mero del pedido generado
		log().info("Se valida que el n�mero del pedido se encuentre en la grilla. ");
		String numeroPedido = headerNumeroPedido();

		if (numeroPedido.contains(ProductosDisponiblesPage.getNumeroPedido())) {
			log().info("El n�mero del pedido se encontr� en la grilla. ");

		} else {
			log().info("El n�mero del pedido no se encuentra en la grilla. ");
			throw new Exception("El n�mero del pedido no se encuentra en la grilla.");
		}

	}

	public void cancelarDataProvider(String pedido) throws Exception {

		try {

			log().info("Se cancela el n�mero del pedido presente en el data provider. ");
			String xpathButton = reemplazarEnXpath(cancelarPedidoReserva, textoButton, pedido);
			driver.findElement(By.xpath(xpathButton)).click();
			log().info("Se presiona bot�n de cancelar pedido. ");
			Thread.sleep(5000);
			Assert.assertTrue(cartelConfirmarCancelar(),
					"Se valida que aparezca cartel de confirmaci�n de cancelaci�n. ");
			log().info("Se valida que aparezca el cartel de confirmaci�n. ");
			click(btnValidarCancelacion, "Se confirma la cancelaci�n del pedido.");
		} catch (Exception e) {
			throw new Exception("No se pudo cancelar el pedido. ", e);
		}
	}

	public void cancelarPedidoBoBtn() throws Exception {

		try {

			log().info("Se cancela el n�mero del pedido obtenido anteriormente. ");
			String xpathButton = reemplazarEnXpath(cancelarPedidoReserva, textoButton,
					ProductosDisponiblesPage.getNumeroPedidoBo());
			driver.findElement(By.xpath(xpathButton)).click();
			log().info("Se presiona bot�n de cancelar pedido. ");
			Thread.sleep(5000);
			Assert.assertTrue(cartelConfirmarCancelar(),
					"Se valida que aparezca cartel de confirmaci�n de cancelaci�n. ");
			log().info("Se valida que aparezca el cartel de confirmaci�n. ");
			click(btnValidarCancelacion, "Se confirma la cancelaci�n del pedido.");

		} catch (Exception e) {
			throw new Exception("No se pudo cancelar el pedido. ", e);
		}
	}
	
	public void cancelarPedidoBtn() throws Exception {

		try {

			log().info("Se cancela el n�mero del pedido obtenido anteriormente. ");
			String xpathButton = reemplazarEnXpath(cancelarPedidoReserva, textoButton,
					ProductosDisponiblesPage.getNumeroPedido());
			driver.findElement(By.xpath(xpathButton)).click();
			log().info("Se presiona bot�n de cancelar pedido. ");
			Thread.sleep(5000);
			Assert.assertTrue(cartelConfirmarCancelar(),
					"Se valida que aparezca cartel de confirmaci�n de cancelaci�n. ");
			log().info("Se valida que aparezca el cartel de confirmaci�n. ");
			click(btnValidarCancelacion, "Se confirma la cancelaci�n del pedido.");

		} catch (Exception e) {
			throw new Exception("No se pudo cancelar el pedido. ", e);
		}
	}

	public String agregarPedidoReservaAPI(String bodyRequest){
		sendRequest("POST", "Creacion de un pedido BO", "https://azapimngtdev.pan-energy.com/pdc-api/1.0/",
				"pedidos-backorder", "Ocp-Apim-Subscription-Key",
				"7f9fa0d81cdf4d029a3b7ebf9e27153e", bodyRequest);

		String orderNumber = "";

		// Se obtiene la respuesta
		JsonPath resp = getRequestResponse().jsonPath();
		int statusCode = getRequestResponse().getStatusCode();

		// Asersiones
		if (statusCode == 201) {
			orderNumber = resp.get("d.EOrderNumber");
			log().info("Se cre� el pedido BackOrder n�mero: " + orderNumber + " satisfactoriamente.");
		} else {
			log().info("No se cre� el pedido BackOrder el servicio retorn� c�digo: " + statusCode);
		}

		return orderNumber;
	}

	public void seleccionarPedidoDeReserva(String nroPedido){
		WebElement btnPedido = driver.findElement(By.xpath(reemplazarEnXpath(btnDeUnPedidoReserva,"nroDePedido", nroPedido )));
		click(btnPedido, "Se clickea en el boton del pedido: " + nroPedido);
	}

	public void eliminar1LineaDePedidoReserva(String nroPedido, String nroProducto){
			WebElement checkBox = driver.findElement(By.xpath(reemplazarEnXpath(checkBoxProducto, "nroDeProducto", nroProducto)));
			click(checkBox, "Se clickea en la checkbox del producto: " + nroProducto);

			WebElement btnCancelarProducto = driver.findElement(By.xpath("//button[@id='modal-btn-cancelar-no' and contains (., 'Cancelar producto')]"));
			click(btnCancelarProducto, "Se clickea en cancelar producto");

			WebElement btnAceptar = driver.findElement(By.xpath("//button[@id='modal-btn-cancelar-yes' and contains (., 'S�')]"));
			click(btnAceptar, "Se clickea en aceptar");

			waitForInVisibility("//div//div//p[contains (., 'Productos')]");
	}

	public boolean existenciaPedidoDeReserva(String orderNumber){
		try{
			WebElement btnPedido = driver.findElement(By.xpath(reemplazarEnXpath(btnDeUnPedidoReserva,"nroDePedido", orderNumber )));
			return true;
		}
		catch (NoSuchElementException e){
			return false;
		}

	}

	public boolean validarEliminarPedidoSinSeleccionarProducto(String nroPedido){

		WebElement btnPedido = driver.findElement(By.xpath(reemplazarEnXpath(btnDeUnPedidoReserva,"nroDePedido", nroPedido )));
		click(btnPedido, "Se clickea en el boton del pedido: " + nroPedido);

		WebElement btnCancelarProducto = driver.findElement(By.xpath("//button[@id='modal-btn-cancelar-no' and contains (., 'Cancelar producto')]"));
		click(btnCancelarProducto, "Se clickea en cancelar producto");

		return isVisibleinPage(By.xpath("//div[@class='modal-header']//h4[contains (.,'Advertencia')]"));
	}

	public boolean productoContinuaEnPedido(String nroProducto, String nroPedido){
		WebElement btnPedido = driver.findElement(By.xpath(reemplazarEnXpath(btnDeUnPedidoReserva,"nroDePedido", nroPedido )));
		click(btnPedido, "Se clickea en el boton del pedido: " + nroPedido);
		try {
			return isVisibleinPage(By.xpath(reemplazarEnXpath(checkBoxProducto, "nroDeProducto", nroProducto)));
		}
		catch (Exception e){
			return false;
		}
	}

	public void nuevoPedidoPesados() {
		click(btnNuevosPedidosPesados, "Se clickea el boton Nuevo Pedido de Combustibles Pesados");
	}

	public String validarTituloPesados(){
		if (textoNuevosPedidosPesados.isDisplayed()){
			return textoNuevosPedidosPesados.getText();
		}
		return "";
	}

	public String validarTituloLiquidos() {
		if (textoNuevosPedidosLiquidos.isDisplayed()){
			return textoNuevosPedidosLiquidos.getText();
		}
		return "";

	}

	public void nuevoPedidoLiquidos() {
		click(btnNuevosPedidosLiquidos, "Se clickea el boton Nuevo Pedido de Combustibles Liquidos");
	}
}
