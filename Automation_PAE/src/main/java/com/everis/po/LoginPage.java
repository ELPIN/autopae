package com.everis.po;

import com.everis.global.Global;
import com.everis.utilities.ExtentReportUtil;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class LoginPage extends Global {

	// Definir web elements a nivel de clase

	public LoginPage(WebDriver d) {
		super(d);
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//h1[@class='text-blue']")
	private WebElement loginMessage;

	@FindBy(id = "signInName")
	private WebElement userInputBox;

	@FindBy(id = "password")
	private WebElement passInputBox;




	///comentario agregado

	@FindBy(id = "next")
	private WebElement iniciarSessionBtn;

	@FindBy(id = "forgotPassword")
	private WebElement btnRecuperarPass;

	@FindBy(xpath = "//div[@class='error pageLevel' and contains(., 'The username or password provided in the request are invalid.')]")
	private WebElement cartelErrorDatoLogInvalido;

	@FindBy(xpath = "//h2[@class='text-blue']")
	private WebElement headerRecuperarPassword;

	@FindBy(id = "email")
	private WebElement campoEmailRecuperarPass;

	@FindBy(id = "email_ver_but_send")
	private WebElement btnEnviarCodigo;

	@FindBy(id = "email_info")
	private WebElement emailInfo;

	@FindBy(id = "cancel")
	private WebElement btnCancelar;

	@FindBy(id = "email_ver_input")
	private WebElement inputCodigoVerificacion;

	@FindBy(id = "email_ver_but_verify")
	private WebElement btnComprobarCodigo;

	@FindBy(xpath = "//div[@class='attrEntry']")
	private WebElement headerInfoRecuperarPass;

	@FindBy(xpath = "(//div[@class='error itemLevel'])[1]")
	private WebElement errorTxtCampoUsuario;
	
	@FindBy(xpath = "(//div[@class='error itemLevel'])[2]")
	private WebElement errorTxtCampoContrasena;

	@FindBy(xpath = "//h2[@class='text-blue']")
	private WebElement headerRecuperarContrasena;
	
	@FindBy(xpath = "//div[@id='email_info' and @style='display: inline;']")
	private WebElement infoEmailTxt;
	
	@FindBy(xpath = "//div[@class='error itemLevel show' and @role='alert']")
	private WebElement errorUserText;
	
	@FindBy(xpath = "//h1[@class='text-blue']")
	private WebElement headerLogin;
	
	@FindBy(xpath = "//div[@id='email_fail_retry' and @style='display: inline;']")
	private WebElement infoMailFailTxt;
	
	@FindBy(xpath = "//div[@id='email_fail_no_retry' and @style='display: inline;']")
	private WebElement infoMailFailNoRetryTxt;
	
	// Steps

	public Boolean headerIsVisible() {
		waitForVisibility(headerLogin);
		return headerLogin.isDisplayed();
	}
	
	public Boolean cartelErrorCampoPassword() {
		waitForVisibility(errorTxtCampoContrasena);
		return errorTxtCampoContrasena.isDisplayed();
	}
	
	public Boolean cartelErrorCampoUser() {
		waitForVisibility(errorTxtCampoUsuario);
		return errorTxtCampoUsuario.isDisplayed();
	}
	
	public Boolean infoEmailText() {
		waitForVisibility(infoEmailTxt);
		return infoEmailTxt.isDisplayed();
	}

	public Boolean errorUserFieldText() {
		waitForVisibility(errorUserText);
		return errorUserText.isDisplayed();
	}
	
	public Boolean infoEmailFailText() {
		waitForVisibility(infoMailFailTxt);
		return infoMailFailTxt.isDisplayed();
	}
	
	public Boolean infoEmailFailNoRetryText() {
		waitForVisibility(infoMailFailNoRetryTxt);
		return infoMailFailNoRetryTxt.isDisplayed();
	}

	public Boolean headerRecuperarPassword() {
		waitForVisibility(headerRecuperarContrasena);
		return headerRecuperarContrasena.isDisplayed();
	}

	public String getHeaderText() {
		return getText(loginMessage);
	}

	public String getHeaderInfoRecuperarContrasenaText() {
		return headerInfoRecuperarPass.getText();
	}

	public void setUsername(String user) throws Exception {
		sendKeys(userInputBox, user, "Se ingresa el usuario:" + user);
	}

	public void setPassword(String pass)  {
		sendKeys(passInputBox, pass, "Se ingresa contrase�a.");

	}

	public String getRecuperarPasswordText() {
		return getText(headerRecuperarPassword);
	}


	public String errorPageCartelDatoLogInvalido() {
		return getText(cartelErrorDatoLogInvalido);
}

	public String infoEmailRecuperarPass() {
		return emailInfo.getText();
	}

	public void clickSubmit() {
		click(iniciarSessionBtn, "Se presiona bot�n de Iniciar sesi�n.");
	}

	public void clickCancelar() {
		click(btnCancelar, "Se presiona bot�n Cancelar.");

	}

	public void clickOlvidoContrasena() {
		click(btnRecuperarPass,"Se presiona opci�n Recuperar Contrase�a.");
		
		// Aserciones
		Assert.assertTrue(headerRecuperarPassword(), "Se valida el ingreso a la secci�n de Recuperar Contrase�a.");
		Assert.assertEquals(getRecuperarPasswordText(), "Cambio de Contrase�a");
		log().info("Se valida el ingreso a la secci�n de Recuperar Contrase�a.");
	}

	public void setEmailRecuperarPass(String user) {

		sendKeys(campoEmailRecuperarPass, user, "Se ingresa el user: " + user);
		click(btnEnviarCodigo, "Se presiona bot�n de Enviar c�digo de verificaci�n.");

	}

	// Actions

	public void executeLogin(String user, String pass){
		IndexPage index = new IndexPage(driver);

		try{
			setUsername(user);
		}
		catch(Exception e){
			System.out.println("Fallido");
		}

        setPassword(pass);
        clickSubmit();

		// Aserciones
		Assert.assertTrue(index.headerIsVisible(), "Valido si el mensaje de bienvenida aparecio correctamente.");
		Assert.assertEquals(index.getHeaderText(), "�Bienvenid@!");
	}

	public void recuperarContrasenaUser(String user) {

		clickOlvidoContrasena();
		setEmailRecuperarPass(user);

		// Aserciones
		Assert.assertTrue(infoEmailText(), "Valido si aparece el header con mensaje de c�digo enviado.");
		log().info("Se valid� el env�o del c�digo de verificaci�n al user indicado. ");

	}

	public void setCodigoVerificacion(String codigo) {

		sendKeys(inputCodigoVerificacion, codigo,"Se ingresa c�digo de verificaci�n.");
		click(btnComprobarCodigo, "Se presiona bot�n de comprobar c�digo.");
	}

}
