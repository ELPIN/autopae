package com.everis.po;

import com.everis.global.Global;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class MiCuentaPage extends Global {

	// Definir web elements a nivel de clase

	public MiCuentaPage(WebDriver d) {
		super(d);
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//input[@placeholder=\"Ingrese una patente, por ejemplo 'AR123CC'\"]")
	private WebElement inputPatente;

	@FindBy(xpath = "//div[@class='btn-group']")
	private WebElement btnAsociarPatente;

	@FindBy(xpath = "//div[@class='invalid-feedback']")
	private WebElement cartelErrorPatente;

	@FindBy(xpath = "//div//h4[contains (., 'Subusuarios')]")
	private WebElement headerUsuarios;

	@FindBy(xpath = "//tr//th[contains(., 'Correo electr�nico')]")
	private WebElement grillaAgregarUsuarios;

	@FindBy(xpath = "//i-feather[@name='user-plus']")
	private WebElement btnAgregarUsuarios;

	@FindBy(xpath = "//input[@type='email' and @placeholder='Ejemplo: micorreo@ejemplo.com']")
	private WebElement campoAgregarUsuarios;

	@FindBy(xpath = "//div//a[@class='btn btn-outline-dark border-light d-inline-flex align-items-center btn-default bg-primary text-white']")
	private WebElement btnDarAlta;

	@FindBy(xpath = "//div[@class='invalid-feedback']")
	private WebElement errorCampoAgregarUsuarios;

	@FindBy(xpath = "//div[@class='modal-content' and contains(.,'Informaci�n')]")
	private WebElement popupAltaSubUsuario;

	@FindBy(xpath = "//div[@class='modal-body']")
	private WebElement popupBajaSubUsuario;

	@FindBy(xpath = "//button[contains(.,'Aceptar')]")
	private WebElement btnAceptar;

	@FindBy(xpath = "//button[contains(.,'S�, deseo eliminarlo')]")
	private WebElement btnSiDeseoEliminarSubUsuario;

	@FindBy(xpath = "//a[@class='btn rounded btn-outline-dark border-light cross dropdown-toggle tooltip-open tp-transparent']")
	private WebElement eliminar;

	@FindBy(xpath = "//div//div//a//h5[contains(.,'Gestionar sub-usuarios')]")
	private WebElement seccionGestionarSubUsuarios;

	@FindBy(xpath = "//div//div//a//h5[contains(.,'Mis datos personales')]")
	private WebElement seccionMisDatosPersonales;

	@FindBy(xpath = "//div//div//a//h5[contains(.,'Patentes')]")
	private WebElement seccionPatentes;



	private String grillaSubUsuarioEliminar = "//tbody//tr[td[contains(.,'textoButton')]]//a[@class='btn rounded btn-outline-dark border-light cross dropdown-toggle tooltip-open tp-transparent']";
	private String grillaSubUsuarioModificar = "//tbody//tr[td[contains(.,'textoButton')]]//a[@class='btn rounded btn-outline-dark border-light pen dropdown-toggle tooltip-open tp-transparent']";
	private String subUsuarioIsPresent = "//tbody//tr[td[contains(.,'textoButton')]]";
	private String textoButton = "textoButton";
	private String popupSubUsuarioExistente = "//div[@class='modal-body' and contains (.,'El usuario no pudo ser dado de alta (El usuario ya existe)')]";
	private String modificarShipTo1 = "//tbody//tr[td/span[contains(.,'textoButton')]]//td/input[@class='form-check-input disableDefaultClick']";
	private String modificarShipTo2 = "//tbody//tr[td/span[contains(.,'textoButton')]]//td/input[@class='form-check-input']";
	private String habilitado1 = "//tbody//tr[td/span[contains(.,'textoButton')]]//td/input[@id='shiptoSeleccionado']";
	private static String numeroShipto;

	// Steps

	public static String getNumeroShipto() {
		return numeroShipto;
	}

	private static void setNumeroShipto(String numeroShipto) {
		MiCuentaPage.numeroShipto = numeroShipto;
	}

	public String popupAltaSubUsuario() {
		return getText(popupAltaSubUsuario);
	}

	public String errorAgregarUsuarios() {
		return getText(errorCampoAgregarUsuarios);
	}

	public String poupBajaSubUsuario() {
		return getText(popupBajaSubUsuario);
	}

	public void irSeccionMisDatosPersonales(){
		click(seccionMisDatosPersonales, "Se clickea sobre la seccion mis datos Personales");
	}

	public void irSeccionGestionarSubUsuarios(){
		click(seccionGestionarSubUsuarios, "Se clickea sobre la seccion gestionar sub-usuarios");
	}

	public void irSeccionPatentes(){
		click(seccionPatentes, "Se clickea sobre la seccion de patentes");
	}


	public Boolean validarGrillaSubUsuarios() {
		waitForVisibility(grillaAgregarUsuarios);
		return grillaAgregarUsuarios.isDisplayed();
	}

	public boolean buttonAsociarPatente() {
		try {
			waitForVisibility(seccionPatentes);
			return (seccionPatentes.isDisplayed());
		} catch (Exception e) {
			return false;
		}

	}

	public String errorAsociarPatente() {
		return getText(cartelErrorPatente);
	}

	public void clickBtnAgregarUsuarios() throws InterruptedException {
		Thread.sleep(5000);
		click(btnAgregarUsuarios, "Se presiona bot�n de Agregar Usuarios.");

	}

	public void clickBtnDarAlta() {
		click(btnDarAlta, "Se presiona bot�n de Dar de Alta.");

	}

	public void sendMailAgregarUsuarios(String user) {
		sendKeys(campoAgregarUsuarios, user, "Se ingresa usuario: " + user);

	}

	// Actions

	public void editarUsuario(String subUsuario) throws Exception {

		try {
			// Se ubica el subUsuario en la grilla y se modifica
			String xpathSubUsuario = reemplazarEnXpath(grillaSubUsuarioModificar, textoButton, subUsuario);
			driver.findElement(By.xpath(xpathSubUsuario)).click();
			log().info("Se ubica el subUsuario: " + subUsuario + " y se modifica.");
			Thread.sleep(3000);

		} catch (Exception e) {

			log().info("Ocurri� un error al ubicar usuario a modificar: " + subUsuario);
			throw new Exception("Ocurri� un error al ubicar usuario a modificar.", e);
		}

	}

	public void asociarPatente(String patente) {
		sendKeys(inputPatente, patente, "Se ingresa patente: " + patente);
		click(btnAsociarPatente, "Se presiona bot�n de asociar patente.");
	}

	public void validarSubUsuario(String user) throws Exception {

		String xpathSubUsuario = reemplazarEnXpath(subUsuarioIsPresent, textoButton, user);

		boolean a = driver.findElements(By.xpath(xpathSubUsuario)).size() > 0;

		if (a) {
			log().info("El usuario que se intent� dar de alta: " + user + ", ya existe, se procede a eliminarlo.");
			eliminarSubUsuario(user);

		} else {
			log().info("El usuario :" + user + " no existe, se procede al alta del mismo.");

		}
	}

	public void altaSubUsuario(String user) throws Exception {
		clickBtnAgregarUsuarios();
		sendMailAgregarUsuarios(user);
		clickBtnDarAlta();

		boolean a = driver.findElements(By.xpath(popupSubUsuarioExistente)).size() > 0;

		if (a) {
			log().info("El usuario que se intent� dar de alta: " + user + ", ya existe.");
			throw new Exception("Ocuri� un error al dar de alta al usuario. Usuario ya existente.");
		} else {

			// Aserciones
			String cartelAltaSubUsuario = popupAltaSubUsuario();
			Assert.assertTrue(cartelAltaSubUsuario.contains("fue dado de alta"));
			log().info("Se valida cartel de alta de SubUsuario: " + user);

			click(btnAceptar, "Se presiona bot�n de Aceptar.");
		}
	}

	public void eliminarSubUsuario(String subUsuario) throws Exception {

		try {
			Thread.sleep(5000);

			String xpathButton = reemplazarEnXpath(grillaSubUsuarioEliminar, textoButton, subUsuario);
			driver.findElement(By.xpath(xpathButton)).click();
			click(btnSiDeseoEliminarSubUsuario, "Se  clickea si, deseo eliminarlo");
			log().info("Se ubica el subUsuario: " + subUsuario + " y se elimina.");

			// Aserciones
			Assert.assertEquals(poupBajaSubUsuario(), "El usuario fue eliminado satisfactoriamente");
			log().info("Se valida cartel de baja de usuario satisfactoriamente.");
			click(btnAceptar, "Se presiona bot�n de aceptar.");
		} catch (Exception e) {
			log().info("Ocuri� un error al eliminar el SubUsuario. ");
			throw new Exception("Ocuri� un error al eliminar el SubUsuario.");
		}
	}

	public void modificarShipToPorDefecto(String subUsuario, String shiptoUno, String shiptoDos) throws Exception {

		editarUsuario(subUsuario);
		log().info("Se valida el shipTo configurado por defecto y se modifica.");

		// Se validan shiptos habilitados y se modifican
		String xpathValidarShiptoHabilitado = reemplazarEnXpath(modificarShipTo1, textoButton, shiptoUno);
		String xpathShipto1 = reemplazarEnXpath(modificarShipTo2, textoButton, shiptoDos);
		String xpathShipto2 = reemplazarEnXpath(modificarShipTo2, textoButton, shiptoUno);

		try {
			boolean a = driver.findElements(By.xpath(xpathValidarShiptoHabilitado)).size() > 0;

			if (a) {
				driver.findElement(By.xpath(xpathShipto1)).click();
				log().info("Se selecciona shipTo por defecto: " + shiptoDos);
				Thread.sleep(4000);
				Assert.assertEquals(driver.findElement(By.xpath(xpathShipto1)).getAttribute("disabled"), "true");
				log().info("Se valida que el shipTo est� habilitado.");
				setNumeroShipto(shiptoDos);

			} else {
				driver.findElement(By.xpath(xpathShipto2)).click();
				log().info("Se selecciona shipTo por defecto: " + shiptoUno);
				Thread.sleep(4000);
				Assert.assertEquals(driver.findElement(By.xpath(xpathShipto2)).getAttribute("disabled"), "true");
				log().info("Se valida que el shipTo est� habilitado.");
				setNumeroShipto(shiptoUno);
			}
		} catch (Exception e) {

			log().info("Ocurrio un error en la modificaci�n de shiptos.");
			throw new Exception("Ocurrio un error en la modificaci�n de shiptos.", e);
		}

	}

	public boolean isDisabledShipto(String shipto) {

		String xpathShipto1 = reemplazarEnXpath(modificarShipTo2, textoButton, shipto);

		if (driver.findElement(By.xpath(xpathShipto1)).getAttribute("disabled") != null) {
			return true;
		} else {
			return false;
		}

	}

	public void habilitarShipto(String subUsuario, String shipto1, String shipto2, String shipto3) throws Exception {

		editarUsuario(subUsuario);
		log().info("Se validan los shipTo habilitados y configurados por defecto y se modifican.");

		// Se validan los dos primeros shipto habilitados y por defecto y se modifican
		String xpathShiptoHabilitado1 = reemplazarEnXpath(habilitado1, textoButton, shipto1);
		String xpathShiptoHabilitado2 = reemplazarEnXpath(habilitado1, textoButton, shipto2);
		String xpathShiptoDefecto2 = reemplazarEnXpath(modificarShipTo2, textoButton, shipto3);

		try {
			if (isDisabledShipto(shipto2) == true
					&& !driver.findElement(By.xpath(xpathShiptoHabilitado1)).isSelected()) {

				// Se selecciona shipTo por defecto distinto a los primeros dos.
				driver.findElement(By.xpath(xpathShiptoDefecto2)).click();
				Thread.sleep(3000);
				// Se habilitan y deshabilitan los dos primeros shipTo.
				driver.findElement(By.xpath(xpathShiptoHabilitado1)).click();
				driver.findElement(By.xpath(xpathShiptoHabilitado2)).click();
				Thread.sleep(3000);
				setNumeroShipto(shipto1);
				log().info("Se habilita el ShipTo: " + getNumeroShipto());

			} else if (isDisabledShipto(shipto2) == true
					&& driver.findElement(By.xpath(xpathShiptoHabilitado1)).isSelected()) {

				// Se selecciona shipTo por defecto distinto a los primeros dos.
				driver.findElement(By.xpath(xpathShiptoDefecto2)).click();
				Thread.sleep(3000);
				// Se habilitan y deshabilitan los dos primeros shipTo.
				driver.findElement(By.xpath(xpathShiptoHabilitado2)).click();
				Thread.sleep(3000);
				setNumeroShipto(shipto1);
				log().info("Se habilita el ShipTo: " + getNumeroShipto());

			} else if (isDisabledShipto(shipto1) == true
					&& !driver.findElement(By.xpath(xpathShiptoHabilitado2)).isSelected()) {

				// Se selecciona shipTo por defecto distinto a los primeros dos.
				driver.findElement(By.xpath(xpathShiptoDefecto2)).click();
				Thread.sleep(3000);
				// Se habilitan y deshabilitan los dos primeros shipTo.
				driver.findElement(By.xpath(xpathShiptoHabilitado1)).click();
				driver.findElement(By.xpath(xpathShiptoHabilitado2)).click();
				Thread.sleep(3000);
				setNumeroShipto(shipto2);
				log().info("Se habilita el ShipTo: " + getNumeroShipto());

			} else if (isDisabledShipto(shipto1) == true
					&& driver.findElement(By.xpath(xpathShiptoHabilitado2)).isSelected()) {

				// Se selecciona shipTo por defecto distinto a los primeros dos.
				driver.findElement(By.xpath(xpathShiptoDefecto2)).click();
				Thread.sleep(3000);
				// Se habilitan y deshabilitan los dos primeros shipTo.
				driver.findElement(By.xpath(xpathShiptoHabilitado1)).click();
				Thread.sleep(3000);
				setNumeroShipto(shipto2);
				log().info("Se habilita el ShipTo: " + getNumeroShipto());

			} else if (!driver.findElement(By.xpath(xpathShiptoHabilitado1)).isSelected()
					&& !driver.findElement(By.xpath(xpathShiptoHabilitado2)).isSelected()) {

				driver.findElement(By.xpath(xpathShiptoHabilitado1)).click();
				Thread.sleep(3000);
				setNumeroShipto(shipto1);
				log().info("Se habilita el ShipTo: " + getNumeroShipto());

			} else if (driver.findElement(By.xpath(xpathShiptoHabilitado1)).isSelected()
					&& driver.findElement(By.xpath(xpathShiptoHabilitado2)).isSelected()) {

				driver.findElement(By.xpath(xpathShiptoHabilitado1)).click();
				Thread.sleep(3000);
				setNumeroShipto(shipto2);
				log().info("Se habilita el ShipTo: " + getNumeroShipto());

			} else if (!driver.findElement(By.xpath(xpathShiptoHabilitado1)).isSelected()
					&& driver.findElement(By.xpath(xpathShiptoHabilitado2)).isSelected()) {

				driver.findElement(By.xpath(xpathShiptoHabilitado1)).click();
				driver.findElement(By.xpath(xpathShiptoHabilitado2)).click();
				Thread.sleep(3000);
				setNumeroShipto(shipto1);
				log().info("Se habilita el ShipTo: " + getNumeroShipto());

			} else if (driver.findElement(By.xpath(xpathShiptoHabilitado1)).isSelected()
					&& !driver.findElement(By.xpath(xpathShiptoHabilitado2)).isSelected()) {

				driver.findElement(By.xpath(xpathShiptoHabilitado1)).click();
				driver.findElement(By.xpath(xpathShiptoHabilitado2)).click();
				Thread.sleep(3000);
				setNumeroShipto(shipto2);
				log().info("Se habilita el ShipTo: " + getNumeroShipto());
			}
		} catch (Exception e) {

			log().info("Ocurrio un error en la modificaci�n de ShipTo del Sub Usuario: " + subUsuario);
			throw new Exception("Ocurrio un error en la modificaci�n de shiptos.", e);
		}
	}

	public void eliminarSubUsuarioApi(String sUsername, String subUsuario) {

		String body = "{\"login\": {\"email\": \"" + sUsername + "\"},\"userMailToInactivate\": \"" + subUsuario
				+ "\"}";

		sendRequest("DELETE", "Eliminar SubUsuario", "https://azapimngtdev.pan-energy.com/crm_test/1.0/api/",
				"SubUsuarios", "Ocp-Apim-Subscription-Key", "7f9fa0d81cdf4d029a3b7ebf9e27153e", body);

		// Se obtiene la respuesta
		int statusCode = getRequestResponse().getStatusCode();

		// Asersiones
		if (statusCode == 200) {
			log().info("Se elimin� el sub usuario: " + subUsuario + " satisfactoriamente.");
		} else {
			log().info("No se elimin� el sub usuario: " + subUsuario + " el servicio retorn� c�digo: " + statusCode);
		}

	}

}
