package com.everis.po;

import com.everis.global.Global;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AprobacionesPage extends Global {

	// Definir web elements a nivel de clase

	public AprobacionesPage(WebDriver d) {
		super(d);
		PageFactory.initElements(driver, this);
	}


	@FindBy(xpath = "//div//h4[@class='modal-title']")
	private WebElement headerVentanaNotificacion;

	@FindBy(xpath = "//div[@class='modal-body']//p")
	private WebElement popupTyCretail;

	@FindBy(xpath = "//body//embed[@type='application/pdf']")
	private WebElement notificacionesPdf;

	@FindBy(xpath = "//html[@dir='ltr']")
	private WebElement notificacionesPdfFirefox;

	@FindBy(xpath = "//div//embed[@frameborder='0']")
	private WebElement frame;

	@FindBy(xpath = "//button[@id='download']")
	private WebElement descargar;
	
	@FindBy(xpath = "//div[@class='table-responsive overflow-visible abs-center']//div[@class='abs-center']")
	private WebElement noAprobacionesDiv;

	@FindBy(xpath = "//button[contains (.,'Historial')]")
	private WebElement btnHistorial;


	private String grillaAprobaciones = "//tbody[tr[notificacionesPendientes] and not (tr[notificacionesPendientesMasUno])]";
	private String notificacionShipTo = "//tbody//tr[td[contains(.,'textoButton')]]";
	private String textoButton = "textoButton";
	private String historialTituloNotificacion = "//tbody//tr[td[contains(.,'textoTitulo')]]";
	private String textoTitulo = "textoTitulo";
	private String notificacionesPendientes = "notificacionesPendientes";
	private String notificacionesPendientesMasUno = "notificacionesPendientesMasUno";
	private String popUpAdvertencia = "//div[@class='modal-header']//h4[contains(.,'Advertencia')]";
	private String tituloHistorialTexto = "Historial";

	// Steps

	public String headerVentanaNotificacion() {
		return getText(headerVentanaNotificacion);
	}

	public String getPopupTyCretailTxt() {
		return getText(popupTyCretail);
	}
	
	public String getGrillaAprobacionesPendientes() {
		return getText(noAprobacionesDiv);
	}


	public Boolean notificacionesPdf() {
		// try {
		switchTo(frame, "Se cambia de frame.");
		if (getDriverInUse().equalsIgnoreCase("chromedriver")) {
			log().info("Se identifica navegador 'Chrome' ");
			notificacionesPdf.isDisplayed();
			return true;

		} else if (getDriverInUse().equalsIgnoreCase("geckodriver")) {
			log().info("Se identifica navegador 'Firefox' ");
			notificacionesPdfFirefox.isDisplayed();
			return true;
		} else {

			log().info("No se valida .pdf de la notificaci�n seleccionada. ");
			return false;
		}

	}

	public void clickDownload() throws InterruptedException {

		if (getDriverInUse().equalsIgnoreCase("geckodriver")) {

			click(descargar, "Se presiona bot�n de descargar notficiaci�n.");
		} else {
			log().info("Se identifica navegador 'Chrome', no se descarga la notificaci�n. ");
		}

	}

	// Actions

	public void clickNotificacionPorShipto(String shipTo) throws InterruptedException {

		String xpathButton = reemplazarEnXpath(notificacionShipTo, textoButton, shipTo);
		driver.findElement(By.xpath(xpathButton)).click();
		log().info("Se presiona notificiaci�n correspondiente al ShipTo: " + shipTo);
		Thread.sleep(3000);

	}

	public boolean validarGrillaAprobaciones() {

		// Se arma xpath con el n�mero de notificaciones obtenido
		String xpathNotificacionesPendientes = reemplazarEnXpath(grillaAprobaciones, notificacionesPendientes,
				IndexPage.getNotificacionesTxt());

		// Se convierte a int para sumar y luego vuelve a string
		int numeroNotificaciones = Integer.parseInt(IndexPage.getNotificacionesTxt());
		int suma = numeroNotificaciones + 1;
		String sumaNotificaciones = String.valueOf(suma);

		// Se completa xpath de validaci�n del n�mero de notificaciones obtenido m�s uno
		String xpathNotifciacionesSumaFinal = reemplazarEnXpath(xpathNotificacionesPendientes,
				notificacionesPendientesMasUno, sumaNotificaciones);

		log().info("Se validan las notificaciones pendientes en la grilla. ");
		boolean a = driver.findElements(By.xpath(xpathNotifciacionesSumaFinal)).size() > 0;

		if (a == true) {
			return true;
		} else {
			log().info("No se validaron correctamente la Notificaciones en la grilla de Aprobaciones. ");
			return false;
		}

	}

	public void clickHistorial(){
		click(btnHistorial, "Se clickea el boton historial");
	}

	public Boolean validarTituloHistorial(){
		return driver.findElement(By.xpath("//h4[contains (.,'Historial')]")).getText().equals(tituloHistorialTexto);
	}

	public void descargarNotificacionEnHistorialPorTitulo(String titulo) throws InterruptedException {
		String xpathButton = reemplazarEnXpath(historialTituloNotificacion, textoTitulo, titulo);
		driver.findElement(By.xpath(xpathButton)).findElement(By.xpath("./..//td[@type='button']")).click();
		log().info("Se presiona notificiaci�n correspondiente al ShipTo: " + textoTitulo);
		Thread.sleep(3000);
	}

}