package com.everis.po;

import com.everis.global.Global;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.io.File;
import java.util.List;

public class ProductosDisponiblesPage extends Global {

	// Definir web elements a nivel de clase

	public ProductosDisponiblesPage(WebDriver d) {
		super(d);
		PageFactory.initElements(driver, this);
	}

	String rutaAbsoluta = new File("").getAbsolutePath();

	public String cartelProductosDisponibles() {
		return getText(cartelProductosDisponibles);
	}

	public String cartelErrorCantidad() {
		return getText(cartelErroCantidad);
	}

	public String cartelErrorCantidadInsuficiente() {
		return getText(cartelErrorStockInsuficiente);
	}

	public String cartelLlamadoCentroAtencion() {
		return getText(cartelLlamadoCA);
	}

	public String cartelGenerarPedidoReserva() {
		return getText(cartelPedidoReserva);
	}

	public String btnAgregar() {
		return getText(btnAgregar);
	}

	public String getRutaArchivosEntrada() {
		return rutaArchivosEntrada;
	}

	public String numeroPedidoGenerado() {
		return getText(cartelNumeroPedidoGenerado);
	}

	public String mensajeErrorPedido() {
		return getText(cartelErrorPedido);
	}

	public String numeroPedidoBackOrderGenerado() {
		return getText(cartelNumeroPedidoBackOrderGenerado);
	}

	public String numeroPedidoBackOrderGeneradoSimple() {
		return getText(numeroPedidoBackOrderGeneradoSimple);
	}

	public void setRutaArchivosEntrada(String rutaArchivosEntrada) {
		this.rutaArchivosEntrada = rutaArchivosEntrada;
	}

	public static String getNumeroPedido() {
		return numeroPedido;
	}

	private static void setNumeroPedido(String numeroPedido) {
		ProductosDisponiblesPage.numeroPedido = numeroPedido;
	}

	public static String getNumeroPedidoBo() {
		return numeroPedidoBo;
	}

	private static void setNumeroPedidoBo(String numeroPedidoBo) {
		ProductosDisponiblesPage.numeroPedidoBo = numeroPedidoBo;
	}

	@FindBy(xpath = "//div//h4[@class='headline text-muted' and contains(., 'Productos disponibles')]")
	private WebElement cartelProductosDisponibles;

	@FindBy(xpath = "//input[@placeholder='Buscar producto...']")
	private WebElement buscadorProductos;

	@FindBy(xpath = "//div//button[@class='btn btn-outline-dark border-light d-inline-flex dropdown-toggle align-items-center']")
	private WebElement bntFiltros;

	@FindBy(xpath = "//div[@class='categorias-menu dropdown-menu dropdown-menu-right animated bounceInDown show']")
	private WebElement filtrosProductos;

	@FindBy(xpath = "//div//a[@class='btn btn-primary ml-4' and contains (.,'Aplicar Filtros')]")
	private WebElement btnAplicarFiltro;

	@FindBy(xpath = "//div//a[@class='link underline ml-auto text-nowrap' and contains(., 'Limpiar filtros')]")
	private WebElement btnLimpiarFiltros;

	@FindBy(xpath = "//div[@class='jerarquias']")
	private WebElement filtroAplicado;

	@FindBy(xpath = "//button[@class='btn btn-outline-dark border-light d-inline-flex align-items-center' and contains(., 'Cargar archivo de pedido ')]")
	private WebElement btnCargarArchivoPedido;

	@FindBy(xpath = "//input[@type='file']")
	private WebElement cargaProductosCSV;

	@FindBy(xpath = "//div//div[@class='modal-content']")
	private WebElement cartelLlamadoCA;

	@FindBy(xpath = "//div[@class='modal-body']")
	private WebElement cartelErroCantidad;

	@FindBy(xpath = "//div//h4[@class='modal-title']")
	private WebElement cartelErrorStockInsuficiente;

	@FindBy(xpath = "//div[@class='modal-content' and contains (., 'La cantidad ingresada no est� disponible �quiere generar un Pedido de Reserva?')]")
	private WebElement cartelPedidoBackOrder;

	@FindBy(xpath = "//button[@class='btn btn-accion' and contains (.,'Aceptar')]")
	private WebElement aceptarCartelErrorCantidad;

	@FindBy(name = "cantidadSolicitada")
	private WebElement cantidadSolicitada;

	@FindBy(xpath = "//div[@class='modal-content']")
	private WebElement cartelPedidoReserva;

	@FindBy(xpath = "//div//button[@class='btn btn-lg btn-success']")
	private WebElement btnReservarCantidad;

	@FindBy(xpath = "//div//button[@class='btn btn-lg btn-danger']")
	private WebElement btnQuitarDelPedido;

	@FindBy(xpath = "//td//button[@type='button' and  @class='btn btn-primary btn-accion rounded m-0']")
	private WebElement btnAgregar;

	@FindBy(name = "arrow-left")
	private WebElement btnProductosDisponibles;

	@FindBy(xpath = "//button[@class='btn btn-primary border-radius w-100 p-2 mb-3 mb-xl-0' and contains (.,'Confirmar Pedido')]")
	private WebElement btnConfirmarPedido;

	@FindBy(xpath = "//button[@class='btn btn-lg btn-success' and contains (.,'S�, confirmar el Pedido')]")
	private WebElement btnValidarConfirmarPedido;

	@FindBy(xpath = "//p//span[@class='font-weight-bold']")
	private WebElement cartelNumeroPedidoGenerado;

	@FindBy(xpath = "//button[@type='button']")
	private WebElement btnVolverGeneracionPedido;

	@FindBy(xpath = "//div[@class='modal-body mensaje-resultado']")
	private WebElement cartelErrorPedido;

	@FindBy(xpath = "//p[contains(., 'Tambi�n se gener� un Pedido de reserva de producto con el n�mero')]//span[@class='font-weight-bold']")
	private WebElement cartelNumeroPedidoBackOrderGenerado;

	@FindBy(xpath = "//p[contains(., 'e gener� un Pedido de reserva ')]//span[@class='font-weight-bold']")
	private WebElement numeroPedidoBackOrderGeneradoSimple;

	@FindBy(xpath = "//p[contains (.,'Puede realizar el seguimiento de sus Pedidos ingresando al Portal, secci�n Pedidos')]")
	private WebElement cartelPedidoGenerado;

	@FindBy(xpath = "//p[contains (.,'e gener� un Pedido de reserva ')]")
	private WebElement cartelPedidoGeneradoBO;

	@FindBy(xpath = "//p[contains (.,'Tambi�n se gener� un Pedido de reserva de producto con el n�mero')]")
	private WebElement cartelPedidoGeneradoMixtoBO;

	@FindBy(xpath = "//tbody//tr[@class='flex-row flex-wrap d-flex font-weight-medium abs-center']")
	private WebElement grillaProductosDisponibles;

	private static String numeroPedido;
	private static String numeroPedidoBo;
	private String rutaArchivosEntrada = "./src/test/archivosDeEntrada/";
	private String codigoProducto = "//td//span[@class='spn-descripcion' and contains(., 'textoButton')]";
	private String categoriaProducto = "//li//span[@id='filtros-span-descripcionJerarquia' and contains (.,'textoButton')]";
	private String agregarProducto = "//tbody//tr[td/span[contains(.,'textoButton')]]//td/button";
	private String agregarCantidad = "//tbody//tr[td/span[contains(.,'textoButton')]]//td[3]/input";
	private String quitarProducto = "//tbody//tr[td/span[contains(.,'textoButton')]]//td/input[@value='Quitar']";
	private String aplicadoFiltro = "//div[@class='jerarquias' and contains(., 'textoButton')]";
	private String textoButton = "textoButton";

	// mensajes del log
	private String clienteCifMensaje = "Se identifica cliente CIF";
	private String clienteBofMensaje = "Se identifica cliente FOB";
	private String cantidadMenorMensaje = "Se agrega una cantidad menor del producto a la disponible.";
	private String cantidadMayorMensaje = "Se agrega una cantidad mayor del producto a la disponible.";
	private String mensajePedidoBackOrder = "La cantidad ingresada no est� disponible �quiere generar un Pedido de Reserva?";

	public Boolean cartelPedidoGenerado() {
		waitForVisibility(cartelPedidoGenerado);
		return cartelPedidoGenerado.isDisplayed();
	}

	public Boolean cartelPedidoBackOrderGeneradoSimple() {
		waitForVisibility(cartelPedidoGeneradoBO);
		return cartelPedidoGeneradoBO.isDisplayed();
	}

	public Boolean cartelPedidoBackOrderGenerado() {
		waitForVisibility(cartelPedidoGeneradoMixtoBO);
		return cartelPedidoGeneradoMixtoBO.isDisplayed();
	}

	public Boolean cartelCodigoErrorPedido() {
		return super.isVisibleinPage(By.xpath(
				"//div[@class='modal-content' and contains (., 'Ocurri� un error al generar el Pedido solicitado | cod: (')]"));
	}

	private boolean cartelAtencionPedidoObsoleto() {
		try {
			cartelLlamadoCA.isDisplayed();
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	private boolean cartelErrorPedidoSolicitado() {
		try {
			Assert.assertTrue(cartelCodigoErrorPedido(), "Se valida si aparece cartel de error. ");
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	private boolean cartelPedidoBackOrder() {
		try {
			cartelPedidoBackOrder.isDisplayed();
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	public boolean validarPedidoBusqueda() {
		try {
			cartelPedidoGenerado();
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	// Steps

	public void validarProductoPorIdentificador(String identificador) throws Exception {

		try {
			String xpathButton = reemplazarEnXpath(codigoProducto, textoButton, identificador);
			Thread.sleep(5000);
			Assert.assertTrue(xpathButton.contains(identificador));
			log().info("Se valida la b�squeda del producto: " + identificador);
		} catch (Exception e) {
			log().info("No se realiz� la b�squeda del producto: " + identificador);
			throw new Exception("No se realiz� la b�squeda del producto: " + identificador, e);
		}
	}

	public void validarFiltroAplicado(String categoria) throws Exception {

		try {
			String validarFiltro = getText(filtroAplicado);
			Assert.assertTrue(validarFiltro.contains(categoria));
			log().info("Se valida el filtro aplicado.");
		} catch (Exception e) {
			log().info("Hubo un error al validar el filtro aplicado.");
			throw new Exception("Hubo un error al validar el filtro aplicado.", e);
		}
	}

	public void clickBtnVolver() {

		click(btnVolverGeneracionPedido, "Se presiona bot�n volver.");
	}

	public void seleccionarfiltroAplicado(String categoria) {

		String xpathButton = reemplazarEnXpath(aplicadoFiltro, textoButton, categoria);
		driver.findElement(By.xpath(xpathButton)).click();

	}

	public boolean validarFiltro(String categoria) {

		String xpathButton = reemplazarEnXpath(aplicadoFiltro, textoButton, categoria);

		boolean a = driver.findElements(By.xpath(xpathButton)).size() > 0;

		if (a == true) {
			log().info(a);
			return true;
		} else {
			log().info(a);
			return false;
		}

	}

	public void agregarProducto(String producto) throws Exception {

		try {
			String xpathButton = reemplazarEnXpath(agregarProducto, textoButton, producto);
			driver.findElement(By.xpath(xpathButton)).click();
			log().info("Se presiona bot�n de agregar producto. ");
		} catch (Exception e) {
			throw new Exception("Ocurri� un error al presionar el bot�n de Agregar. ", e);
		}

	}

	public void quitarProducto(String producto) {

		String xpathButton = reemplazarEnXpath(quitarProducto, textoButton, producto);
		driver.findElement(By.xpath(xpathButton)).click();

	}

	public void agregarCantidad(String cantidad, String codigo) throws Exception {

		try {
			String xpathButton = reemplazarEnXpath(agregarCantidad, textoButton, codigo);
			driver.findElement(By.xpath(xpathButton)).clear();
			driver.findElement(By.xpath(xpathButton)).sendKeys(cantidad);
			log().info("Se agrega cantidad: " + cantidad + " al producto: " + codigo);
		} catch (Exception e) {
			log().info("Ocurri� un error al agregar cantidad : " + cantidad + " al producto: " + codigo);
			throw new Exception("No se agreg� la cantidad al producto. ", e);
		}
	}

	public void btnReservarCantidad() {

		click(btnReservarCantidad, "Se presiona bot�n de reservar cantidad.");
	}

	public void btnQuitarDelPedido() {

		click(btnQuitarDelPedido, "Se quit� la cantidad del pedido.");

	}

	public void clickbtnProductosDisponiblesBack() {

		click(btnProductosDisponibles, "Se cierra la ventana del Carrito.");

	}

	// Actions

	public void buscadorProductos(String identificador) {

		clear(buscadorProductos);
		sendKeys(buscadorProductos, identificador, "Se busca el producto: " + identificador);

	}

	public void limpiarFiltros() {

		click(bntFiltros, "Se presiona bot�n de filtros.");
		click(btnLimpiarFiltros, "Se presiona bot�n de limpiar filtros.");

	}

	public void subirArchivo(String archivo) throws Exception {
		try {

			List<WebElement> inputs = driver.findElements(By.xpath("//input[@type='file']"));

			for (WebElement input : inputs) {
				((JavascriptExecutor) driver).executeScript("arguments[0].removeAttribute('hidden','hidden')", input);
			}

			setRutaArchivosEntrada(rutaAbsoluta + getRutaArchivosEntrada());
			log().info("Se procede a la carga masiva.");
			Thread.sleep(5000);
			sendKeys(cargaProductosCSV, getRutaArchivosEntrada() + archivo, "Se carga el archivo con productos.");

		} catch (Exception e) {
			log().info("No se pudo completar la carga masiva.");
			throw new Exception(e);
		}
	}

	public void filtrarProductos(String categoria) throws Exception {

		String xpathButton = reemplazarEnXpath(categoriaProducto, textoButton, categoria);

		waitForVisibility(grillaProductosDisponibles);
		click(bntFiltros, "Se presiona el bot�n de filtro.");
		driver.findElement(By.xpath(xpathButton)).click();
		log().info("Se selecciona el filtro.");
		click(btnAplicarFiltro, "Se aplica el filtro");
		validarFiltroAplicado(categoria);
	}

	public void validarStockConContrato() {

		if (cartelPedidoBackOrder() == false) {

			log().info("Se valida que el producto posee stock. ");

		} else {
			log().info("El producto agregado no posee stock. ");
			btnReservarCantidad();

		}

	}

	public void clickConfirmarPedido() {

		click(btnConfirmarPedido, "Se presiona bot�n para confirmar el pedido.");
		click(btnValidarConfirmarPedido, "Se confirma la generaci�n del pedido. ");

	}

	public void agregarProductoObsoleto(String canalVenta, String patente, String ordenCompra, String cantidad,
			String codigo) throws Exception {
		CarritoPage carrito = new CarritoPage(driver);

		CabeceraPedidoPage cabeceraPedido = new CabeceraPedidoPage(driver);

		boolean a = cabeceraPedido.isPresent;
		if (a) {
			log().info(clienteCifMensaje);
			cabeceraPedido.completarCabeceraPedidoVtaCif(canalVenta, ordenCompra);
		} else {
			log().info(clienteBofMensaje);
			cabeceraPedido.completarCabeceraPedidoVta(canalVenta, patente, ordenCompra);

		}
		try {
			buscadorProductos(codigo);
			validarProductoPorIdentificador(codigo);
			Thread.sleep(3000);
			agregarCantidad(cantidad, codigo);
			// espera necesaria para productos obsoletos
			Thread.sleep(3000);
			agregarProducto(codigo);
			log().info("Se selecciona la cantidad y se agrega el producto obsoleto. ");

			if (cartelAtencionPedidoObsoleto() == false) {

				log().info(cantidadMenorMensaje);
				carrito.generarPedido();
				clickConfirmarPedido();
				Assert.assertTrue(cartelPedidoGenerado(), "Se valida cartel de generaci�n del pedido. ");
				log().info("Se valida la generaci�n del pedido. ");
				String pedido = numeroPedidoGenerado();
				log().info("Se genera pedido con n�mero: " + pedido);
				setNumeroPedido(pedido);

			} else {

				log().info(cantidadMayorMensaje);

			}
		} catch (Exception e) {
			throw new Exception("No se pudo completar la secci�n de productos disponibles del pedido.", e);
		}

	}

	public void completarProductosDisponibles(String codigo, String cantidad) throws Exception {
		CarritoPage carrito = new CarritoPage(driver);

		//try {
			buscadorProductos(codigo);
			validarProductoPorIdentificador(codigo);
			agregarCantidad(cantidad, codigo);
			agregarProducto(codigo);
			log().info(cantidadMenorMensaje);

			if (cartelPedidoBackOrder() == false) {

				carrito.generarPedido();
				clickConfirmarPedido();

			} else {

				log().info("El producto agregado no posee cantidad disponible suficiente. ");
				throw new Exception("Ocurri� un error al generar el Pedido solicitado. ");
			}

			if (cartelErrorPedidoSolicitado() == false) {

				Assert.assertTrue(cartelPedidoGenerado(), "Se valida cartel de generaci�n del pedido. ");
				log().info("Se valida la generaci�n del pedido. ");
				String pedido = numeroPedidoGenerado();
				log().info("Se genera pedido con n�mero: " + pedido);
				setNumeroPedido(pedido);

			} else {

				log().info("Se despliega cartel de alerta con el siguiente mensaje: " + mensajeErrorPedido());
				throw new Exception("Ocurri� un error al generar el Pedido solicitado. ");
			}

		/*} catch (Exception e) {
			throw new Exception(
					"No se pudo completar la secci�n de productos disponibles del pedido, no se confirm� el pedido.",
					e);
		}*/

	}

	public void validarGenenacionPedido() throws Exception {

		try {

			Assert.assertTrue(cartelPedidoGenerado(), "Se valida cartel de generaci�n del pedido. ");
			String pedido = numeroPedidoGenerado();
			log().info("Se gener� el pedido con el n�mero: " + pedido);
			setNumeroPedido(pedido);

		} catch (Exception e) {

			log().info("Se despliega cartel de alerta con el siguiente mensaje: " + mensajeErrorPedido());
			throw new Exception("Ocurri� un error al generar el Pedido solicitado. ");
		}

	}

	public void validarGenenacionPedidoBO() throws Exception {

		try {

			Assert.assertTrue(cartelPedidoBackOrderGeneradoSimple(),
					"Se valida cartel de generaci�n del pedido BackOrder. ");
			String pedidoBO = numeroPedidoBackOrderGeneradoSimple();
			log().info("Se genera pedido BO con n�mero: " + pedidoBO);
			setNumeroPedidoBo(pedidoBO);

		} catch (Exception e) {

			log().info("Se despliega cartel de alerta con el siguiente mensaje: " + mensajeErrorPedido());
			throw new Exception("Ocurri� un error al generar el Pedido solicitado. ");
		}

	}

	public void completarProductosDisponiblesBackOrder(String codigo, String cantidad) throws Exception {
		CarritoPage carrito = new CarritoPage(driver);

		try {
			buscadorProductos(codigo);
			validarProductoPorIdentificador(codigo);
			agregarCantidad(cantidad, codigo);
			agregarProducto(codigo);
			log().info(cantidadMayorMensaje);

			String pedidoReservaCartel = cartelGenerarPedidoReserva();
			Assert.assertTrue(pedidoReservaCartel.contains(mensajePedidoBackOrder));
			log().info("Se visualiza cartel de: " + mensajePedidoBackOrder);
			btnReservarCantidad();
			carrito.generarPedido();
			clickConfirmarPedido();
			validarGenenacionPedidoBO();

		} catch (Exception e) {

			throw new Exception("No se pudo completar la secci�n de productos disponibles del pedido.", e);

		}

	}

	public void completarProductosDisponiblesBackOrderConContrato(String codigo, String cantidad) throws Exception {

		try {
			buscadorProductos(codigo);
			validarProductoPorIdentificador(codigo);
			agregarCantidad(cantidad, codigo);
			agregarProducto(codigo);
			log().info(cantidadMayorMensaje);

			String pedidoReservaConContratoAtencion = cartelLlamadoCentroAtencion();
			Assert.assertTrue(pedidoReservaConContratoAtencion.contains(
					"La Opci�n Contratos permite generar �nicamente un Pedido de Venta. Consultar con Atenci�n al Cliente por el stock no disponible"));
			log().info("Se visualiza cartel de: "
					+ " La Opci�n Contratos permite generar �nicamente un Pedido de Venta. Consultar con su Asesor de Negocio o Atenci�n al Cliente por el stock no disponible. ");

		} catch (Exception e) {

			throw new Exception("No se pudo completar la secci�n de productos disponibles del pedido.", e);

		}

	}

	public void completarPedidoVenta(String canalVenta, String patente, String ordenCompra, String cantidad,
			String codigo) throws Exception {
		CabeceraPedidoPage cabeceraPedido = new CabeceraPedidoPage(driver);
		boolean a = cabeceraPedido.isPresent;
		if (a) {
			log().info(clienteCifMensaje);
			cabeceraPedido.completarCabeceraPedidoVtaCif(canalVenta, ordenCompra);
			completarProductosDisponibles(codigo, cantidad);
		} else {
			log().info(clienteBofMensaje);
			cabeceraPedido.completarCabeceraPedidoVta(canalVenta, patente, ordenCompra);
			completarProductosDisponibles(codigo, cantidad);
		}

	}

	public void completarPedidoVentaBackOrder(String canalVenta, String patente, String ordenCompra, String cantidad,
			String codigo) throws Exception {
		CabeceraPedidoPage cabeceraPedido = new CabeceraPedidoPage(driver);
		boolean a = cabeceraPedido.isPresent;
		log().info(a);
		if (a) {
			log().info(clienteCifMensaje);
			cabeceraPedido.completarCabeceraPedidoVtaCif(canalVenta, ordenCompra);
			completarProductosDisponiblesBackOrder(codigo, cantidad);
		} else {
			log().info(clienteBofMensaje);
			cabeceraPedido.completarCabeceraPedidoVta(canalVenta, patente, ordenCompra);
			completarProductosDisponiblesBackOrder(codigo, cantidad);
		}

	}

	public void completarPedidoMixto(String canalVenta, String ordenCompra, String patente, String cantidad,
			String codigoStock, String codigoSinStock) throws Exception {
		CarritoPage carrito = new CarritoPage(driver);
		CabeceraPedidoPage cabeceraPedido = new CabeceraPedidoPage(driver);

		boolean a = cabeceraPedido.isPresent;
		if (a) {
			log().info(clienteCifMensaje);
			cabeceraPedido.completarCabeceraPedidoVtaCif(canalVenta, ordenCompra);
		} else {
			log().info(clienteBofMensaje);
			cabeceraPedido.completarCabeceraPedidoVta(canalVenta, patente, ordenCompra);

		}
		try {
			Thread.sleep(4000);

			buscadorProductos(codigoStock);
			validarProductoPorIdentificador(codigoStock);

			agregarCantidad(cantidad, codigoStock);
			agregarProducto(codigoStock);
			log().info(cantidadMenorMensaje);
			// espera necesaria cuando se agregan varios productos
			Thread.sleep(4000);
			buscadorProductos(codigoSinStock);
			validarProductoPorIdentificador(codigoSinStock);
			agregarCantidad(cantidad, codigoSinStock);
			agregarProducto(codigoSinStock);
			log().info(cantidadMayorMensaje);
			String pedidoReservaCartel = cartelGenerarPedidoReserva();
			Assert.assertTrue(pedidoReservaCartel.contains(mensajePedidoBackOrder));
			log().info("Se visualiza cartel de: " + mensajePedidoBackOrder);
			btnReservarCantidad();
			carrito.generarPedido();
			clickConfirmarPedido();
			// Validacion del cartel con numero de pedido
			validarGenenacionPedido();
			validarGenenacionPedidoBO();

		} catch (Exception e) {
			throw new Exception(
					"No se pudo completar la secci�n de productos disponibles del pedido, no se gener� el pedido mixto.",
					e);
		}
	}

	public void completarPedidoMixtoConContrato(String canalVenta, String ordenCompra, String contrato, String patente,
			String cantidad, String codigoStock, String codigoSinStock) throws Exception {
		CarritoPage carrito = new CarritoPage(driver);
		CabeceraPedidoPage cabeceraPedido = new CabeceraPedidoPage(driver);

		boolean a = cabeceraPedido.isPresent;
		if (a) {
			log().info(clienteCifMensaje);
			cabeceraPedido.completarCabeceraPedidoVtaCifConContrato(canalVenta, contrato, ordenCompra);
		} else {

			cabeceraPedido.completarCabeceraPedidoVtaFobConContrato(canalVenta, contrato, patente, ordenCompra);
			log().info(clienteBofMensaje);

		}
		try {
			Thread.sleep(4000);

			buscadorProductos(codigoStock);
			validarProductoPorIdentificador(codigoStock);

			agregarCantidad(cantidad, codigoStock);
			agregarProducto(codigoStock);
			log().info(cantidadMenorMensaje);
			// espera necesaria cuando se agregan varios productos
			Thread.sleep(4000);
			buscadorProductos(codigoSinStock);
			validarProductoPorIdentificador(codigoSinStock);
			agregarCantidad(cantidad, codigoSinStock);
			agregarProducto(codigoSinStock);
			log().info(cantidadMayorMensaje);
			String pedidoReservaCartel = cartelGenerarPedidoReserva();
			Assert.assertTrue(pedidoReservaCartel
					.contains("La cantidad ingresada no est� disponible �quiere generar un Pedido de Reserva?"));
			log().info(
					"Se visualiza cartel de:  'La cantidad ingresada no est� disponible �quiere generar un Pedido de Reserva?'");
			btnReservarCantidad();
			carrito.generarPedido();
			clickConfirmarPedido();
			// Validacion del cartel con numero de pedido
			validarGenenacionPedido();
			validarGenenacionPedidoBO();

		} catch (Exception e) {
			throw new Exception(
					"No se pudo completar la secci�n de productos disponibles del pedido, no se gener� el pedido mixto.",
					e);
		}
	}

	public void cancelarPedidoApi() {
		sendRequest("DELETE", "Cancelar pedido de venta", "https://azapimngtdev.pan-energy.com/pdc-api/1.0/",
				"pedidos-ventas/" + getNumeroPedido(), "Ocp-Apim-Subscription-Key", "7f9fa0d81cdf4d029a3b7ebf9e27153e",
				"");

		// Se obtiene la respuesta
		int statusCode = getRequestResponse().getStatusCode();

		// Asersiones
		if (statusCode == 204) {
			log().info("Se cancel� el pedido n�mero: " + getNumeroPedido() + " satisfactoriamente.");
		} else {
			log().info("No se cancel� el pedido n�mero: " + getNumeroPedido() + " el servicio retorn� c�digo: "
					+ statusCode);
		}

	}

	public void cancelarPedidoBoApi() {
		sendRequest("DELETE", "Cancelar pedido de venta", "https://azapimngtdev.pan-energy.com/pdc-api/1.0/",
				"pedidos-ventas/" + getNumeroPedidoBo(), "Ocp-Apim-Subscription-Key",
				"7f9fa0d81cdf4d029a3b7ebf9e27153e", "");

		// Se obtiene la respuesta
		int statusCode = getRequestResponse().getStatusCode();

		// Asersiones
		if (statusCode == 204) {
			log().info("Se cancel� el pedido BackOrder n�mero: " + getNumeroPedidoBo() + " satisfactoriamente.");
		} else {
			log().info("No se cancel� el pedido BackOrder n�mero: " + getNumeroPedidoBo()
					+ " el servicio retorn� c�digo: " + statusCode);
		}

	}

	public void cancelarPedidoMixtoApi() throws Exception {

		try {
			cancelarPedidoApi();
			cancelarPedidoBoApi();

		} catch (Exception e) {
			throw new Exception("Ocurri� un error al cancelar el pedido mixto mediante API Rest.", e);
		}
	}

}