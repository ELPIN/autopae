package com.everis.po;

import com.everis.global.Global;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class CarritoPage extends Global {

	// Definir web elements a nivel de clase

	public CarritoPage(WebDriver d) {
		super(d);
		PageFactory.initElements(driver, this);
	}

	public String cartelProductosDisponibles() {
		return carritoCompra.getText();
	}

	public String cartelTotalYPeso() {
		return getText(cartelTotalYPeso);
	}

	public String resumenCarrito() {
		return getText(resumenCarrito);
	}

	public String resumenProductoCarrito() {
		return resumenProductoCarrito.getText();
	}

	public String resumenCantidadSolicitadaCarrito() {
		return resumenCantidadSolicitadaCarrito.getText();
	}
	
	public String bntGenerarPedido() {
		return getText(btnGenerarPedido);
	}

	@FindBy(name = "shopping-cart")
	private WebElement carritoCompra;

	@FindBy(xpath = "//div//a[contains(.,'Agregar m�s productos')]")
	private WebElement btnAgregarMasProductos;

	@FindBy(xpath = "//div//a[contains(.,'Modificar varios productos')]")
	private WebElement btnModificarProductos;

	@FindBy(xpath = "//div//button[@type='submit' and contains (.,' Modificar Seleccionados ')]")
	private WebElement btnModificarSeleccionado;

	@FindBy(id = "btn-cancelar_carrito")
	private WebElement btnCancelarPedido;

	@FindBy(xpath = "//div//button[@class='btn btn-success btn-lg' and contains (.,'Continuar')]")
	private WebElement btnConfirmarCancelar;

	@FindBy(xpath = "//div[@class='float-right']")
	private WebElement cartelTotalYPeso;

	@FindBy(name = "trash-2")
	private WebElement iconoEliminarProducto;

	@FindBy(xpath = "//div//button[@class='btn btn-primary border-radius w-100 p-2 mb-3 mb-xl-0' and contains (.,'Generar Pedido')]")
	private WebElement btnGenerarPedido;
	
	@FindBy(xpath = "//div//button[@type='submit' and contains (.,'Generar Pedido')]")
	private WebElement btnGenerarPedidoCarrito;

	@FindBy(xpath = "//div//tr[@class='border-0 p-0 my-0']")
	private WebElement resumenCarrito;

	@FindBy(xpath = "//td//span[@class='itemCarritoTable']")
	private WebElement resumenProductoCarrito;

	@FindBy(xpath = "//td[@id='cantidad-solicitada']")
	private WebElement resumenCantidadSolicitadaCarrito;

	@FindBy(xpath = "//tr//td[@id='cantidad-stock' and not(contains(@class,'col-2 col-sm-2 col-md-2 col-lg-2 abs-center text-danger'))]")
	private WebElement resumenCantidadDisponibleCarrito;
	
	@FindBy(xpath = "//div//a[@class='nav-link carrito']")
	private WebElement carritoIsDisplayed;
	
	private String quitarProducto = "//tbody//tr[td/span[contains(.,'textoButton')]]//td/input[@value='Quitar']";
	private String textoButton = "textoButton";
	private String validarProducto = "//td//span[@class='itemCarritoTable' and contains(.,'textoButton')]";
	private String validarCantidadSolicitada = "//tbody//tr[td/span[contains(.,'textoButton')]]//td[@id='cantidad-solicitada']";
	private String validarCantidadDisponible = "//tbody//tr[td/span[contains(.,'textoButton')]]//td[@id='cantidad-stock']";

	// Steps

	public Boolean carritoIsDisplayed() {
		waitForVisibility(carritoIsDisplayed);
		return carritoIsDisplayed.isDisplayed();
	}
	
	public Boolean btnModificarSeleccionadosIsDisplayed() {
		waitForVisibility(btnModificarSeleccionado);
		return btnModificarSeleccionado.isDisplayed();
	}

	public void agregarProducto(String producto) {

		String xpathButton = reemplazarEnXpath(quitarProducto, textoButton, producto);
		driver.findElement(By.xpath(xpathButton)).click();

	}

	public void carritoCompra() throws InterruptedException {
		click(carritoCompra,"Se ingresa al carrito de compras.");
	}

	public void btnAgregarProductos() {

		click(btnAgregarMasProductos,"Se presiona bot�n de agregar m�s productos.");
	}

	public void btnModificarProductos() {

		click(btnModificarProductos,"Se presiona bot�n de modificar productos.");

	}

	public void cancelarPedido() {

		click(btnCancelarPedido,"Se presiona bot�n de cancelar pedido.");
		click(btnConfirmarCancelar,"Se cancela el pedido");

	}

	public void eliminarProducto() {

		click(iconoEliminarProducto,"Se presiona �cono de eliminar producto.");
		click(btnConfirmarCancelar,"Se presiona bot�n de continuar. ");

	}

	public void generarPedido() {

		click(btnGenerarPedido,"Se presiona bot�n de generar pedido.");

	}
	
	public void generarPedidoCarrito() {

		click(btnGenerarPedidoCarrito,"Se presiona bot�n de generar pedido.");

	}

	// Actions

	public boolean validarResumenProductosCarrito(String codigoProductoStock, String codigoProductoSinStock,
			String cantidad) {

		try {
			
			// valida c�digo de productos del carrito
			String xpathProductoStock = reemplazarEnXpath(validarProducto, textoButton, codigoProductoStock);
			String a = driver.findElement(By.xpath(xpathProductoStock)).getText();
			Assert.assertTrue(a.contains(codigoProductoStock));
			String xpathProductoSinStock = reemplazarEnXpath(validarProducto, textoButton,
					codigoProductoSinStock);
			String b = driver.findElement(By.xpath(xpathProductoSinStock)).getText();
			Assert.assertTrue(b.contains(codigoProductoSinStock));
			log().info("Se validan los c�digos de los productos presentes en el resumen del carrito");
			
			// valida cantidad solicitada de productos del carrito
			String xpathCantidadProductoStock = reemplazarEnXpath(validarCantidadSolicitada, textoButton,
					codigoProductoStock);
			String c = driver.findElement(By.xpath(xpathCantidadProductoStock)).getText();
			Assert.assertTrue(c.contains(cantidad));
			String xpathCantidadProductoSinStock = reemplazarEnXpath(validarCantidadSolicitada, textoButton,
					codigoProductoSinStock);
			String d = driver.findElement(By.xpath(xpathCantidadProductoSinStock)).getText();
			Assert.assertTrue(d.contains(cantidad));
			log().info("Se validan las cantidades solicitadas de los productos presentes en el resumen del carrito");
			
			// valida cantidad disponible de productos del carrito
			String xpathProductoCantidadDisponibleStock = reemplazarEnXpath(validarCantidadDisponible,
					textoButton, codigoProductoStock);
			String e = driver.findElement(By.xpath(xpathProductoCantidadDisponibleStock)).getText();
			Assert.assertTrue(e.contains("-"));
			String xpathProductoCantidadDisponibleSinStock = reemplazarEnXpath(validarCantidadDisponible,
					textoButton, codigoProductoSinStock);
			String f = driver.findElement(By.xpath(xpathProductoCantidadDisponibleSinStock)).getText();
			Assert.assertTrue(f.contains("0"));
			log().info("Se validan las cantidades disponibles de los productos presentes en el resumen del carrito");

			return true;
		} catch (Exception e) {
			log().info("Hubo un error en la validaci�n del resumen del carrito");
			return false;

		}

	}

}
