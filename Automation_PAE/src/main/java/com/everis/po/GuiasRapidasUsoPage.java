package com.everis.po;

import com.everis.global.Global;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.Iterator;
import java.util.Set;

public class GuiasRapidasUsoPage extends Global {

	// Definir web elements a nivel de clase

	public GuiasRapidasUsoPage(WebDriver d) {
		super(d);
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "(//div[@class='card-body']//h5[@class='card-title pb-2'])[1]")
	private WebElement headerPresentacionGuia;

	@FindBy(xpath = "//a//p[contains (.,'Video Presentaci�n Portal')]")
	private WebElement btnPresentacionGuia;

	@FindBy(xpath = "(//div[@class='card-body']//h5[@class='card-title pb-2'])[2]")
	private WebElement headerManualAplicacion;

	@FindBy(xpath = "//a//p[contains (.,'Documento resumen')]")
	private WebElement btnManualAplicacion;
	
	@FindBy(xpath = "//a//p[contains (.,'Funcionalidades')]")
	private WebElement btnFuncionalidadesEcenter;

	@FindBy(xpath = "//a//p[contains (.,'Documento resumen de navegaci�n')]")
	private WebElement btnIngresoAplicacion;
	
	@FindBy(xpath = "//a//p[contains (.,'Gesti�n de Pedidos de Lubricantes')]")
	private WebElement btnGestionPedidos;
	
	@FindBy(xpath = "//a//p[contains (.,'T�rminos y Condiciones')]")
	private WebElement btnTyC;
	
	@FindBy(xpath = "//a//p[contains (.,'Cuenta corriente y monto disponible')]")
	private WebElement btnCuentaCorriente;

	@FindBy(xpath = "//a//p[contains (.,'B�squeda de Productos Lubricantes')]")
	private WebElement btnProductosLubricantes;
	
	@FindBy(xpath = "(//div[@class='card-body']//h5[@class='card-title pb-2'])[3]")
	private WebElement headerIntroduccionEnergyCenterGuia;

	@FindBy(xpath = "(//div[@class='card-body']//h5[@class='card-title pb-2'])[4]")
	private WebElement headerIngresoAplicacionGuia;

	@FindBy(xpath = "(//div[@class='card-body']//h5[@class='card-title pb-2'])[5]")
	private WebElement headerGestionPedidosGuia;

	@FindBy(xpath = "(//div[@class='card-body']//h5[@class='card-title pb-2'])[6]")
	private WebElement headerTyCGuia;

	@FindBy(xpath = "(//div[@class='card-body']//h5[@class='card-title pb-2'])[7]")
	private WebElement headerCuentaCorrienteGuia;

	@FindBy(xpath = "(//div[@class='card-body']//h5[@class='card-title pb-2'])[8]")
	private WebElement headerProductosLubricantes;

	@FindBy(xpath = "//div[@class='container-fluid mt-xl-0 mt-md-3']//h4")
	private WebElement headerGuias;

	@FindBy(xpath = "//h4[@class='modal-title']")
	private WebElement cartelVideoPresentacion;

	@FindBy(xpath = "//iframe[@class='embed-responsive-item']")
	private WebElement frameVideo;

	@FindBy(xpath = "//button[@class='ytp-large-play-button ytp-button']")
	private WebElement btnPlayVideo;

	public String headerGuias() {
		return getText(headerGuias);
	}

	public String headerPresentacionGuia() {
		return getText(headerPresentacionGuia);
	}

	public String headerManualAplicacion() {
		return getText(headerManualAplicacion);
	}

	public String headerIntroduccionEnergyCenterGuia() {
		return getText(headerIntroduccionEnergyCenterGuia);
	}

	public String headerIngresoAplicacionGuia() {
		return getText(headerIngresoAplicacionGuia);
	}

	public String headerGestionPedidosGuia() {
		return getText(headerGestionPedidosGuia);
	}

	public String headerTyCGuia() {
		return getText(headerTyCGuia);
	}

	private String headerProductosLubricantes() {
		return getText(headerProductosLubricantes);
	}

	public String headerCuentaCorrienteGuia() {
		return getText(headerCuentaCorrienteGuia);
	}

	public String popupVideoPresentacion() {
		return getText(cartelVideoPresentacion);
	}

	// Steps

	public void clickPresentacion() throws InterruptedException {

		click(btnPresentacionGuia, "Se presiona el bot�n del video de Presentaci�n.");

	}

	public void validarWebTab(String guia) throws Exception {
		try {
			String parent = driver.getWindowHandle();
			Set<String> s = driver.getWindowHandles();

			Iterator<String> t1 = s.iterator();

			while (t1.hasNext()) {

				String childWindow = t1.next();

				if (!parent.equals(childWindow)) {
					driver.switchTo().window(childWindow);
					String a = driver.switchTo().window(childWindow).getCurrentUrl();

					//Asersion
					log().info("Se valida despliegue de: " + guia);
					Assert.assertTrue(a.contains(guia));
					log().info("Se valid� satisfactoriamente despliegue de: " + guia);
					
					driver.close();
				}
			}
		} catch (Exception e) {
			throw new Exception("Ocurri� un error al validar: " + guia, e);
		}
	}
	
	public void visualizarVideo() throws Exception {

		try {

			switchTo(frameVideo, "Se cambia de frame.");
			click(btnPlayVideo, "Se presiona el bot�n de Play del video.");
			Thread.sleep(3000);

		} catch (Exception e) {
			throw new Exception("Ocurri� un error al cambiar de frame y reproducir el video.", e);
		}
	}

	
	public void clickManualAplicacion(String guia) throws Exception {

		click(btnManualAplicacion, "Se presiona el bot�n del Manual de Aplicaci�n.");
		Thread.sleep(5000);
		validarWebTab(guia);
	}
	
	public void clickIntroduccionEcenter(String guia) throws Exception {

		click(btnFuncionalidadesEcenter, "Se presiona el bot�n de Introducci�n a ENERGY CENTER.");
		Thread.sleep(5000);
		validarWebTab(guia);
	}

	public void clickIngresoAplicacion(String guia) throws Exception {

		click(btnIngresoAplicacion, "Se presiona el bot�n de Ingreso a la Aplicaci�n.");
		Thread.sleep(5000);
		validarWebTab(guia);
	}
	
	public void clickGestionPedidos(String guia) throws Exception {

		click(btnGestionPedidos, "Se presiona el bot�n de Gesti�n de Pedidos.");
		Thread.sleep(5000);
		validarWebTab(guia);
	}
	
	public void clickTyC(String guia) throws Exception {

		click(btnTyC, "Se presiona el bot�n de T&C.");
		Thread.sleep(5000);
		validarWebTab(guia);
	}
	
	public void clickCuentaCorriente(String guia) throws Exception {
		click(btnCuentaCorriente, "Se presiona el bot�n de Cuenta Corriente.");
		Thread.sleep(5000);
		validarWebTab(guia);
	}

	public void clickProductosLubricantes(String guia) throws Exception {
		click(btnProductosLubricantes, "Se presiona el bot�n de Productos Lubricantes.");
		Thread.sleep(5000);
		validarWebTab(guia);
	}
		
	public boolean validarGuiasRapidasUso() {

		try {

			// Se valida gu�a Presentaci�n
			String guiaPresentacion = headerPresentacionGuia();
			Assert.assertTrue(guiaPresentacion.contains("Presentaci�n"));
			log().info("Se valida gu�a Presentaci�n.");

			// Se valida gu�a Manual de Aplicaci�n
			String guiaManualAplicacion = headerManualAplicacion();
			Assert.assertTrue(guiaManualAplicacion.contains("Manual de Aplicaci�n"));
			log().info("Se valida gu�a Manual de Aplicaci�n.");

			// Se valida gu�a Introducci�n a ENERGY CENTER
			String guiaIntroduccionEnergyCenter = headerIntroduccionEnergyCenterGuia();
			Assert.assertTrue(guiaIntroduccionEnergyCenter.contains("Introducci�n a ENERGY CENTER"));
			log().info("Se valida gu�a Introducci�n a ENERGY CENTER.");

			// Se valida gu�a Ingreso a la Aplicaci�n
			String guiaIngresoAplicacionGuia = headerIngresoAplicacionGuia();
			Assert.assertTrue(guiaIngresoAplicacionGuia.contains("Ingreso a la Aplicaci�n"));
			log().info("Se valida gu�a Ingreso a la Aplicaci�n.");

			// Se valida gu�a Gesti�n de pedidos
			String guiaGestionPedidos = headerGestionPedidosGuia();
			Assert.assertTrue(guiaGestionPedidos.contains("Gesti�n de pedidos"));
			log().info("Se valida gu�a Gesti�n de pedidos.");

			//Se valida gu�a TyC
			String guiaTyC = headerTyCGuia();
			Assert.assertTrue(guiaTyC.contains("T�rminos y Condiciones"));
			log().info("Se valida gu�a T&C.");

			//Se valida gu�a Cuenta corriente
			String guiaCuentaCorriente = headerCuentaCorrienteGuia();
			Assert.assertTrue(guiaCuentaCorriente.contains("Cuenta Corriente"));
			log().info("Se valida gu�a Cuenta Corriente.");

			//Se valida gu�a B�squeda de Productos Lubricantes
			String guiaProductosLubricantes = headerProductosLubricantes();
			Assert.assertTrue(guiaCuentaCorriente.contains("Cuenta Corriente"));
			log().info("Se valida gu�a B�squeda de Productos Lubricantes.");

			log().info("Se valida la presencia de todas las Gu�as rapidas de uso.");

			return true;
		} catch (Exception e) {
			log().info("Hubo un error en la validaci�n de las Gu�as rapidas de uso.");
			return false;

		}

	}



}
