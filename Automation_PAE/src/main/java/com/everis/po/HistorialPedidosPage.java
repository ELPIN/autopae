package com.everis.po;

import com.everis.global.Global;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.List;

public class HistorialPedidosPage extends Global {

	// Definir web elements a nivel de clase

	public HistorialPedidosPage(WebDriver d) {
		super(d);
		PageFactory.initElements(driver, this);
	}

	List<String> listaPedidos = new ArrayList<String>();

	@FindBy(xpath = "//input[@formcontrolname='nroOrden']")
	private WebElement campoNroOrden;

	@FindBy(xpath = "//form//div//button[@class='btn justify-content-center rounded bg-primary text-white w-100']")
	private WebElement btnBuscarHistorial;

	@FindBy(xpath = "//button//span[contains (., 'Descargar reporte')]")
	private WebElement btnDescargarReporte;

	@FindBy(xpath = "//a[@data-target='#search']")
	private WebElement iconoHistorial;

	@FindBy(xpath = "//select[@formcontrolname='estados']")
	private WebElement listaEstados;

	@FindBy(xpath = "//input[@formcontrolname='producto']")
	private WebElement campoProductoHistorial;

	@FindBy(xpath = "//div//img[@class='historial-icon']")
	private WebElement headerHistorial;

	@FindBy(xpath = "//span[@data-target='#modal-detalle-pedido']")
	private WebElement grillaDetallePedido;

	@FindBy(xpath = "//div[@class='btn status' and contains (., ' �ltimos 90 d�as ')]")
	private WebElement grillaBusquedaPedidos;

	private String validarNroPedido = "//td[img[@alt='�cono de Pedido de Lubricantes']]/span[contains(.,'textoButton')]";
	private String textoButton = "textoButton";

	// Steps

	public Boolean headerHistorialPedido() {
		waitForVisibility(headerHistorial);
		return headerHistorial.isDisplayed();
	}

	public Boolean grillaBusquedaProductos() {
		waitForVisibility(grillaBusquedaPedidos);
		return grillaBusquedaPedidos.isDisplayed();
	}

	public void descargarReporte() {
		click(btnDescargarReporte, "Se visualiza y se presiona el bot�n de descargar reporte. ");
	}

	public void validarBusquedaPedido(String nroPedido) throws Exception {

		// validaci�n de la b�squeda

		String xpathButton = reemplazarEnXpath(validarNroPedido, textoButton, nroPedido);

		boolean a = driver.findElements(By.xpath(xpathButton)).size() > 0;

		if (a == true) {
			log().info("Se valida la b�squeda del pedido. ");

		} else {
			log().info("El pedido generado no se encuentra en el historial.");
			throw new Exception("El pedido generado no se encuentra en el historial. ");

		}
	}

	public void btnBuscarHistorial() {
		click(btnBuscarHistorial, "Se presiona bot�n de buscar historial.");
	}

	// Actions

	/**
	 * @param noPedido This string may be use for further computation in overriding
	 *                 classes
	 */
	public void buscarNoPedido(String noPedido) throws Exception {
		IndexPage index = new IndexPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);

		if (productosDisponibles.validarPedidoBusqueda() == true) {
			try {

				productosDisponibles.clickBtnVolver();
				index.clickSeccionPedidos();
				Thread.sleep(15000);
				click(iconoHistorial, "Se presiona icono de Historial de Pedidos.");
				headerHistorialPedido();
				sendKeys(campoNroOrden, ProductosDisponiblesPage.getNumeroPedido(),
						"Se busca el n�mero del pedido " + ProductosDisponiblesPage.getNumeroPedido());
				btnBuscarHistorial();
			} catch (Exception e) {
				throw new Exception("No se complet� la b�squeda del pedido. ", e);
			}
			Thread.sleep(5000);
			validarBusquedaPedido(ProductosDisponiblesPage.getNumeroPedido());

		} else {
			try {
				click(iconoHistorial, "Se presiona icono de Historial de Pedidos.");
				headerHistorialPedido();
				sendKeys(campoNroOrden, noPedido, "Se busca el n�mero del pedido " + noPedido);
				btnBuscarHistorial();
				validarBusquedaPedido(noPedido);

			} catch (Exception e) {
				throw new Exception("No se realiz� la b�squeda de manera satisfactoria. ", e);
			}
		}
	}

	public void busquedaEstado(String estado) throws Exception {
		IndexPage index = new IndexPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);

		productosDisponibles.clickBtnVolver();
		index.clickSeccionPedidos();
		click(iconoHistorial, "Se presiona icono de Historial de Pedidos.");
		headerHistorialPedido();
		Select selectObject = new Select(listaEstados);
		selectObject.selectByVisibleText(estado);
		log().info("Se selecciona el estado: " + estado);
		btnBuscarHistorial();
		Assert.assertTrue(grillaBusquedaProductos(), "Se despliega la grilla con los pedidos.");
		log().info("Se despliega la grilla con los pedidos. ");

	}

	public void busquedaProducto(String codigo) throws Exception {
		IndexPage index = new IndexPage(driver);
		ProductosDisponiblesPage productosDisponibles = new ProductosDisponiblesPage(driver);

		productosDisponibles.clickBtnVolver();
		index.clickSeccionPedidos();
		click(iconoHistorial, "Se presiona icono de Historial de Pedidos.");
		campoProductoHistorial.sendKeys(codigo);
		headerHistorialPedido();
		log().info("Se busca producto por c�digo: " + codigo);
		btnBuscarHistorial();
		Assert.assertTrue(grillaBusquedaProductos(), "Se despliega la grilla con los pedidos.");
		log().info("Se despliega la grilla con los pedidos. ");

	}

}
