package com.everis.po;

import com.everis.global.Global;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

public class MontoDisponiblePage extends Global {

    public MontoDisponiblePage(WebDriver d) {
        super(d);
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//div//select[@id='acc-filter-dropdown']")
    private WebElement selectFiltroAcc;

    @FindBy(xpath = "//div//button[contains (.,'Detalle')]")
    private WebElement btnDetalle;

    @FindBy(xpath = "//tr//td//div//span[contains (.,' Pedidos Liberados ')]")
    private WebElement pedidosLiberados;

    @FindBy(xpath = "//tr//td//div//span[contains (.,'Partidas Abiertas')]")
    private WebElement partidasAbiertas;

    @FindBy(xpath = "//tr//td//div//span[contains (.,'Deudoras')]")
    private WebElement partidasAbiertasDeudoras;

    @FindBy(xpath = "//tr//td//div//span[contains (.,'Acreedoras')]")
    private WebElement partidasAbiertasAcreedoras;

    @FindBy(xpath = "//tr//td//div//span[contains (.,'Cheques pendientes de Acreditar')]")
    private WebElement chequesPendientesAcreditar;

    @FindBy(xpath = "//div[@class='modal-footer']//button[contains (.,'Aceptar')]")
    private WebElement btnAceptarModalSaldoConsumido;

    @FindBy(xpath = "//tr//div//div//span[contains (.,'Detalle Pedidos Liberados')]")
    private WebElement tituloDetallePedidosLiberados;



    String textoFiltroAccPredeterminado = "ACC-0020 Lubes";

    public void seleccionarFiltroAcc(String filtroAcc){
        Select selectObject = new Select(selectFiltroAcc);

        if (filtroAcc.contains("ACC-0003 Industria")){  //Este filtro especifico no me deja seleccionarlo mediante el texto, Por un tema de la codificacion proveniente de Excel.
            selectObject.selectByVisibleText("ACC-0003 Industria...");
        }
        else{
            selectObject.selectByVisibleText(filtroAcc);
        }
        log().info("Se selecciona el filtro ACC:" + filtroAcc);
    }

    public String getTextFiltroAccActual(){
        Select selectObject = new Select(selectFiltroAcc);
        WebElement option = selectObject.getFirstSelectedOption();
        return option.getText();
    }

    public String getTextFiltroAccPredeterminado(){
        return textoFiltroAccPredeterminado;
    }

    public void clickDetalle(){
        click(btnDetalle, "Se clickea el boton saldo consumido");
    }

    public void verPedidosLiberados(){
        clickDetalle();
        click(pedidosLiberados, "Se clickea sobre pedidos liberados");
    }

    public void verPartidasAbiertasDeudoras(){
        clickDetalle();
        click(partidasAbiertas, "Se clickea sobre partidas abiertas");
        click(partidasAbiertasDeudoras, "Se clickea sobre Deudoras");
    }

    public void verPartidasAbiertasAcreedoras(){
        clickDetalle();
        click(partidasAbiertas, "Se clickea sobre partidas abiertas");
        click(partidasAbiertasAcreedoras, "Se clickea sobre Acreedoras");
    }

    public void verChequesPendientesAcreditar(){
        clickDetalle();
        click(chequesPendientesAcreditar, "Se clickea sobre Cheques pendientes a acreditar");
    }

    public boolean validarTransladoACuentaCorriente(){
        try{
            WebElement tituloPartidasAbiertas = driver.findElement(By.xpath("//div//div//div//h4[contains (., 'Cuenta Corriente: Partidas Abiertas y Cheques Pendientes/Rechazados')]"));
            return tituloPartidasAbiertas.isDisplayed();
        }
        catch (NoSuchElementException e){
            return false;
        }
    }

    public boolean validarSeccionPedidosLiberados(){
        return tituloDetallePedidosLiberados.isDisplayed();
    }

}
