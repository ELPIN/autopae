package com.everis.po;

import com.everis.global.Global;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PreguntasFrecuentesPage extends Global {

	// Definir web elements a nivel de clase

	public PreguntasFrecuentesPage(WebDriver d) {
		super(d);
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath = "//div[@class='card card-list']")
	private WebElement grillaPreguntasFrecuentes;

	@FindBy(xpath = "//div[@class='card-body']")
	private WebElement bodyRespuestas;

	public String grillaPreguntasFrecuentes() {
		return getText(grillaPreguntasFrecuentes);
	}

	private String preguntaFrecuenteOption = "//button[contains(.,'textoButton')]";
	private String textoButton = "textoButton";

	// Steps

	public Boolean bodyRespuestaPreguntaFrecuente() {
		waitForVisibility(bodyRespuestas);
		return bodyRespuestas.isDisplayed();
	}

	public void clickPreguntaFrecuente(String pregunta) throws Exception {

		try {
			String xpathButton = reemplazarEnXpath(preguntaFrecuenteOption, textoButton, pregunta);

			driver.findElement(By.xpath(xpathButton)).click();
			log().info("Se da click a la pregunta: " + pregunta);
			
		} catch (Exception e) {
			throw new Exception("Ocurri� un error al dar click a la Pregunta Frecuente " + pregunta, e);
		}
	}

}
