package com.everis.po;

import com.everis.global.Global;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CuentaCorrientePage extends Global {

	// Definir web elements a nivel de clase

	public CuentaCorrientePage(WebDriver d) {
		super(d);
		PageFactory.initElements(driver, this);
	}

	public static String getAsignacionDefault() {
		return defaultAsignacion;
	}

	private static void setAsignacionDefault(String defaultAsignacion) {
		CuentaCorrientePage.defaultAsignacion = defaultAsignacion;
	}

	private static String defaultAsignacion;

	@FindBy(xpath = "//h1[@class='text-blue']")
	private WebElement loginMessage;

	@FindBy(id = "acc-filter-dropdown")
	private WebElement selectAccCtaCorriente;

	@FindBy(xpath = "(//tbody//tr[@class='font-weight-medium partidas-abiertas']//td)[1]")
	private WebElement trAsignacion;

	@FindBy(id = "acc-filter-dropdown")
	private WebElement selectElementACC;

	@FindBy(xpath = "//div/button[contains (., 'Exportar Excel')]")
	private WebElement btnExportarExcel;

	@FindBy(xpath = "//a/span[contains (.,'Partidas Aplicadas')]")
	private WebElement btnPartidasAplicadas;

	@FindBy(xpath = "//a/span[contains (.,'Cheques Rechazados Hist�ricos')]")
	private WebElement btnChequesRechazados;

	@FindBy(xpath = "//div/button[contains (.,'Filtrar')]")
	private WebElement btnFiltrar;

	@FindBy(xpath = "//ngb-datepicker-navigation-select//select[@title='Select month']")
	private WebElement selectMonth;

	@FindBy(xpath = "//ngb-datepicker-navigation-select//select[@title='Select year']")
	private WebElement selectYear;

	@FindBy(xpath = "//th//div//div[contains (.,'Partidas Abiertas')]")
	private WebElement tituloColumnasPartidasAbiertas;


	private String pruebaString = "//table[@id='table-cuenta-corriente']//tbody";

	// Steps

	public String getDefaultAsignacion() {
		return getText(trAsignacion);
	}

	protected Boolean grillaCtaCorriente() {

		return driver.findElements(By.xpath(pruebaString)).size() > 0;
	}

	// Actions

	public void validarAreasControlCredito() throws Exception {
		Select selectObject = new Select(selectAccCtaCorriente);

		// Se recorre grilla y se guardan en una lista
		List<WebElement> elements = driver.findElements(By.xpath("//select[@id='acc-filter-dropdown']//option"));

		List<String> names = new ArrayList<String>();

		try {
			log().info("Se obtienen las �reas de Control de Cr�dito y se valida ACC por defecto.");
			for (WebElement element : elements) {
				names.add(element.getText());
			}

			String defaultAsignacion = getDefaultAsignacion();
			setAsignacionDefault(defaultAsignacion);

			int count = 0;
			int control = 0;

			log().info("Se recorre y se valida lista de �reas de Contol de Cr�dito.");

			for (int i = 0; i < names.size(); i++) {

				selectObject.selectByVisibleText(names.get(count));

				if (getAsignacionDefault().equals(getDefaultAsignacion()) && control == 0) {
					log().info(
							"La Asignacion inicialmente obtenida coincide con la actual. Se identifica Asignaci?n por defecto.");
					count++;
					control++;
				} else {

					Assert.assertFalse(defaultAsignacion.equals(getDefaultAsignacion()), "Se comparan valores.");
					log().info("La primera Asignaci�n del " + names.get(count) + " es: " + getDefaultAsignacion());
					count++;
				}
			}
			
			log().info("Se validan todas las ACC satisfactoriamente.");

		} catch (Exception e) {
			log().info("Ocurri� un error en la obtenci�n y/o validaci�n de ACC en la secci�n de Cuenta Corriente.");
			throw new Exception(e.getMessage());
		}

	}

	public void validarReferencia(String referenciaId){
		WebElement referenciaLink = driver.findElement(By.xpath(String.format("//tr/td[contains (., '%s')]", referenciaId)));
		click(referenciaLink, "abro el PDF de factura en otra pesta�a");

	}

	public void seleccionarACC(String filtroACC) throws Exception {

		Select selectObject = new Select(selectElementACC);
		try {
			selectObject.selectByVisibleText(filtroACC);
			Thread.sleep(3000);
			log().info("Se selecciona el Filtro ACC.");
		} catch (Exception e) {
			log().info("No se pudo seleccionar el Filtro ACC.");
			throw new Exception("No se pudo seleccionar el Filtro ACC.");

		}

	}

	public void completarFechaHasta(String dia, String mes, String ano) throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, TIMEWAIT);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div//div[@class='table-responsive d-inline-flex flex-wrap overflow-visible abs-center']")));
		waitForVisibility(btnFiltrar); // La pagina de cuenta corriente puede tardar en cargar a veces por el servicio

		//Se clickea sobre el boton del calendario
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("document.querySelector('div.cont-icon',':before').click();");

		//Se selecciona el a�o
		log().info("Se selecciona el a�o: " + ano);
		Select selectObjectY = new Select(selectYear);
		selectObjectY.selectByVisibleText(ano);

		//Se selecciona el mes
		log().info("Se selecciona el mes: " + mes);
		Select selectObjectM = new Select(selectMonth);
		selectObjectM.selectByVisibleText(mes);

		//Se selecciona el dia
		WebElement btnDia = driver.findElement(By.xpath("//div[@role='gridcell']//div[contains (.,'" + dia + "')]"));
		click(btnDia, "Se clickea sobre el boton del dia");

		//Se filtra
		click(btnFiltrar,"Se clickea el boton filtrar");

	}

	public void completarFechaDesdeHasta(String diaDesde, String mesDesde, String anoDesde, String diaHasta, String mesHasta, String anoHasta) throws InterruptedException {

		WebDriverWait wait = new WebDriverWait(driver, TIMEWAIT);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div//div[@class='table-responsive d-inline-flex flex-wrap overflow-visible abs-center']")));
		waitForVisibility(btnFiltrar); // La pagina de cuenta corriente puede tardar en cargar a veces por el servicio

		//Se clickea sobre el boton del calendario Desde
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript(" document.querySelectorAll('div.cont-icon',':before').item(0).click();");

		//Se selecciona el a�o
		log().info("Se selecciona el a�o: " + anoDesde);
		Select selectObjectY = new Select(selectYear);
		selectObjectY.selectByVisibleText(anoDesde);

		//Se selecciona el mes
		log().info("Se selecciona el mes: " + mesHasta);
		Select selectObjectM = new Select(selectMonth);
		selectObjectM.selectByVisibleText(mesDesde);

		//Se selecciona el dia
		WebElement btnDia = driver.findElement(By.xpath("//div[@role='gridcell']//div[contains (.,'" + diaDesde + "')]"));
		click(btnDia, "Se clickea sobre el boton del dia");

		//Se clickea sobre el boton del calendario Hasta
		js.executeScript(" document.querySelectorAll('div.cont-icon',':before').item(1).click();");

		//Se selecciona el a�o Hasta
		log().info("Se selecciona el a�o: " + anoHasta);
		Select selectObjectYHasta = new Select(selectYear);
		selectObjectY.selectByVisibleText(anoHasta);

		//Se selecciona el mes Hasta
		log().info("Se selecciona el mes: " + mesHasta);
		Select selectObjectMHasta= new Select(selectMonth);
		selectObjectM.selectByVisibleText(mesHasta);

		//Se selecciona el dia Hasta
		WebElement btnDiaHasta = driver.findElement(By.xpath("//div[@role='gridcell']//div[contains (.,'" + diaHasta + "')]"));
		click(btnDiaHasta, "Se clickea sobre el boton del dia");

		//Se filtra
		click(btnFiltrar,"Se clickea el boton filtrar");
	}

	public void exportarExcel(){
		click(btnExportarExcel, "Se exporta el Excel de cuenta corriente");
	}

	public void clickFiltrarFechas(){
		WebDriverWait wait = new WebDriverWait(driver, TIMEWAIT);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div//div[@class='table-responsive d-inline-flex flex-wrap overflow-visible abs-center']")));
		click(btnFiltrar, "Se clickea en Filtrar");
	}

	public void clickPartidasAplicadas(){
		waitForVisibility(tituloColumnasPartidasAbiertas); //Porque entrar a la pagina de Cuenta corriente puede tardar por la data, cuando s muestra ese elemento es que ya cargo.
		click(btnPartidasAplicadas, "Se clickea partidas aplicadas");
	}

	public void clickChequesRechazados(){
		waitForVisibility(tituloColumnasPartidasAbiertas); //Porque entrar a la pagina de Cuenta corriente puede tardar por la data, cuando s muestra ese elemento es que ya cargo.
		click(btnChequesRechazados, "Se clickea cheques rechazados historicos");
	}

	public String validarFechaInvalida() {
		WebElement textoFechaInvalida = driver.findElement(By.xpath("//div//div[@class='sinFecha' and contains (., 'Fecha inv�lida')]"));
		return textoFechaInvalida.getText();
	}

	public List<String> validarFechasInvalidas() {
		List<WebElement> textosFechaInvalida = driver.findElements(By.xpath("//div//div[@class='sinFecha' and contains (., 'Fecha inv�lida')]"));
		List<String> txtsFechaInvalida = Arrays.asList(textosFechaInvalida.get(0).getText(), textosFechaInvalida.get(1).getText());
		return txtsFechaInvalida;
	}


}


