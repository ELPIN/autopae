package com.everis.po;

import com.everis.global.Global;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;


public class PatentesPage extends Global {

    // Definir web elements a nivel de clase

    public PatentesPage(WebDriver d) {
        super(d);
        PageFactory.initElements(driver, this);
    }

    //Definici�n de elementos Web
    @FindBy(xpath = "//input[@placeholder=\"Ingrese una patente, por ejemplo 'AR123CC'\"]")
    private WebElement we_inputPatente;

    @FindBy(xpath = "//span[contains (., 'Asociar Patente')]")
    private WebElement we_btn_asociarPatente;

    //otra opci�n -->"//span[contains (text(), �Eliminar Patente�)]"
    @FindBy(xpath = "//span[contains (., 'Eliminar Patente')]")
    private WebElement we_btn_eliminarPatente;

    @FindBy(className = "font-weight-bold patente-chip")
    private List<WebElement> we_patentesAsociadas;

    @FindBy(xpath = "//div//h4[contains(., 'Patentes habilitadas para FOB')]")
    private WebElement we_tituloPatentesHabilitadas;


   // Contenidos textuales de los componentes
    private String grillaSubUsuarioEliminar = "//tbody//tr[td[contains(.,'textoButton')]]//a[@class='btn rounded btn-outline-dark border-light cross dropdown-toggle tooltip-open tp-transparent']";
    private String grillaSubUsuarioModificar = "//tbody//tr[td[contains(.,'textoButton')]]//a[@class='btn rounded btn-outline-dark border-light pen dropdown-toggle tooltip-open tp-transparent']";
    private String subUsuarioIsPresent = "//tbody//tr[td[contains(.,'textoButton')]]";
    private String textoButton = "textoButton";
    private String popupSubUsuarioExistente = "//div[@class='modal-body' and contains (.,'El usuario no pudo ser dado de alta (El usuario ya existe)')]";
    private String modificarShipTo1 = "//tbody//tr[td/span[contains(.,'textoButton')]]//td/input[@class='form-check-input disableDefaultClick']";
    private String modificarShipTo2 = "//tbody//tr[td/span[contains(.,'textoButton')]]//td/input[@class='form-check-input']";
    private String habilitado1 = "//tbody//tr[td/span[contains(.,'textoButton')]]//td/input[@id='shiptoSeleccionado']";
    private static String numeroShipto;

    // Steps

//    public void irSeccionPatentes(){
//        click(seccionPatentes, "Se clickea sobre la seccion de patentes");
//    }

    public boolean buttonAsociarPatente() {
        try {
            waitForVisibility(we_btn_asociarPatente);
            return (we_btn_asociarPatente.isDisplayed());
        } catch (Exception e) {
            return false;
        }

    }

    public boolean tituloPatentesHabilitadas() {
        try {
            waitForVisibility(we_tituloPatentesHabilitadas);
            return (we_tituloPatentesHabilitadas.isDisplayed());
        } catch (Exception e) {
            return false;
        }

    }

//    public String errorAsociarPatente() {
//        return getText(cartelErrorPatente);
//    }


    // Actions

    public void asociarPatente(String patente) {
        sendKeys(we_inputPatente, patente, "Se ingresa patente: " + patente);
        click(we_btn_asociarPatente, "Se presiona bot�n de asociar patente.");
    }

}
